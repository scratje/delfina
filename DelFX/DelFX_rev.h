#define VERSION		1
#define REVISION	10
#define DATE		"09.06.99"
#define VERS		"DelFX 1.10"
#define VSTRING		"DelFX 1.10 (09.06.99)\r\n"
#define VERSTAG		"\0$VER: DelFX 1.10 (09.06.99)\r\n"
#define PROJECT		"DelFX"
