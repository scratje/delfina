#include <libraries/mui.h>
#include <dos/dos.h>
#include <graphics/gfxmacros.h>
#include <libraries/delfina.h>

#include <clib/alib_protos.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/utility.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "global.h"
#include "delfx_rev.h"
#include "delfx.h"

#define status(text) set(sttext, MUIA_Text_Contents, text)

extern APTR app, sttext, txrate, slider[SLIDERS];

extern struct Library *MUIMasterBase, *DelfinaBase, *IFFParseBase;

extern int freq;
extern struct DelfModule *delfmod;

/*******************************************************************/
/* The standard MUI levelmeter class is so ugly that I decided to  */
/* write my own.                                                   */


/*
** Create new VU meter 
*/
__saveds ULONG mNew(struct IClass *cl, Object *obj, Msg msg)
{
        struct LevelData *data;
        struct TagItem *tag, *tags;
        
        if (!(obj = (Object *)DoSuperMethodA(cl,obj,msg))) return(0);
        data=INST_DATA(cl,obj);
        
        data->vuaddr=0;
        data->oraddr=0;
        data->time=40;
        data->div=3;

        data->ihnode.ihn_Object=NULL;
        
        /* parse initial taglist */
        for (tags=((struct opSet *)msg)->ops_AttrList;tag=NextTagItem(&tags);)
        {
                switch (tag->ti_Tag)
                {
                        case ATTR_DELFADDR:
                                data->vuaddr=tag->ti_Data;break;

                        case ATTR_ORADDR:
                                data->oraddr=tag->ti_Data;break;
                                
                        case ATTR_FREQ:
                                if (tag->ti_Data) data->time=1000/tag->ti_Data;
                                break;
                                
                        case ATTR_RES:
                                data->div=10-tag->ti_Data;
                                break;

                        case ATTR_PENS+0:
                                if (tag->ti_Data) data->penspec0=*((struct MUI_PenSpec *)tag->ti_Data);
                                break;                          
                                
                        case ATTR_PENS+1:
                                if (tag->ti_Data) data->penspec1=*((struct MUI_PenSpec *)tag->ti_Data);
                                break;                          
                                
                        case ATTR_PENS+2:
                                if (tag->ti_Data) data->penspec2=*((struct MUI_PenSpec *)tag->ti_Data);
                                break;                          

                        case ATTR_PENS+3:
                                if (tag->ti_Data) data->penspec3=*((struct MUI_PenSpec *)tag->ti_Data);
                                break;                          
                }
        }
        
        data->level=65535;      
        data->actual=0;
        
        return((ULONG)obj);
}
        
/*
** OM_SET method, we need to see if someone changed our attributes.
*/

__saveds ULONG mSet(struct IClass *cl,Object *obj,Msg msg)
{
        struct LevelData *data = INST_DATA(cl,obj);
        struct TagItem *tag, *tags;

        for (tags=((struct opSet *)msg)->ops_AttrList;tag=NextTagItem(&tags);)
        {
                switch (tag->ti_Tag)
                {
                        case ATTR_ORADDR:
                                data->oraddr=tag->ti_Data;break;

                        case ATTR_DELFADDR:
                                data->vuaddr=tag->ti_Data;break;
                                
                        case ATTR_FREQ:
                                if (tag->ti_Data) {
                                        data->time=1000/tag->ti_Data;
                                        if (data->ihnode.ihn_Object) {
                                                DoMethod(app,MUIM_Application_RemInputHandler,&data->ihnode);
                                                data->ihnode.ihn_Object=obj;
                                                data->ihnode.ihn_Millis=data->time;
                                                data->ihnode.ihn_Flags=MUIIHNF_TIMER;
                                                data->ihnode.ihn_Method=METHOD_UPDATE;
                                                DoMethod(app,MUIM_Application_AddInputHandler,&data->ihnode);
                                        }
                                }
                                break;
                                
                        case ATTR_RES:
                                data->div=10-tag->ti_Data;
                                break;
                                
                        case ATTR_PENS+0:
                                if (tag->ti_Data) {
                                        data->penspec0=*((struct MUI_PenSpec *)tag->ti_Data);
                                        data->penchange=TRUE;
                                        MUI_Redraw(obj,MADF_DRAWOBJECT);
                                }
                                break;                          
                                
                        case ATTR_PENS+1:
                                if (tag->ti_Data) {
                                        data->penspec1=*((struct MUI_PenSpec *)tag->ti_Data);
                                        data->penchange=TRUE;
                                        MUI_Redraw(obj,MADF_DRAWOBJECT);
                                }
                                break;                          
                                
                        case ATTR_PENS+2:
                                if (tag->ti_Data) {
                                        data->penspec2=*((struct MUI_PenSpec *)tag->ti_Data);
                                        data->penchange=TRUE;
                                        MUI_Redraw(obj,MADF_DRAWOBJECT);
                                }
                                break;                          

                        case ATTR_PENS+3:
                                if (tag->ti_Data) {
                                        data->penspec3=*((struct MUI_PenSpec *)tag->ti_Data);
                                        data->penchange=TRUE;
                                        MUI_Redraw(obj,MADF_DRAWOBJECT);
                                }
                                break;                          
                }
        }
        
        return(DoSuperMethodA(cl,obj,msg));
}       

/*
** OM_GET method, see if someone wants to read delfaddr.
*/

static ULONG mGet(struct IClass *cl,Object *obj,Msg msg)
{
        struct LevelData *data = INST_DATA(cl,obj);
        ULONG *store = ((struct opGet *)msg)->opg_Storage;

        switch (((struct opGet *)msg)->opg_AttrID)
        {
                case ATTR_ORADDR:       *store = (ULONG)data->oraddr; return(TRUE);
                case ATTR_DELFADDR:     *store = (ULONG)data->vuaddr; return(TRUE);
                case ATTR_FREQ:         *store = (ULONG)(1000/data->time); return(TRUE);
                case ATTR_RES:          *store = (ULONG)(10-data->div); return(TRUE);
                case ATTR_PENS+0:       *store = (ULONG)&data->penspec0; return(TRUE);
                case ATTR_PENS+1:       *store = (ULONG)&data->penspec1; return(TRUE);
                case ATTR_PENS+2:       *store = (ULONG)&data->penspec2; return(TRUE);
                case ATTR_PENS+3:       *store = (ULONG)&data->penspec3; return(TRUE);
        }

        return(DoSuperMethodA(cl,obj,msg));
}

/*
** setup&cleanup, setup timer and obtain pen
*/
__saveds ULONG mSetup(struct IClass *cl, Object *obj, Msg msg)
{
        struct LevelData *data = INST_DATA(cl,obj);
        
        if (!DoSuperMethodA(cl,obj,msg)) return(FALSE);
        
        /* timer notification */
        data->ihnode.ihn_Object=obj;
        data->ihnode.ihn_Millis=data->time;
        data->ihnode.ihn_Flags=MUIIHNF_TIMER;
        data->ihnode.ihn_Method=METHOD_UPDATE;
        DoMethod(app,MUIM_Application_AddInputHandler,&data->ihnode);
        
        data->pen[0]=MUI_ObtainPen(muiRenderInfo(obj),&data->penspec0,0);
        data->pen[1]=MUI_ObtainPen(muiRenderInfo(obj),&data->penspec1,0);
        data->pen[2]=MUI_ObtainPen(muiRenderInfo(obj),&data->penspec2,0);
        data->pen[3]=MUI_ObtainPen(muiRenderInfo(obj),&data->penspec3,0);
        
        set(obj,MUIA_FillArea,FALSE);
        
        return(TRUE);
}

__saveds ULONG mCleanup(struct IClass *cl, Object *obj, Msg msg)
{
        int x;
        struct LevelData *data = INST_DATA(cl,obj);

        for (x=0;x<PENS;x++) MUI_ReleasePen(muiRenderInfo(obj),data->pen[x]);

        DoMethod(app,MUIM_Application_RemInputHandler,&data->ihnode);
        data->ihnode.ihn_Object=NULL;

        return(DoSuperMethodA(cl,obj,msg));
}


/*
** update method
** get data from Delfina, update display if needed
*/
__saveds ULONG mUpdate(struct IClass *cl, Object *obj, Msg msg)
{
        int val;
        struct LevelData *data=INST_DATA(cl,obj);
        
        val=Delf_Peek(data->vuaddr,DMEMF_XDATA);
        if (val>65535) val=65535;
        data->level+=(val-data->level)/data->div;
        
        if (data->oraddr) {
                if (data->or=Delf_Peek(data->oraddr,DMEMF_XDATA)) 
                        Delf_Poke(data->oraddr,DMEMF_XDATA,0);
        }       
        
        if (abs(data->actual-data->level)>100 || data->or != data->oldor)
                 MUI_Redraw(obj,MADF_DRAWUPDATE);

        return(0);
}

        
/*
** AskMinMax method will be called before the window is opened
** and before layout takes place. We need to tell MUI the
** minimum, maximum and default size of our object.
*/

__saveds ULONG mAskMinMax(struct IClass *cl,Object *obj,struct MUIP_AskMinMax *msg)
{
        /*
        ** let our superclass first fill in what it thinks about sizes.
        ** this will e.g. add the size of frame and inner spacing.
        */

        DoSuperMethodA(cl,obj,(Msg)msg);

        /*
        ** now add the values specific to our object. note that we
        ** indeed need to *add* these values, not just set them!
        */

        
        msg->MinMaxInfo->MinWidth  += 60;
        msg->MinMaxInfo->DefWidth  += 130;
        msg->MinMaxInfo->MaxWidth  += 500;

        msg->MinMaxInfo->MinHeight += 30;
        msg->MinMaxInfo->DefHeight += 100;
        msg->MinMaxInfo->MaxHeight += 300;

        return(0);
}

/*
** Draw method is called whenever MUI feels we should render
** our object. This usually happens after layout is finished
** or when we need to refresh in a simplerefresh window.
** Note: You may only render within the rectangle
**       _mleft(obj), _mtop(obj), _mwidth(obj), _mheight(obj).
*/

__saveds ULONG mDraw(struct IClass *cl,Object *obj,struct MUIP_Draw *msg)
{
        int i,w,h;
        struct LevelData *data;
        double rad,xscale,yscale;
        struct RastPort *rp;
        
        /*
        ** let our superclass draw itself first, area class would
        ** e.g. draw the frame and clear the whole region. What
        ** it does exactly depends on msg->flags.
        */

        DoSuperMethodA(cl,obj,(Msg)msg);

        /*
        ** if MADF_DRAWOBJECT isn't set, we shouldn't draw anything.
        ** MUI just wanted to update the frame or something like that.
        */
        data=INST_DATA(cl,obj);

        if (msg->flags & MADF_DRAWOBJECT) {       /* redraw the whole thing! */ 
        
                rp=_rp(obj);
/* allocate new pens */ 
                if (data->penchange) {
                        data->penchange=FALSE;
                        for (i=0;i<PENS;i++) MUI_ReleasePen(muiRenderInfo(obj),data->pen[i]);
                        data->pen[0]=MUI_ObtainPen(muiRenderInfo(obj),&data->penspec0,0);
                        data->pen[1]=MUI_ObtainPen(muiRenderInfo(obj),&data->penspec1,0);
                        data->pen[2]=MUI_ObtainPen(muiRenderInfo(obj),&data->penspec2,0);
                        data->pen[3]=MUI_ObtainPen(muiRenderInfo(obj),&data->penspec3,0);
                }
                
/* fill background */   
                SetAPen(rp,MUIPEN(data->pen[0]));
                RectFill(rp,_mleft(obj),_mtop(obj),_mright(obj),_mbottom(obj));
        
/* draw frame */        
                SetAPen(rp,_dri(obj)->dri_Pens[SHADOWPEN]);
                Move(rp,_mleft(obj),_mbottom(obj));
                Draw(rp,_mleft(obj),_mtop(obj));
                Draw(rp,_mright(obj),_mtop(obj));
                
                SetAPen(rp,_dri(obj)->dri_Pens[SHINEPEN]);
                Draw(rp,_mright(obj),_mbottom(obj));
                Draw(rp,_mleft(obj),_mbottom(obj));

/* calculate scale factor */    
                SetAPen(rp,MUIPEN(data->pen[2]));

                w=(_mright(obj)-_mleft(obj))/2;
                h=_mbottom(obj)-_mtop(obj);
                data->x=_mleft(obj)+w;
                data->y=_mbottom(obj)-1;
                data->xscale=w*95/100;
                data->yscale=h*95/100-2;
                data->oldx=data->x;
                data->oldy=data->y-1;

                data->actual=65536;
                data->level=0;
                
                data->textx=_mleft(obj)+2;
                data->texty=_mtop(obj)+1+rp->TxHeight;
                
/* draw scale */        
                for (i=1;i<10;i+=2) {
                        rad=i*0.3141;
                
                        xscale=data->xscale*1.05;yscale=data->yscale*1.05;
                        Move(rp,data->x-(int)(cos(rad)*xscale),data->y-(int)(sin(rad)*yscale));
                        xscale=(double)data->xscale;yscale=(double)data->yscale;
                        Draw(rp,data->x-(int)(cos(rad)*xscale),data->y-(int)(sin(rad)*yscale));
                }
        }
        if (msg->flags & (MADF_DRAWUPDATE | MADF_DRAWOBJECT)) {         /* update pointer */
        

                rp=_rp(obj);
                if (data->or != data->oldor) {
/* Draw clipping indicator */
                        data->oldor=data->or;
                        if (data->or)   SetAPen(rp,MUIPEN(data->pen[3]));
                        else                    SetAPen(rp,MUIPEN(data->pen[0]));
                                
                        Move(rp,data->textx,data->texty);
                        Text(rp,"CLIP",4);      
                }

                if (abs(data->actual-data->level)>200) {
/* clear old pointer    */
                        SetAPen(rp,MUIPEN(data->pen[0]));
                        Move(rp,data->oldx,data->oldy);
                        Draw(rp,data->x,data->y);
                
/* draw pointer */              
                        xscale=(double)data->xscale;yscale=(double)data->yscale;
                        SetAPen(rp,MUIPEN(data->pen[1]));
                        rad=data->level/26088.8+0.314;
                        data->oldx=data->x-(int)(cos(rad)*xscale);
                        data->oldy=data->y-(int)(sin(rad)*yscale);
                        Move(rp,data->oldx,data->oldy);
                        Draw(rp,data->x,data->y);
                        data->actual=data->level;
                }
        }

        return(0);
}

/*
** Here comes the dispatcher for our custom class. We only need to
** care about MUIM_AskMinMax and MUIM_Draw in this simple case.
** Unknown/unused methods are passed to the superclass immediately.
*/

__saveds __asm ULONG LevelDispatcher(   register __a0 struct IClass *cl,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg)
{                                                       
        switch (msg->MethodID)
        {
                case OM_NEW        : return(mNew      (cl,obj,(APTR)msg));
                case OM_SET        : return(mSet      (cl,obj,(APTR)msg));
                case OM_GET        : return(mGet      (cl,obj,(APTR)msg));
                case MUIM_AskMinMax: return(mAskMinMax(cl,obj,(APTR)msg));
                case MUIM_Setup    : return(mSetup    (cl,obj,(APTR)msg));
                case MUIM_Cleanup  : return(mCleanup  (cl,obj,(APTR)msg));
                case MUIM_Draw     : return(mDraw     (cl,obj,(APTR)msg));
                case METHOD_UPDATE : return(mUpdate       (cl,obj,(APTR)msg));
        }

        return(DoSuperMethodA(cl,obj,msg));
}


/*******************************************************************/
/* My own Decibel custom sub class. Show 1.5*realvalue in the knob */

ULONG __saveds __asm DecibelDispatcher( register __a0 struct IClass *cl,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg)
{
        struct DecibelData *data;
        struct MUIP_Numeric_Stringify *m = (APTR) msg;
        int x;
        
        switch (msg->MethodID) {
                case MUIM_Numeric_Stringify:
                        data=INST_DATA(cl,obj);
                        
                        sprintf(data->buf,"%.1f",m->value*1.5);
                        return((ULONG)data->buf);
                        
                case METHOD_UPDATE:
                        get(obj,MUIA_Numeric_Value,&x);
                        Delf_SetAttrs(DA_InputGain,x*INVAL,0);
                        return(0);

                case OM_NEW:
                        if (!(obj = (Object *)DoSuperMethodA(cl,obj,msg))) return(0);
                        DoMethod(obj,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,obj,1,METHOD_UPDATE);
                        return(obj);
        }               
        return(DoSuperMethodA(cl,obj,msg));
}

/*******************************************************************/
/* My own Rate custom sub class. Show real Delf freq in the knob  */

ULONG __saveds __asm RateDispatcher(    register __a0 struct IClass *cl,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg)
{
        struct RateData *data;
        struct MUIP_Numeric_Stringify *m = (APTR) msg;
        int newfreq,x;
        char c[10];
        
        switch (msg->MethodID) {
                case MUIM_Numeric_Stringify:
                        data=INST_DATA(cl,obj);
                        x=Delf_GetAttr(DA_Freq,m->value);
                        sprintf(data->buf,"%d",x);
                        return((ULONG)data->buf);
                        
                case METHOD_UPDATE:
                        get(obj,MUIA_Numeric_Value,&x);
                        newfreq=Delf_GetAttr(DA_Freq,x);
                        if (freq!=newfreq) {
                                Delf_SetModAttrs(delfmod,DM_Freq,newfreq,0);
                                freq=Delf_GetAttr(DA_Freq,0);
                                for (x=0;x<SLIDERS;x++) set(slider[x],ATTR_FREQ,freq);
                                sprintf(c,"%d",freq);
                                set(txrate,MUIA_Text_Contents,c);
                        }
                        return(0);
                        
                case OM_NEW:    
                        if (!(obj = (Object *)DoSuperMethodA(cl,obj,msg))) return(0);
                        DoMethod(obj,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,obj,1,METHOD_UPDATE);
                        DoMethod(obj,METHOD_UPDATE);
                        return(obj);
        }
        return(DoSuperMethodA(cl,obj,msg));
}
                
/*******************************************************************
** Value class, direct modify of Delfina x-space variable         
**
*/

ULONG __saveds __asm ValueDispatcher(   register __a0 struct IClass *cl,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg)
{
        struct ValueData *data;
        struct MUIP_Numeric_Stringify *m = (APTR) msg;
        struct TagItem *tag;
        int x;

        switch (msg->MethodID) {
                case MUIM_Numeric_Stringify:
                        data=INST_DATA(cl,obj);
                        if (data->dispdiv!=1) {
                                sprintf(data->buf,"%.1f",m->value/(double)data->dispdiv);
                                return((ULONG) data->buf);
                        }
                        break;
                        
                case METHOD_UPDATE:
                        data=INST_DATA(cl,obj);
                        get(obj,MUIA_Numeric_Value,&x);
                        if (data->delfaddr) Delf_Poke(data->delfaddr,DMEMF_XDATA,((x*48000)/data->freq)*data->mult);
                        return(0);
                        
                case OM_NEW:
                        if (!(obj = (Object *)DoSuperMethodA(cl,obj,msg))) return(0);
                        data=INST_DATA(cl,obj);
                        data->delfaddr=0;
                        data->mult=83886;
                        data->freq=48000;
                        data->dispdiv=1;
                        
                        tag=FindTagItem(ATTR_DELFADDR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->delfaddr=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_MULT,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->mult=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_DISPDIV,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->dispdiv=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_FREQ,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->freq=tag->ti_Data;
                        
                        DoMethod(obj,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,obj,1,METHOD_UPDATE);
                        DoMethod(obj,METHOD_UPDATE);
                        return(obj);
                
                case OM_SET:
                        data=INST_DATA(cl,obj);
                        
                        tag=FindTagItem(ATTR_DELFADDR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->delfaddr=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_MULT,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->mult=tag->ti_Data;
                                
                        tag=FindTagItem(ATTR_DISPDIV,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->dispdiv=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_FREQ,((struct opSet *)msg)->ops_AttrList);
                        if (tag) {
                                data->freq=tag->ti_Data;
                                DoMethod(obj,METHOD_UPDATE);
                        }
                        
                        break;
                        
        }
        return(DoSuperMethodA(cl,obj,msg));
}

/*******************************************************************
** Volume class, direct modify of Delfina x-space variable         
** 
*/


ULONG __saveds __asm VolumeDispatcher(  register __a0 struct IClass *cl,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg)
{
        struct VolumeData *data;
        struct TagItem *tag;
        int x;

        switch (msg->MethodID) {
                case METHOD_UPDATE:
                        data=INST_DATA(cl,obj);
                        get(obj,MUIA_Numeric_Value,&x);
                        if (x==-50) x=0;
                        else x=(int)(pow(2.0,x/6.0)*8388607.0)/data->div;
                        if (data->delfaddr) Delf_Poke(data->delfaddr,DMEMF_XDATA,x);
                        return(0);
                        
                case OM_NEW:
                        if (!(obj = (Object *)DoSuperMethodA(cl,obj,msg))) return(0);
                        data=INST_DATA(cl,obj);
                        data->delfaddr=0;
                        data->div=1;
                        
                        tag=FindTagItem(ATTR_DELFADDR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->delfaddr=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_DISPDIV,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->div=tag->ti_Data;
                        
                        DoMethod(obj,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,obj,1,METHOD_UPDATE);
                        DoMethod(obj,METHOD_UPDATE);
                        return(obj);
                
                case OM_SET:
                        data=INST_DATA(cl,obj);
                        
                        tag=FindTagItem(ATTR_DELFADDR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->delfaddr=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_DISPDIV,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->div=tag->ti_Data;
                        
                        break;
                        
        }
        return(DoSuperMethodA(cl,obj,msg));
}

                
/*******************************************************************
** Millisecond class, direct modify of Delfina x-space variable         
**
*/

ULONG __saveds __asm MilliDispatcher(   register __a0 struct IClass *cl,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg)
{
        struct MilliData *data;
        struct MUIP_Numeric_Stringify *m = (APTR) msg;
        struct TagItem *tag;
        int x;

        switch (msg->MethodID) {
                case MUIM_Setup:                /* hack, a bug in MUI */
                        data=INST_DATA(cl,obj);
                        data->fake=1;
                        break;
                
                case MUIM_Show:
                        data=INST_DATA(cl,obj);
                        data->fake=0;
                        break;

                case MUIM_Numeric_Stringify:
                        data=INST_DATA(cl,obj);
                        x=m->value*data->mult;
                        if (data->fake) {
                                sprintf(data->buf,"%d",abs(x*1000/8000));
                                return((ULONG) data->buf);
                        }
                        sprintf(data->buf,"%d",abs(x*1000/data->freq));
                        return((ULONG) data->buf);
                        
                case METHOD_UPDATE:
                        data=INST_DATA(cl,obj);
                        get(obj,MUIA_Numeric_Value,&x);
                        x=x*data->mult;
                        if (data->delfaddr && *data->sizeptr>=x) Delf_Poke(data->delfaddr,DMEMF_XDATA,x);
                        if (*data->sizeptr<x) 
                                DoMethod(app,MUIM_Application_ReturnID,data->appid);

                        MUI_Redraw(obj,MADF_DRAWUPDATE);
                        return(0);
                        
                case OM_NEW:
                        if (!(obj = (Object *)DoSuperMethodA(cl,obj,msg))) return(0);
                        data=INST_DATA(cl,obj);
                        data->delfaddr=0;
                        data->mult=10;
                        data->freq=48000;
                        
                        tag=FindTagItem(ATTR_APPID,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->appid=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_SIZEVAR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->sizeptr=(int *)tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_DELFADDR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->delfaddr=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_MULT,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->mult=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_FREQ,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->freq=tag->ti_Data;
                        
                        DoMethod(obj,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,obj,1,METHOD_UPDATE);
                        DoMethod(obj,METHOD_UPDATE);
                        return(obj);
                
                case OM_SET:
                        data=INST_DATA(cl,obj);
                        
                        tag=FindTagItem(ATTR_APPID,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->appid=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_SIZEVAR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->sizeptr=(int *)tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_DELFADDR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->delfaddr=tag->ti_Data;
                        
                        tag=FindTagItem(ATTR_MULT,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->mult=tag->ti_Data;
                                
                        tag=FindTagItem(ATTR_FREQ,((struct opSet *)msg)->ops_AttrList);
                        if (tag) {
                                data->freq=tag->ti_Data;
                                DoMethod(obj,METHOD_UPDATE);
                        }
                                
                        break;
                        
        }
        return(DoSuperMethodA(cl,obj,msg));
}

                
/*******************************************************************
** Equ class, direct modify of Delfina x-space variable         
**
*/

ULONG __saveds __asm EquDispatcher(     register __a0 struct IClass *cl,
                                                                        register __a2 Object *obj,
                                                                        register __a1 Msg msg)
{
        struct EquData *data;
        struct TagItem *tag;
        int x;

        switch (msg->MethodID) {
                case OM_NEW:
                        if (!(obj = (Object *)DoSuperMethodA(cl,obj,msg))) return(0);
                        data=INST_DATA(cl,obj);
                        data->delfaddr=0;
                        
                        tag=FindTagItem(ATTR_DELFADDR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->delfaddr=tag->ti_Data;
                        
                        DoMethod(obj,MUIM_Notify,MUIA_Prop_First,MUIV_EveryTime,obj,1,METHOD_UPDATE);
                        DoMethod(obj,METHOD_UPDATE);                    
                        return(obj);
                
                case OM_SET:
                        data=INST_DATA(cl,obj);
                        
                        tag=FindTagItem(ATTR_DELFADDR,((struct opSet *)msg)->ops_AttrList);
                        if (tag) data->delfaddr=tag->ti_Data;
                        break;
                        
                case METHOD_UPDATE:
                        data=INST_DATA(cl,obj);
                        get(obj,MUIA_Prop_First,&x);
                        x-=500;
                        
                        if (x>=0)
                                x*=-3355;
                        else
                                x=(int)(pow(-x/500.0,2.0)*8388607.0);
                                
                        if (data->delfaddr) Delf_Poke(data->delfaddr,DMEMF_XDATA,x);
                        return(0);
        }
        return(DoSuperMethodA(cl,obj,msg));
}
                
