/* commands */

enum { ID_STOP=1, MEN_PROJECT, MEN_HELP, MEN_ABOUT, MEN_QUIT, MEN_SETTINGS, MEN_METERS, MEN_MUI, ID_QUERY, ID_FRATE, ID_START,  ID_SAVE, ID_UNLIN, ID_FXLOAD, ID_FXSAVE, ID_PHASER, ID_DELAY};
enum { CYIN_LINE=0, CYIN_MIC, CYIN_MICLINE };

#define METHOD_UPDATE 0x8023
#define ATTR_ORADDR 0x8024
#define ATTR_DELFADDR 0x8025
#define ATTR_FREQ 0x8026
#define ATTR_RES 0x8027
#define ATTR_PENS 0x8028
#define ATTR_MULT 0x8029
#define ATTR_DISPDIV 0x802a
#define ATTR_SIZEVAR 0x802b
#define ATTR_APPID 0x802c

#define C_DIE   -1
#define C_NULL  0
#define C_START 1
#define C_STOP  2
#define C_QUERY 3

#define PENS 4
#define VUMETERS 5
#define SLIDERS 13
#define EFFECTS 6

#define INMAX 15
#define INVAL 4369

typedef struct {
        unsigned short  exponent;       // Exponent, bit #15 is sign bit for mantissa
        unsigned long   mantissa[2];    // 64 bit mantissa
} extended;

struct MyMsg {
        struct Message msg;
        ULONG  Command;  
        ULONG  Data;
};

struct FileData {
        char *name;
        int  mode;
        int  filefreq;
        int  codecfreq;
        int  stereo;
};

struct COMMch {
        WORD channels;
        ULONG frames;
        WORD bits;
        extended rate;
        ULONG compr;
};

ULONG __saveds __asm DecibelDispatcher( register __a0 struct IClass *,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg);
                                                                                
ULONG __saveds __asm LevelDispatcher(   register __a0 struct IClass *,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg);
                                                                                
ULONG __saveds __asm RateDispatcher(    register __a0 struct IClass *,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg);
                                                                                
ULONG __saveds __asm ValueDispatcher(   register __a0 struct IClass *,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg);

ULONG __saveds __asm VolumeDispatcher(  register __a0 struct IClass *,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg);

ULONG __saveds __asm MilliDispatcher(   register __a0 struct IClass *,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg);

ULONG __saveds __asm EquDispatcher(     register __a0 struct IClass *,
                                                                                register __a2 Object *obj,
                                                                                register __a1 Msg msg);
                                                                                
struct EquData
{
        int delfaddr;
};
                                                                                
struct MilliData
{
        int delfaddr;
        int mult;
        int freq;
        int fake;
        int *sizeptr;
        int appid;
        char buf[10];
};
                                                                                                                                                                
struct VolumeData
{
        int delfaddr;
        int div;
};
                                                                                                                                                                
struct DecibelData
{
        char buf[10];
};

struct RateData
{
        char buf[10];
};

struct ValueData
{
        int delfaddr;
        int mult;
        int freq;
        int dispdiv;
        char buf[10];
};

struct LevelData
{
        int level; /* 0-65535 */
        int actual;
        int xscale; /* length of the pointer */
        int yscale;
        int or;
        int oldor;
        int     x;
        int y;
        int oldx;
        int oldy;
        int vuaddr;
        int oraddr;
        int time;
        int div;
        int textx;
        int texty;
        BOOL penchange;
        int pen[4];
        struct MUI_PenSpec penspec0;
        struct MUI_PenSpec penspec1;
        struct MUI_PenSpec penspec2;
        struct MUI_PenSpec penspec3;
        struct MUI_InputHandlerNode ihnode;
};
