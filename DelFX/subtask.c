#include <dos/dos.h>
#include <libraries/delfina.h>
#include <exec/interrupts.h>
#include <libraries/iffparse.h>
#include <libraries/mui.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/iffparse.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <dos.h>

#include "delfx.h"

#define DBUFSIZE 4096
#define BUFSIZE 262144

int mode=0,framesize=2,bufsig;

__far char buf1[BUFSIZE],buf2[BUFSIZE];
char *diskbuf=buf1, *currbuf=buf2, *currptr=buf2;
int buflen=BUFSIZE,nextbuflen=BUFSIZE,copyflags,disksize=DBUFSIZE/2;

extern APTR app,sttext;
extern struct Library *DelfinaBase, *IFFParseBase;
extern struct MsgPort *subport, *mainport;
extern struct Process *subtask;
extern struct DelfPrg *prg;
extern void recrout(void);
extern void playrout(void);
struct MyMsg mainmsg;

/* status functions */
void status(char *text)
{
	DoMethod(app,MUIM_Application_PushMethod,sttext, 3, MUIM_Set,MUIA_Text_Contents,text);
}

void statusf(char *text,int arg)
{
	static char buf[500];
	sprintf(buf,text,arg);
	DoMethod(app,MUIM_Application_PushMethod,sttext, 3, MUIM_Set,MUIA_Text_Contents,buf);
}

/*
 *	Convert an 80 bit IEEE Standard 754 floating point number
 *	into an integer value.
 */

long ex2long(extended *ex)
{
	unsigned long	mantissa = ex -> mantissa[0];	// We only need 32 bits precision
	long			exponent = ex -> exponent,
					sign;

	if(exponent & 0x8000) sign = -1; else sign = 1;
	exponent = (exponent & 0x7FFF) - 0x3FFF;

	if(exponent < 0) mantissa = 0;
	else
	{
		exponent -= 31;
		if(exponent > 0) mantissa = 0x7FFFFFFF;
		else mantissa >>= -exponent;
	}
	return(sign * (long)mantissa);
}

extended *long2ex(ULONG i,extended *ex)
{
	int sign=0,exp=31;
	
	if (i<0) { 
		sign=0x8000;
		i=-i;
	}
	
	while (! (i & 0x80000000)) {
		i<<=1;
		exp--;
	}
	
	exp=exp+0x3fff | sign;	
	ex->exponent=exp;
	ex->mantissa[0]=i;
	ex->mantissa[1]=0;
	return(&ex);
}

/******************************************
 * open/close IFF handle with DOS file
 */

BOOL openiff(struct IFFHandle *iff, char *name,int mode)
{
	if (iff->iff_Stream=Open(name,mode ? MODE_NEWFILE : MODE_OLDFILE)) {
		InitIFFasDOS(iff);
		if (!OpenIFF(iff,mode)) return(TRUE);
		Close(iff->iff_Stream);
		iff->iff_Stream=NULL;
	}
	status("File open failed.");
	return(FALSE);
}

void closeiff(struct IFFHandle *iff)
{
	struct ContextNode *node;
	int frames=0,fail=0;
	
	if (iff) if (iff->iff_Stream) {
		if (mode==2) {				/* special magic on write handles */
			if (node=CurrentChunk(iff)) 
				frames=(node->cn_Scan-8)/framesize;
			if (PopChunk(iff)) fail=1;
			if (PopChunk(iff)) fail=1;
			CloseIFF(iff);
			Seek(iff->iff_Stream,0x16,OFFSET_BEGINNING);
			if (Seek(iff->iff_Stream,0,OFFSET_CURRENT)==0x16)
				Write(iff->iff_Stream,&frames,4);
			else fail=1;
			
			if (fail) status("Write error.");
				
		} else CloseIFF(iff);
		Close(iff->iff_Stream);
		iff->iff_Stream=NULL;
	}
}

/********************************************
 * Get file information
 * after this call, IFFHandle is positioned
 * in start of sample data. Just use
 * ReadChunkBytes() to get data..
 */
struct FileData *readhdr(struct IFFHandle *iff)
{
	static struct FileData fdata;
	struct COMMch *ch;	
	struct StoredProperty *prop;
	int buf[2];
	int fail=0;
	
	fdata.name=NULL;
	fdata.mode=0;
	fdata.codecfreq=0;
	
	if ((!PropChunk(iff,'AIFF','COMM')) && (!PropChunk(iff,'AIFC','COMM')) && (!StopChunk(iff,'AIFF','SSND')) && (!StopChunk(iff,'AIFC','SSND'))) {
		if (!ParseIFF(iff,IFFPARSE_SCAN)) {
			if (!(prop=FindProp(iff,'AIFF','COMM'))) prop=FindProp(iff,'AIFC','COMM');
			if (prop) {
				ch=(struct COMMch *)prop->sp_Data;
				if (prop->sp_Size>0x15) if (ch->compr!='NONE') fail=3;
				if (fail || (ch->channels>2) || (ch->bits!=16))
					fail=3;
				else {
					fdata.stereo=ch->channels-1;
					fdata.filefreq=ex2long(&ch->rate);
					if (ReadChunkBytes(iff,buf,8)!=8) fail=1;
					if (buf[0] || buf[1]) fail=3;
				}
			} else fail=3;
		} else fail=3;
	} else fail=2;
	
	switch (fail) {
		case 0:
			return(&fdata);
		
		case 1:
			status("Read error.");
			break;
			
		case 2:
			status("Iffparse error.");
			break;
			
		case 3:
			status("Invalid AIFF file.");
			break;
			
	}
	return(NULL);
}

/********************************************
 * write file information
 * after this call, IFFHandle is positioned
 * in start of sample data. Just use
 * WriteChunkBytes() to write data..
 */
struct FileData *writehdr(struct IFFHandle *iff, struct FileData *fdata)
{
	struct COMMch ch;	
	int buf[2]={0};
	int fail=0;
	
	ch.channels=fdata->stereo ? 2:1;
	ch.frames=0;
	ch.bits=16;
	long2ex(fdata->filefreq,&ch.rate);
	framesize=ch.channels*2;
	
	if (!PushChunk(iff,'AIFF','FORM',IFFSIZE_UNKNOWN)) {
		if (!PushChunk(iff,'AIFF','COMM',0x12)) {
			if (WriteChunkBytes(iff,&ch,0x12)==0x12) {
				PopChunk(iff);
				if (!PushChunk(iff,'AIFF','SSND',IFFSIZE_UNKNOWN)) {
					if (WriteChunkBytes(iff,buf,8)!=8) fail=1;
				} else fail=1;
			} else fail=1;
		} else fail=1;
	} else fail=1;
		
		
	if (fail) {
		status("Write error.");
		return(NULL);
	}
	
	return(fdata);
}

/************************************************
 * init buffer pointers etc
 */
void initbuf(void)
{
	Disable();
	diskbuf=buf2;
	currbuf=buf1;
	currptr=buf1;
	buflen=nextbuflen=BUFSIZE;
	Enable();
}

/***********************************************/
void __saveds subtaskcode(void)
{
	struct Interrupt recint = {0};
	struct Interrupt playint = {0};
	struct MyMsg *msg;
	struct FileData *fdata;
	struct IFFHandle *iff;
	int signals,sigbit,sigset;
	
	subport=CreateMsgPort();
	/* init msg */
	memset(&mainmsg,0,sizeof(struct MyMsg));
	mainmsg.msg.mn_Node.ln_Type=NT_MESSAGE;
	mainmsg.msg.mn_Length=sizeof(struct MyMsg);
	mainmsg.msg.mn_ReplyPort=subport;
	mainmsg.Command=0;	
	mainmsg.Data=0;
	
	sigbit=AllocSignal(-1);
	bufsig=1<<sigbit;	
	
	recint.is_Code=recrout;
	recint.is_Data=(void *)getreg(REG_A4);
	Delf_AddIntServer(prg->prog,&recint);
	
	playint.is_Code=playrout;
	playint.is_Data=(void *)getreg(REG_A4);
	Delf_AddIntServer(prg->prog+1,&playint);
	
	iff=AllocIFF();
	
	if (!iff || !subport) {
		if (subport) DeleteMsgPort(subport);
		if (iff) FreeIFF(iff);
		Delf_RemIntServer(prg->prog);
		Delf_RemIntServer(prg->prog+1);
		subtask=NULL;
		subport=NULL;
		Forbid();
		PutMsg(mainport,&mainmsg.msg);
		return;
	}
	
	PutMsg(mainport,&mainmsg.msg);
	
	signals=(1<<subport->mp_SigBit) | bufsig;
	/* main loop, wait for messages */
	for (;;) {
		sigset=Wait(signals);
		if (sigset & bufsig) {
			if (mode==2) {	/* REC */
				if (WriteChunkBytes(iff,diskbuf,BUFSIZE)!=BUFSIZE) {
					DoMethod(app,MUIM_Application_PushMethod,app, 2, MUIM_Application_ReturnID, 1);
				}
			} else if (mode==1) {		/* PLAY */
				if ((nextbuflen=ReadChunkBytes(iff,diskbuf,BUFSIZE))<0) nextbuflen=0;
				if (!buflen) { 				
					nextbuflen=0;
					Wait(bufsig);
					DoMethod(app,MUIM_Application_PushMethod,app, 2, MUIM_Application_ReturnID, 1);
				}
			}
		}
		
		while (msg=(struct MyMsg *)GetMsg(subport)) {
			switch (msg->Command) {
			
				case C_DIE:
					closeiff(iff);
					if (subport) DeleteMsgPort(subport);
					if (iff) FreeIFF(iff);
					Delf_RemIntServer(prg->prog);
					Delf_RemIntServer(prg->prog+1);
					subport=NULL;
					subtask=NULL;
					Forbid();		
					ReplyMsg(&msg->msg);
					return;
					
				case C_START:
					fdata=(struct FileData*)msg->Data;
					msg->Data=FALSE;
					if (fdata->mode==1) {
						if (openiff(iff,fdata->name,IFFF_READ)) {
							if (readhdr(iff)) {
								msg->Data=TRUE;
								mode=1;
								status("Playing...");
								initbuf();
								if ((buflen=ReadChunkBytes(iff,buf1,BUFSIZE))<0) buflen=0;
								if ((nextbuflen=ReadChunkBytes(iff,buf2,BUFSIZE))<0) nextbuflen=0;
								copyflags=DCPF_16BITH|DCPF_TO_DELFINA | (fdata->stereo?DCPF_LDATA : DCPF_XDATA);
								if (fdata->stereo) disksize=DBUFSIZE; else disksize=DBUFSIZE/2;
								Delf_Run(prg->prog,0,0,1+fdata->stereo*3,0,fdata->filefreq,0);
							}
						}
					} else {
						if (openiff(iff,fdata->name,IFFF_WRITE)) {
							if (writehdr(iff,fdata)) {
								msg->Data=TRUE;
								mode=2;
								status("Recording...");
								initbuf();
								copyflags=DCPF_16BITH|DCPF_TO_AMY | (fdata->stereo?DCPF_LDATA : DCPF_XDATA);
								if (fdata->stereo) disksize=DBUFSIZE; else disksize=DBUFSIZE/2;
								Delf_Run(prg->prog,0,0,2+fdata->stereo*3,0,fdata->filefreq,0);
							}
						}
					}
					ReplyMsg(&msg->msg);
					break;
					
				case C_STOP:
					Delf_Run(prg->prog,0,0,0,0,0,0);
					if (mode==2) {
						if (SetSignal(0,bufsig) & bufsig) 
							WriteChunkBytes(iff,diskbuf,BUFSIZE);
						WriteChunkBytes(iff,currbuf,currptr-currbuf);
					}
					closeiff(iff);
					mode=0;
					status("Stopped.");
					ReplyMsg(&msg->msg);
					break;
			
				case C_QUERY:
					if (openiff(iff,(char*)msg->Data,IFFF_READ)) {
						msg->Data=(ULONG)readhdr(iff);
						closeiff(iff);
					} else {
						 msg->Data=NULL;
						status("New file");
					}
					ReplyMsg(&msg->msg);
					break;
			}
		}
	}
}