
#include <libraries/mui.h>
#include <dos/dos.h>
#include <dos/dostags.h>
#include <graphics/gfxmacros.h>
#include <workbench/workbench.h>
#include <libraries/delfina.h>
#include <libraries/asl.h>
#include <exec/interrupts.h>

#include <clib/alib_protos.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/icon.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/gadtools.h>
#include <proto/utility.h>
#include <proto/asl.h>
#include <clib/muimaster_protos.h>
#include <pragmas/muimaster_pragmas.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "global.h"
#include "delfx_rev.h"
#include "delfx.h"
#include "effectparams.h"         // New Stuff

#define status(text) set(sttext, MUIA_Text_Contents, text)

struct Library *MUIMasterBase=NULL, *DelfinaBase=NULL, *IFFParseBase=NULL;

struct MsgPort *notifyport=NULL;
struct MsgPort *externport=NULL;  // New stuff

static struct MUI_CustomClass *DecibelClass=NULL, *LevelClass=NULL, *VolumeClass=NULL,
        *RateClass=NULL, *MilliClass=NULL, *ValueClass=NULL, *EquClass=NULL;

char fxpath[256]="PROGDIR:Effects";

char buff[20];          // New Stuff
int  number=1;          // New Stuff
BOOL port_ok = FALSE;   // New Stuff

APTR txinput,txoutput,popinput,popoutput,lstinput,lstoutput,txdel,btdel,txpha,btpha,app,stfname,cyftype,stfclock,sttext,mainw,vucfgw,strip,btstart,btstop,cymode,slvures,slvufreq,btsave,btuse,txrate,slrate,sldgain,sldlin,bteqres,strfxrd,strfxwr,popfxrd,popfxwr,btfxrd,btfxwr,btfxhk,winhot,bthotsave,bthotuse;
APTR vupop[PENS], vumeter[VUMETERS],slider[SLIDERS],btfx[EFFECTS],winfx[EFFECTS],preq[10],bteq[10],strhot[10];

int prefdata[256],*prefptr=prefdata;

/*** subtask stuff ***/
struct Process *subtask=NULL;
extern void *subtaskcode(void);
struct MsgPort *subport=NULL, *mainport=NULL;
struct MyMsg submsg;

void play_rec(BOOL record, BOOL showreq);

LONG __stack = 8192;

int freq=48000,active=0,hadpipe=0,outsocket=0;
struct DelfModule *delfmod=NULL;
DELFPTR intmem=0,delaybuf=0,phaserbuf=0,delaysize=0,phasersize=0;

char *inpipes[50], *outpipes[50],pipebuf[5000],outname[50]="Connectors";

int lintab[256];

extern struct DelfObj delfcode;

struct DelfPrg *prg=NULL;
struct Interrupt delfint;

struct ExtPortMsg {             // New Stuff
    struct Message msg;
    int            effect;
    int            param;
    long            value;
};

static struct NewMenu MenuData1[] =
{
        { NM_TITLE,     "Project"       , 0 ,0,0    ,(APTR)MEN_PROJECT },
        { NM_ITEM,      "Help..."      , 0 ,0,0         ,(APTR)MEN_HELP  },
        { NM_ITEM,      NM_BARLABEL    , 0 ,0,0      ,(APTR)0     },
        { NM_ITEM,      "About..."     , 0 ,0,0  ,(APTR)MEN_ABOUT   },
        { NM_ITEM,      NM_BARLABEL    , 0 ,0,0   ,(APTR)0  },
        { NM_ITEM,      "Quit"         ,"Q",0,0   ,(APTR)MEN_QUIT  },
        
        { NM_TITLE, "Settings"    , 0 ,0,0    ,(APTR)MEN_SETTINGS },
        { NM_ITEM,      "VU meters..."     , 0 ,0,0   ,(APTR)MEN_METERS    },
        { NM_ITEM,      "MUI..."    , 0 ,0,0     ,(APTR)MEN_MUI  },
        

        { NM_END,NULL,0,0,0,(APTR) 0 },
};


/**************************************************
 * Load & save FX 
 */
Object *sfx(int id, Object *obj)
{
        *prefptr++=id;
        *prefptr++=(int)obj;
        return(obj);
}

/* save FX */
void writefx(char *name)
{
        FILE *fh;
        static char file[512];
        int *ptr=prefdata;      
        int x,i;
        
        strcpy(file,fxpath);
        AddPart(file,name,512);
        if (fh=fopen(file,"wb")) {
                fwrite("DSFXEQUA",1,8,fh); /* write file id, equ id */
                for (x=0;x<10;x++) {
                        get (preq[x],MUIA_Prop_First,&i);
                        fwrite(&i,1,4,fh);      /* write data */
                }
                
                while (ptr[1]) {
                        get((Object *)ptr[1],MUIA_Numeric_Value,&i);
                        fwrite(ptr,1,4,fh);     /* write ID */
                        fwrite(&i,1,4,fh);      /* write data */
                        ptr+=2;
                }
                fclose(fh);
        }
}
                        
/* load FX */                   
void readfx(char *name)
{
        FILE *fh;
        static char file[512];
        int *ptr=prefdata,*startptr;
        int i,buf[2];
        
        strcpy(file,fxpath);
        AddPart(file,name,512);
        if (fh=fopen(file,"rb")) {
                fread(&i,1,4,fh);
                if (i != 'DSFX') {
                        status("Invalid file.");
                        fclose(fh);return;
                }
                
                while (fread(buf,1,8,fh)) {
                        switch(buf[0]) {
                                default:
                                        startptr=ptr;
                                        for (;;) {
                                                if (ptr[0]==buf[0]) {
                                                        set((Object *)ptr[1],MUIA_Numeric_Value,buf[1]);
                                                        break;
                                                }
                                                ptr+=2;
                                                if (!ptr[1]) ptr=prefdata;
                                                if (ptr==startptr) break;
                                        }
                                        break;
                                        
                                case 'EQUA':
                                        set(preq[0],MUIA_Prop_First,buf[1]);
                                        for (i=1;i<10;i++) {
                                                fread(buf,1,4,fh);
                                                set(preq[i],MUIA_Prop_First,buf[0]);
                                        }
                                        break;
                                        
                        }
                }
                fclose(fh);
                status("Effect loaded");
        } else {
                status("File not found.\n");
        }
}
                                
/************************************************************************
 * FX file hooks
 */
__saveds __asm BOOL FXStart(    register __a2 Object *obj,
                                                                register __a1 int *tag)
{
        Object *strobj;
        char *str;
        
        get(obj,MUIA_Popstring_String,&strobj);
        get(strobj,MUIA_String_Contents,&str);
        
        while (*tag) tag+=2;
        
        tag[0]=ASLFR_InitialDrawer;
        tag[1]=(int)fxpath;
        tag[2]=ASLFR_InitialFile;
        tag[3]=(int)str;
        tag[4]=TAG_DONE;
        
        return(TRUE);
}
        
__saveds __asm void FXStopL(    register __a2 Object *obj,      
                                                                register __a1 struct FileRequester *req)
{                                                               
        Object *strobj;
        
        strcpy(fxpath,req->fr_Drawer);
        
        get(obj,MUIA_Popstring_String,&strobj);
        set(strobj,MUIA_String_Contents,req->fr_File);
        
        if (obj==popfxrd) readfx(req->fr_File);
}

__saveds __asm void FXStopS(    register __a2 Object *obj,      
                                                                register __a1 struct FileRequester *req)
{                                                               
        Object *strobj;
        
        strcpy(fxpath,req->fr_Drawer);
        
        get(obj,MUIA_Popstring_String,&strobj);
        set(strobj,MUIA_String_Contents,req->fr_File);
        
        writefx(req->fr_File);
}

__saveds __asm void HotFunc(    register __a2 Object *obj)
{                                                               
        char *txt;
        
        get(obj,MUIA_String_Contents,&txt);
        if (txt) if (strlen(txt)) {
                set(popfxrd,MUIA_String_Contents,txt);
                readfx(txt);
        } else {
                status("Hotkey empty");
        }
}

__saveds __asm void FileFunc(   register __a2 Object *obj,      
                                                                register __a1 struct FileRequester *req)
{
        Object *strobj;
        static char file[512];

        get(obj,MUIA_Popstring_String,&strobj);

        strcpy(file,req->fr_Drawer);
        AddPart(file,req->fr_File,512);

        set(strobj,MUIA_String_Contents,file);

        DoMethod(app,MUIM_Application_ReturnID,ID_QUERY);       
}

__saveds __asm void PopFunc(    register __a2 Object *list,
                                                                register __a1 Object *txt)
{                                                               
        char *iname,*oname;
        struct DelfModule *imod, *omod,*oldi,*oldo;
        int osock=DSOCKET_NEXT,haspipe=0,x;
        
        DoMethod(list,MUIM_List_GetEntry,MUIV_List_GetEntry_Active,&iname);
        set(txt,MUIA_Text_Contents,iname);
        get(txinput,MUIA_Text_Contents,&iname);
        get(txoutput,MUIA_Text_Contents,&oname);
        
        ObtainSemaphore(delfmod->semaphore);
        
        imod=Delf_FindModule(iname);
        omod=Delf_FindModule(oname);
        if (!imod || !omod) {
                status("Module not found");
        } else {
                if (hadpipe) {
                        oldo=delfmod->sockets[0].module;
                        oldi=Delf_FindModule(outname);
                        if (oldi && oldo) Delf_SetPipe(oldo,delfmod->sockets[0].socket,oldi,outsocket);
                }
        
                if (omod->inputs>1) {
                        for (x=0;x<omod->inputs;x++) {
                                if (omod->sockets[x].module==imod) osock=x;
                        }
                        if (osock!=DSOCKET_NEXT) haspipe=1;
                } else {
                        osock=0;
                        if (omod->sockets[osock].module==imod) haspipe=1;
                }
                
                Delf_SetPipe(delfmod,0,NULL,0);                         /* remove old output */
                if (!Delf_SetPipe(delfmod,0,omod,osock)) {      /* connect output */
                        status("Invalid output");
                } else {
                        if (!Delf_SetPipe(imod,0,delfmod,0)) {  /* connect input */
                                status("Invalid input");
                        } else {
                                hadpipe=haspipe;
                                outsocket=osock;
                                strcpy(outname,oname);
                        }
                }
        }
        ReleaseSemaphore(delfmod->semaphore);
}
/*************************/
/* Init & Fail Functions */
/*************************/

static VOID fail(APTR app,char *str)
{
        struct DelfModule *oldi, *oldo;
        struct MyMsg *rep=NULL;
        
        if (delfmod) {
                if (hadpipe) {
                        ObtainSemaphore(delfmod->semaphore);
                        oldo=delfmod->sockets[0].module;
                        oldi=Delf_FindModule(outname);
                        if (oldi && oldo) Delf_SetPipe(oldo,delfmod->sockets[0].socket,oldi,outsocket);
                        ReleaseSemaphore(delfmod->semaphore);
                }
                Delf_RemModule(delfmod);
        }

        if (phaserbuf) Delf_FreeMem(phaserbuf,DMEMF_XDATA);
        if (delaybuf) Delf_FreeMem(delaybuf,DMEMF_YDATA);

        if (subtask && subport) {
                submsg.Command=C_DIE;
                PutMsg(subport,&submsg.msg);
                while (!rep) {
                        WaitPort(mainport);
                        rep=(struct MyMsg *)GetMsg(mainport);
                        if (rep) if (rep->Command!=C_DIE) rep=NULL;
                }
                while (GetMsg(mainport));       
        }
        
        // New Stuff
        if (externport)
            if ( externport->mp_Node.ln_Name ) RemPort(externport);
            DeleteMsgPort(externport);
        //---------------------------------------------------------

        if (notifyport) Delf_EndNotify(notifyport);
        if (mainport) DeleteMsgPort(mainport);  
        if (prg) Delf_RemPrg(prg);
        if (intmem) Delf_FreeMem(intmem,DMEMF_LDATA|DMEMF_INTERNAL);
        if (app) MUI_DisposeObject(app);
        if (EquClass) MUI_DeleteCustomClass(EquClass);
        if (MilliClass) MUI_DeleteCustomClass(MilliClass);
        if (VolumeClass) MUI_DeleteCustomClass(VolumeClass);
        if (ValueClass) MUI_DeleteCustomClass(ValueClass);
        if (DecibelClass) MUI_DeleteCustomClass(DecibelClass);
        if (LevelClass) MUI_DeleteCustomClass(LevelClass);
        if (RateClass) MUI_DeleteCustomClass(RateClass);
        if (MUIMasterBase) CloseLibrary(MUIMasterBase);
        if (DelfinaBase) CloseLibrary(DelfinaBase);
        if (IFFParseBase) CloseLibrary(IFFParseBase);                   
        
        if (str) {
                puts(str);
                exit(20);
        }
        exit(0);
}

static VOID init(VOID)
{
        if (!(IFFParseBase = OpenLibrary("iffparse.library",0)))
                fail(NULL,"Failed to open iffparse.library");
                
        if (!(MUIMasterBase = OpenLibrary(MUIMASTER_NAME,MUIMASTER_VMIN)))
                fail(NULL,"Failed to open "MUIMASTER_NAME".");
                
        if (!(DelfinaBase = OpenLibrary("delfina.library",4)))
                fail(NULL,"Failed to open delfina.library V4.");    

        RateClass = MUI_CreateCustomClass(NULL,MUIC_Slider,NULL,sizeof(struct RateData),RateDispatcher);
        if (!RateClass) 
                fail(NULL,"Failed to create custom class.");
                
        DecibelClass = MUI_CreateCustomClass(NULL,MUIC_Slider,NULL,sizeof(struct DecibelData),DecibelDispatcher);
        if (!DecibelClass) 
                fail(NULL,"Failed to create custom class.");
                
        LevelClass = MUI_CreateCustomClass(NULL,MUIC_Area,NULL,sizeof(struct LevelData),LevelDispatcher);
        if (!LevelClass)
                fail(NULL,"Failed to create custom class.");

        ValueClass = MUI_CreateCustomClass(NULL,MUIC_Slider,NULL,sizeof(struct ValueData),ValueDispatcher);
        if (!ValueClass)
                fail(NULL,"Failed to create custom class.");
                
        VolumeClass = MUI_CreateCustomClass(NULL,MUIC_Slider,NULL,sizeof(struct VolumeData),VolumeDispatcher);
        if (!VolumeClass)
                fail(NULL,"Failed to create custom class.");
                
        MilliClass = MUI_CreateCustomClass(NULL,MUIC_Slider,NULL,sizeof(struct MilliData),MilliDispatcher);
        if (!MilliClass)
                fail(NULL,"Failed to create custom class.");
                
        EquClass = MUI_CreateCustomClass(NULL,MUIC_Prop,NULL,sizeof(struct EquData),EquDispatcher);
        if (!EquClass)
                fail(NULL,"Failed to create custom class.");
                
        if (!(intmem=Delf_AllocMem(0x30,DMEMF_LDATA|DMEMF_INTERNAL|DMEMF_ALIGN_64))) 
                fail(NULL,"Out of Delfina's internal memory.");

        if (!(prg=Delf_AddPrg(&delfcode)))
                fail(NULL,"Out of Delfina memory.");

        if (!(mainport=CreateMsgPort()))
                fail(NULL, "Failed to create message port.");

        // New Stuff

        if (!(externport=CreateMsgPort()))
        {
                fail(NULL, "Failed to create external port.");
        }
        else
        {
            port_ok = FALSE;

            while(!port_ok)
            {
                struct MsgPort *port;

                sprintf(buff,"DelFX_External.%d",number);
                Forbid();
                port = FindPort(buff);
                Permit();

                if (port)
                {
                    number++;
                }
                else port_ok = TRUE;

            }
                externport->mp_Node.ln_Name = buff;
                externport->mp_Node.ln_Pri  = 0;
                AddPort(externport);
        }

        // -------------------------------------------------------------------

        if (!(notifyport=Delf_StartNotify(DA_Freq,DA_UpdateMod,0)))
                fail(NULL, "Failed to create notify port.");

        /* init msg */
        memset(&submsg,0,sizeof(struct MyMsg));
        submsg.msg.mn_Node.ln_Type=NT_MESSAGE;
        submsg.msg.mn_Length=sizeof(struct MyMsg);
        submsg.msg.mn_ReplyPort=mainport;
        submsg.Command=0;       
        submsg.Data=0;
        
        subtask=CreateNewProcTags(      NP_Entry, subtaskcode,
                                                                NP_Name, "DelFX task",
                                                                NP_Priority, 7, 0 );
        if (!subtask)
                fail(NULL,"Failed to create subtask.");
                
        WaitPort(mainport);     
        while (GetMsg(mainport));       
        if (!subport)
                fail(NULL,"Subtask initialization failed.");

        if (!(delfmod=Delf_AddModule(DM_Inputs,1, DM_Outputs,1, DM_Code, prg->prog+2,
                DM_Name, "DelFX", DM_Freq, 48000, 0)))
                fail(NULL, "Failed to allocate Delfina's audio.");
                

        Delf_Run(prg->prog,0,0,0,0,0,intmem);
                
}

/****************************/
/* Read & write config file */
void readcfg()
{
        int x;
        struct MUI_PenSpec spec;
        FILE *fh;
        
        if (fh=fopen("ENVARC:DelFX.prefs","rb")) {
                for (x=0;x<PENS;x++) {
                        fread(&spec,1,sizeof(struct MUI_PenSpec),fh);
                        set(vupop[x],MUIA_Pendisplay_Spec, &spec);
                }
                fread(&x,1,4,fh);
                set(slvures,MUIA_Numeric_Value,x);
                fread(&x,1,4,fh);
                set(slvufreq,MUIA_Numeric_Value,x);
                fclose(fh);
        }
}

void writecfg()
{
        int x;
        struct MUI_PenSpec *spec;
        FILE *fh;
        
        if (fh=fopen("ENVARC:DelFX.prefs","wb")) {
                for (x=0;x<PENS;x++) {
                        get(vupop[x],MUIA_Pendisplay_Spec, &spec);
                        fwrite(spec,1,sizeof(struct MUI_PenSpec),fh);
                }
                get(slvures,MUIA_Numeric_Value,&x);
                fwrite(&x,1,4,fh);
                get(slvufreq,MUIA_Numeric_Value,&x);
                fwrite(&x,1,4,fh);
                
                fclose(fh);
        } else {
                status("Write error.");
        }
}


                                                        
/**************************************************
 * update inpipes & outpipes 
 */
void updpipes(void)
{
        struct DelfModule *mod;
        int i=1,o=1;
        char *c=pipebuf;
        
        mod=Delf_FindModule(NULL);
        
        inpipes[0]="Connectors";
        outpipes[0]="Connectors";
        
        ObtainSemaphore(delfmod->semaphore);
        mod=(struct DelfModule *)mod->node.ln_Succ;
        
        while (mod->node.ln_Succ) {
                if (mod!=delfmod) {
                        strcpy(c,mod->node.ln_Name);
                        if (mod->inputs) inpipes[i++]=c;
                        if (mod->outputs) outpipes[o++]=c;
                        c+=strlen(mod->node.ln_Name)+1;
                }
                mod=(struct DelfModule *)mod->node.ln_Succ;
        }
        ReleaseSemaphore(delfmod->semaphore);
        inpipes[i]=NULL;
        outpipes[o]=NULL;

        
        DoMethod(lstinput,MUIM_List_Clear);
        DoMethod(lstoutput,MUIM_List_Clear);
        DoMethod(lstinput,MUIM_List_Insert,outpipes,-1,MUIV_List_Insert_Bottom);
        DoMethod(lstoutput,MUIM_List_Insert,inpipes,-1,MUIV_List_Insert_Bottom);
}

/* upd own mod */
void updmod(void)
{
        struct DelfModule *imod, *omod, *oldo, *oldi;
        int x,osock=DSOCKET_NEXT,haspipe=0;
        char *iname,*oname=NULL;
        
        omod=Delf_FindModule(NULL);
        
        ObtainSemaphore(delfmod->semaphore);
        
        imod=delfmod->sockets[0].module;
        if (!imod) imod=omod;
        iname=imod->node.ln_Name;
        
        while (omod->node.ln_Succ) {
                for (x=0;x<omod->inputs;x++) {
                        if (omod->sockets[x].module == delfmod) {
                                osock=x;
                                goto updmod_hack;
                        }
                }
                omod=(struct DelfModule *)omod->node.ln_Succ;
        }
        omod=Delf_FindModule(NULL);
        
updmod_hack:
        oname=omod->node.ln_Name;
        set(txinput,MUIA_Text_Contents,iname);
        set(txoutput,MUIA_Text_Contents,oname);
        
        if (omod->inputs>1) {
                if (osock!=DSOCKET_NEXT) haspipe=1;
        } else {
                osock=0;
                if (omod->sockets[osock].module==imod) haspipe=1;
        }
                
        hadpipe=haspipe;
        outsocket=osock;
        strcpy(outname,oname);
        ReleaseSemaphore(delfmod->semaphore);

}


/**************************************************
 * Unlinearity stuff for distortion
 */
void setlin(int val,int gain)
{
        int x,v;
        double pw,g;
        
        if (val || gain) {
                pw=1.0/(val/20.0+1.0);
                g=pow(2.0,gain/6.0);
        
                for (x=0;x<128;x++) {
                        v=(int)(pow(x/128.0,pw)*8388607.0*g);
                        if (v>0x7fffff) v=0x7fffff;
                        lintab[x+128]=v;
                        lintab[128-x]=-v;
                }
                lintab[0]=-(int)(pow(1.0,pw)*8388607.0);
                Delf_CopyA2D(lintab,prg->ydata,256*4,DCPF_YDATA|DCPF_32BIT);
                Delf_Poke(prg->xdata+DIST_EN,DMEMF_XDATA,1);
        } else 
                Delf_Poke(prg->xdata+DIST_EN,DMEMF_XDATA,0);
}       
        
/**************************************************
 * main
 */
int main(int argc,char *argv[])
{
        ULONG signals;
        BOOL running = TRUE;
        char *guidename;
        BPTR lock;
        int id,l,r,x;
        char *StereoSel[] = { "Mono", "Stereo",NULL };
        char *ModeSel[]  = { "Realtime", "Play", "Record",  NULL };
        char *name;
        char *fkey[] = {"f10","f1","f2","f3","f4","f5","f6","f7","f8","f9"};
        char str[10];
        struct DelfMsg *msg;
        struct ExtPortMsg *extmsg;
        struct TagItem *tagptr, *tag;
        struct FileData fdata, *querydata;
        
        static const struct Hook FileHook =    {{NULL,NULL},(void *)FileFunc,NULL,NULL};
        static const struct Hook FXStartHook = {{NULL,NULL},(void *)FXStart,NULL,NULL};
        static const struct Hook FXStopLHook = {{NULL,NULL},(void *)FXStopL,NULL,NULL};
        static const struct Hook FXStopSHook = {{NULL,NULL},(void *)FXStopS,NULL,NULL};
        static const struct Hook HotHook = {{NULL,NULL},(void *)HotFunc,NULL,NULL};
        static const struct Hook PopHook = {{NULL,NULL},(void *)PopFunc,NULL,NULL};
        
        init();

        setlin(0,0);
        
/* find Amigaguide help file*/
        if (lock=Lock(guidename="progdir:Docs/DelFX.guide",SHARED_LOCK)) UnLock(lock);
        else if (lock=Lock(guidename="progdir:DelFX.guide",SHARED_LOCK)) UnLock(lock);
        
/* create windows */
        vucfgw = WindowObject,
                MUIA_Window_Title, "VU prefs",
                MUIA_Window_ID, 'VUPF',
                WindowContents, VGroup,  
                        MUIA_HelpNode, "Meters",
                        Child, HGroup, 
                                Child, ColGroup(2), GroupFrameT("Colors"),
                                        Child, Label1("Background"),Child, vupop[0]=PoppenObject, End,
                                        Child, Label1("Pointer"),       Child, vupop[1]=PoppenObject, End,
                                        Child, Label1("Scale"),         Child, vupop[2]=PoppenObject, End,
                                        Child, Label1("Clipping"),      Child, vupop[3]=PoppenObject, End,
                                End,
                                Child, VGroup, GroupFrameT("Control"),
                                        Child, VSpace(0),
                                        Child, ColGroup(2),
                                                Child, Label1("Response"),      Child, slvures=SliderObject, MUIA_Numeric_Min,0, MUIA_Numeric_Max,9, MUIA_Numeric_Value, 7, End,
                                                Child, Label1("Freq (Hz)"),     Child, slvufreq=SliderObject, MUIA_Numeric_Min,1, MUIA_Numeric_Max,50, MUIA_Numeric_Value, 25, End,
                                        End,
                                        Child, VSpace(0),
                                End,
                        End,
                        Child, VSpace(4),
                        Child, HGroup,
                                Child, btsave = SimpleButton("Save"),
                                Child, HSpace(0),
                                Child, btuse = SimpleButton("Use"),
                        End,
                End,
        End;

        winfx[0] = WindowObject,
                MUIA_Window_Title,"Input",
                MUIA_Window_ID, 'INPU',
                WindowContents, VGroup,
                        Child, ColGroup(2), GroupFrameT("Input control"),
                                MUIA_HelpNode, "Noise gate",
                                Child, Label1("Effect bal"),    Child, slider[11] = sfx('IBAL',NewObject(ValueClass->mcc_Class,0,MUIA_Numeric_Min,-100,MUIA_Numeric_Max,100,ATTR_DELFADDR,prg->xdata+BAL_IN,0)),
                                Child, Label1("Noise gate"),    Child, slider[12] = sfx('GATE',NewObject(ValueClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,100,ATTR_DELFADDR,prg->xdata+CMP_GATE,0)),
                        End,
                        Child, VGroup, GroupFrameT("Compression"),
                                MUIA_HelpNode, "Compression",
                                Child, vumeter[4]=NewObject(LevelClass->mcc_Class,0,ATTR_DELFADDR,prg->xdata+VU_CMP, 0),
                                Child, ColGroup(2), 
                                        Child, Label1("Attenuate (dB)"),        Child, slider[7]= sfx('CATT',NewObject(VolumeClass->mcc_Class,0,MUIA_Numeric_Min,-30,MUIA_Numeric_Max,  0,ATTR_DELFADDR,prg->xdata+CMP_ATTEN,0)),
                                        Child, Label1("Sensitivity"),           Child, slider[0]=sfx('CSEN',NewObject(ValueClass->mcc_Class, 0,MUIA_Numeric_Min,  0,MUIA_Numeric_Max,100,ATTR_DELFADDR,prg->xdata+CMP_ADD,ATTR_MULT,40,0)),
                                End,
                        End,
                        Child, ColGroup(2), GroupFrameT("Distortion"),
                                MUIA_HelpNode, "Distortion",
                                Child, Label1("Gain (dB)"),             Child, sldgain=  sfx('DGAI',SliderObject,MUIA_Numeric_Min,0,MUIA_Numeric_Max,50,MUIA_Numeric_Value,0, End),
                                Child, Label1("Unlinearity"),   Child, sldlin=   sfx('DLIN',SliderObject,MUIA_Numeric_Min,0,MUIA_Numeric_Max,100,MUIA_Numeric_Value,0, End),
                        End,
                End,
        End;

        winfx[1] = WindowObject,
                MUIA_Window_Title,"Delay",
                MUIA_Window_ID, 'DELA',
                WindowContents, VGroup,
                        MUIA_HelpNode, "Delay",
                        Child, VGroup, GroupFrame, MUIA_Background, MUII_GroupBack, 
                                Child, ColGroup(2), 
                                        Child, Label1("Ratio (%)"),             Child, slider[4] = sfx('DERT',NewObject(ValueClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,100,ATTR_DELFADDR,prg->xdata+DEL_RATIO,0)),
                                        Child, Label1("Feedback (%)"),  Child, slider[5] = sfx('DEFB',NewObject(ValueClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,100,ATTR_DELFADDR,prg->xdata+DEL_FB,0)),
                                        Child, Label1("Delay (ms)"),    Child, slider[1]=sfx('DEMS',NewObject(MilliClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,100,ATTR_DELFADDR,prg->xdata+DEL_VALUE,ATTR_MULT,120,ATTR_SIZEVAR,&delaysize,ATTR_APPID,ID_DELAY,0)),
                                End,
                                Child, HGroup,
                                        Child, Label1("Buffer size"),
                                        Child, txdel=TextObject, TextFrame, MUIA_Background, MUII_BACKGROUND, MUIA_Text_Contents, "Flushed", End,
                                        Child, btdel=SimpleButton("Flush"),
                                End,
                        End,
                End,
        End;

        winfx[2] = WindowObject,
                MUIA_Window_Title,"Phaser",
                MUIA_Window_ID, 'PHAS',
                WindowContents, VGroup,
                        MUIA_HelpNode, "Phaser",
                        Child, VGroup, GroupFrame, MUIA_Background, MUII_GroupBack, 
                                Child, ColGroup(2), 
                                        Child, Label1("Ratio (%)"),                     Child, slider[6] = sfx('PHRT',NewObject(ValueClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,100,ATTR_DELFADDR,prg->xdata+PH_RATIO,0)),
                                        Child, Label1("Amplitude (ms)"),        Child, slider[2]=sfx('PHAM',NewObject(MilliClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,100,ATTR_DELFADDR,prg->xdata+PH_AMP,ATTR_MULT,20,ATTR_SIZEVAR,&phasersize,ATTR_APPID,ID_PHASER,0)),
                                        Child, Label1("Frequency (Hz)"),        Child, slider[3]=sfx('PHFR',NewObject(ValueClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,300,ATTR_DELFADDR,prg->xdata+PH_DPHASE,ATTR_MULT,8947,ATTR_DISPDIV,10,0)),
                                End,
                                Child, HGroup,
                                        Child, Label1("Buffer size"),
                                        Child, txpha=TextObject, TextFrame, MUIA_Background, MUII_BACKGROUND, MUIA_Text_Contents, "Flushed", End,
                                        Child, btpha=SimpleButton("Flush"),
                                End,
                        End,
                End,
        End;
                
        winfx[3] = WindowObject,
                MUIA_Window_Title,"Equalizer",
                MUIA_Window_ID, 'EQUA',
                WindowContents, VGroup,
                        MUIA_HelpNode, "Equalizer",
                        Child, VGroup, GroupFrame, MUIA_Background, MUII_GroupBack, 
                                Child, HGroup,
                                        Child, VGroup,
                                                Child, Label1("+14 dB"),
                                                Child, VSpace(0),
                                                Child, Label1("0 dB"),
                                                Child, VSpace(0),
                                                Child, Label1("-14 dB"),
                                                Child, bteqres=SimpleButton("Reset"),
                                        End,
                                        Child, VGroup,
                                                Child, HGroup,
                                                        Child, preq[0]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_0,0),
                                                        Child, preq[1]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_1,0),
                                                        Child, preq[2]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_2,0),
                                                        Child, preq[3]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_3,0),
                                                        Child, preq[4]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_4,0),
                                                        Child, preq[5]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_5,0),
                                                        Child, preq[6]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_6,0),
                                                        Child, preq[7]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_7,0),
                                                        Child, preq[8]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_8,0),
                                                        Child, preq[9]=NewObject(EquClass->mcc_Class,0,MUIA_Prop_First,500,MUIA_Prop_Horiz,FALSE,MUIA_Prop_Visible,1,MUIA_Prop_Entries,1000,ATTR_DELFADDR,prg->xdata+EQU_9,0),
                                                End,
                                                Child, HGroup,
                                                        Child, bteq[0]=SimpleButton("R"),
                                                        Child, bteq[1]=SimpleButton("R"),
                                                        Child, bteq[2]=SimpleButton("R"),
                                                        Child, bteq[3]=SimpleButton("R"),
                                                        Child, bteq[4]=SimpleButton("R"),
                                                        Child, bteq[5]=SimpleButton("R"),
                                                        Child, bteq[6]=SimpleButton("R"),
                                                        Child, bteq[7]=SimpleButton("R"),
                                                        Child, bteq[8]=SimpleButton("R"),
                                                        Child, bteq[9]=SimpleButton("R"),
                                                End,
                                        End,
                                End,
                        End,
                End,
        End;

        winfx[4] = WindowObject,
                MUIA_Window_Title,"Output",
                MUIA_Window_ID, 'OUTP',
                WindowContents, VGroup,
                        MUIA_HelpNode, "Output",
                        Child, VGroup, GroupFrameT("Output monitor"),
                                MUIA_HelpNode, "Meters",
                                Child, HGroup,
                                        Child, vumeter[2]=NewObject(LevelClass->mcc_Class,0,ATTR_DELFADDR,prg->xdata+VU_OUT_L,ATTR_ORADDR,prg->xdata+OR_OUT_L,0),
                                        Child, vumeter[3]=NewObject(LevelClass->mcc_Class,0,ATTR_DELFADDR,prg->xdata+VU_OUT_R,ATTR_ORADDR,prg->xdata+OR_OUT_R,0),
                                End,
                                Child, HGroup,
                                        Child, HSpace(0),
                                        Child, Label1("Left out"),
                                        Child, HSpace(0),
                                        Child, Label1("Right out"),
                                End,
                        End,
                        Child, ColGroup(2), GroupFrameT("Output control"),
                                Child, Label1("Effect bal"),            Child, slider[10] = sfx('OBAL',NewObject(ValueClass->mcc_Class, 0,MUIA_Numeric_Min,-100,MUIA_Numeric_Max,100,ATTR_DELFADDR,prg->xdata+BAL_OUT,0)),
                                Child, Label1("Effect vol (dB)"),       Child, slider[8] = sfx('SFXG',NewObject(VolumeClass->mcc_Class,0,MUIA_Numeric_Min, -50,MUIA_Numeric_Max, 18,ATTR_DELFADDR,prg->xdata+G_SFX,ATTR_DISPDIV,8,MUIA_Numeric_Value,-50,0)),
                                Child, Label1("Passthru vol (dB)"),     Child, slider[9] = sfx('DRYG',NewObject(VolumeClass->mcc_Class,0,MUIA_Numeric_Min, -50,MUIA_Numeric_Max,  0,ATTR_DELFADDR,prg->xdata+G_CLEAN,0)),
                        End,
                End,
        End;
                
        winfx[5] = WindowObject,
                MUIA_Window_Title,"Effects",
                MUIA_Window_ID, 'EFEX',
                WindowContents, VGroup,
                        MUIA_HelpNode, "EffectWin",
                        Child, VGroup, GroupFrameT("File"),
                                Child, popfxrd=PopaslObject,    MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strfxrd=StringObject,StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, ColGroup(2), MUIA_Group_SameSize,TRUE,
                                        Child, btfxrd=SimpleButton("Load"),
                                        Child, btfxwr=SimpleButton("Save"),
                                        Child, btfxhk=SimpleButton("Hotkeys..."),
                                        Child, popfxwr=PopaslObject,MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopSHook,MUIA_Popstring_String,strfxwr=StringObject,MUIA_String_MaxLen,256,MUIA_ShowMe,FALSE,End, MUIA_Popstring_Button, SimpleButton("Save as..."), ASLFR_TitleText, "Select an effect file...", ASLFR_DoSaveMode, TRUE, End,
                                End,
                        End,
                        Child, VSpace(0),
                        Child, btfx[0]=SimpleButton("Input..."),
                        Child, btfx[1]=SimpleButton("Delay..."),
                        Child, btfx[2]=SimpleButton("Phaser..."),
                        Child, btfx[3]=SimpleButton("Equalizer..."),
                        Child, btfx[4]=SimpleButton("Output..."),
                End,
        End;

        winhot=WindowObject,
                MUIA_Window_Title,"Hotkeys",
                MUIA_Window_ID,'HOTK',
                WindowContents, VGroup,
                        MUIA_HelpNode, "Hotkeys",
                        Child, ColGroup(2), GroupFrame, MUIA_Background, MUII_GroupBack, 
                                Child, Label1("F1"), Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[1]=StringObject,MUIA_ObjectID, 'HKF1',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, Label1("F2"), Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[2]=StringObject,MUIA_ObjectID, 'HKF2',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, Label1("F3"), Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[3]=StringObject,MUIA_ObjectID, 'HKF3',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, Label1("F4"), Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[4]=StringObject,MUIA_ObjectID, 'HKF4',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, Label1("F5"), Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[5]=StringObject,MUIA_ObjectID, 'HKF5',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, Label1("F6"), Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[6]=StringObject,MUIA_ObjectID, 'HKF6',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, Label1("F7"), Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[7]=StringObject,MUIA_ObjectID, 'HKF7',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, Label1("F8"), Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[8]=StringObject,MUIA_ObjectID, 'HKF8',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, Label1("F9"), Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[9]=StringObject,MUIA_ObjectID, 'HKF9',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                                Child, Label1("F10"),Child, PopaslObject, MUIA_Popasl_StartHook,&FXStartHook, MUIA_Popasl_StopHook,&FXStopLHook,MUIA_Popstring_String,strhot[0]=StringObject,MUIA_ObjectID, 'HKF0',StringFrame,MUIA_String_MaxLen,256,End,MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an effect file...",End,
                        End,
                        Child, VSpace(4),
                        Child, HGroup,
                                Child, bthotsave = SimpleButton("Save"),
                                Child, HSpace(0),
                                Child, bthotuse = SimpleButton("Use"),
                        End,
                End,
        End;

/* Create application */                
        app = ApplicationObject,
                MUIA_Application_Title      , PROJECT,
                MUIA_Application_Version    , "$VER: " VERS " (" DATE ")",
                MUIA_Application_Copyright  , "� 1997 by Petsoff Limited Partnership",
                MUIA_Application_Author     , "Teemu Suikki",
                MUIA_Application_Description, "Realtime sound effects using Delfina DSP",
                MUIA_Application_Base       , "DELFX",
                MUIA_Application_HelpFile       , guidename,
                
                SubWindow, mainw = WindowObject,
                        MUIA_Window_Title, delfmod->node.ln_Name,
                        MUIA_Window_ID   , 'MAIN',
                        MUIA_Window_Menustrip, strip = MUI_MakeObject(MUIO_MenustripNM,MenuData1,0),
                        MUIA_HelpNode, "MainWin",
                        WindowContents, VGroup,
                                Child, VGroup, GroupFrameT("Input monitor"),
                                        MUIA_HelpNode, "Meters",
                                        Child, HGroup,
                                                Child, vumeter[0]=NewObject(LevelClass->mcc_Class,0,ATTR_DELFADDR,prg->xdata+VU_IN_L, ATTR_ORADDR,prg->xdata+OR_IN_L, 0),
                                                Child, vumeter[1]=NewObject(LevelClass->mcc_Class,0,ATTR_DELFADDR,prg->xdata+VU_IN_R, ATTR_ORADDR,prg->xdata+OR_IN_R, 0),
                                        End,
                                        Child, HGroup,
                                                Child, HSpace(0),
                                                Child, Label1("Left"),
                                                Child, HSpace(0),
                                                Child, Label1("Right"),
                                        End,
                                End,

                                Child, ColGroup(2),
                                        Child, HGroup,
                                                Child, Label1("Status"),
                                                Child, sttext=TextObject, TextFrame, MUIA_Background, MUII_BACKGROUND, MUIA_Text_Contents, "Startup finished", End,
                                        End,
                                        Child, VGroup, GroupFrameT("Control"),
                                                MUIA_HelpNode, "Control",
                                                Child, ColGroup(2),
                                                        Child, Label1("Function"),      Child, cymode=CycleObject,       MUIA_Cycle_Entries, ModeSel, End,
                                                End,
                                                Child, VSpace(0),
                                                Child, HGroup, MUIA_Group_SameSize,TRUE,
                                                        Child, btstart=SimpleButton("Start"), 
                                                        Child, btstop=SimpleButton("Stop"), 
                                                        Child, HSpace(0),
                                                        Child, btfx[5]=SimpleButton("Effects..."),
                                                End,
                                        End,
                                        Child, VGroup, GroupFrameT("Analog I/O"),
                                                MUIA_HelpNode, "AnalogIO",
                                                Child, ColGroup(2),
                                                        Child, Label1("Input"),
                                                        Child, popinput=PopobjectObject, 
                                                                MUIA_Popstring_String, txinput=TextObject, TextFrame, MUIA_Background, MUII_BACKGROUND, MUIA_Text_Contents, "Connectors",End,
                                                                MUIA_Popstring_Button, PopButton(MUII_PopUp),
                                                                MUIA_Popobject_ObjStrHook, &PopHook,
                                                                MUIA_Popobject_Object, lstinput=ListviewObject,
                                                                        MUIA_Listview_List, ListObject,
                                                                                InputListFrame,
                                                                                MUIA_List_SourceArray, NULL,
                                                                        End,
                                                                        MUIA_Listview_MultiSelect, MUIV_Listview_MultiSelect_None,
                                                                End,
                                                        End,
                                                        Child, Label1("Output"),
                                                        Child, popoutput=PopobjectObject, 
                                                                MUIA_Popstring_String, txoutput=TextObject, TextFrame, MUIA_Background, MUII_BACKGROUND, MUIA_Text_Contents, "Connectors",End,
                                                                MUIA_Popstring_Button, PopButton(MUII_PopUp),
                                                                MUIA_Popobject_ObjStrHook, &PopHook,
                                                                MUIA_Popobject_Object, lstoutput=ListviewObject,
                                                                        MUIA_Listview_List, ListObject,
                                                                                InputListFrame,
                                                                                MUIA_List_SourceArray, NULL,
                                                                        End,
                                                                        MUIA_Listview_MultiSelect, MUIV_Listview_MultiSelect_None,
                                                                End,
                                                        End,
                                                        Child, Label1("Clock (Hz)"),    
                                                        Child, HGroup,
                                                                Child, slrate=NewObject(RateClass->mcc_Class,0,MUIA_Numeric_Min,16000,MUIA_Numeric_Max,48000,MUIA_Numeric_Value,48000,0),
                                                                Child, txrate=TextObject, TextFrame, MUIA_Background, MUII_BACKGROUND, MUIA_Text_Contents, "48000", End,
                                                        End,
                                                End,
                                        End,
                                        Child, VGroup,  GroupFrameT("File I/O"),
                                                MUIA_HelpNode, "FileIO",
                                                Child, ColGroup(2),
                                                        Child, Label1("File"),                  Child, stfname= PopaslObject, MUIA_Popasl_StopHook,&FileHook,MUIA_Popstring_String,String(0,256),MUIA_Popstring_Button, PopButton(MUII_PopFile), ASLFR_TitleText,"Select an AIFF file...",End,
                                                        Child, Label1("Type"),                  Child, cyftype= CycleObject,  MUIA_Cycle_Entries, StereoSel, End,
                                                        Child, Label1("Clock (Hz)"),    Child, stfclock=StringObject, StringFrame, MUIA_Background, MUII_BACKGROUND, MUIA_String_Accept, "1234567890", MUIA_String_Integer, 48000, End,
                                                End,
                                        End,
                                End,
                        End,
                End,
                SubWindow, vucfgw,
                SubWindow, winfx[0],
                SubWindow, winfx[1],
                SubWindow, winfx[2],
                SubWindow, winfx[3],
                SubWindow, winfx[4],
                SubWindow, winfx[5],
                SubWindow, winhot,
                        
        End;

        sfx(0,0);
        
        if (!app )
                fail(app,"Failed to create application.");
        updpipes();
        
/* module popups */     
        DoMethod(lstinput,MUIM_Notify,MUIA_Listview_DoubleClick,TRUE,popinput,2,MUIM_Popstring_Close,TRUE);
        DoMethod(lstoutput,MUIM_Notify,MUIA_Listview_DoubleClick,TRUE,popoutput,2,MUIM_Popstring_Close,TRUE);
        
/* FX files */
        DoMethod(strfxwr,MUIM_Notify,MUIA_String_Contents,MUIV_EveryTime,strfxrd,3,MUIM_Set,MUIA_String_Contents,MUIV_TriggerValue);
        DoMethod(strfxrd,MUIM_Notify,MUIA_String_Contents,MUIV_EveryTime,strfxwr,3,MUIM_Set,MUIA_String_Contents,MUIV_TriggerValue);
        DoMethod(btfxrd ,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,ID_FXLOAD);
        DoMethod(btfxwr ,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,ID_FXSAVE);
        
/* hotkeys */   
        DoMethod(btfxhk   ,MUIM_Notify,MUIA_Pressed,FALSE,                        winhot,3,MUIM_Set,MUIA_Window_Open,TRUE);
        DoMethod(winhot   ,MUIM_Notify,MUIA_Window_CloseRequest,TRUE, winhot,3,MUIM_Set,MUIA_Window_Open,FALSE);
        DoMethod(bthotuse ,MUIM_Notify,MUIA_Pressed,FALSE,            winhot,3,MUIM_Set,MUIA_Window_Open,FALSE);
        DoMethod(bthotsave,MUIM_Notify,MUIA_Pressed,FALSE,            app,2,MUIM_Application_Save,MUIV_Application_Save_ENVARC);
        DoMethod(bthotsave,MUIM_Notify,MUIA_Pressed,FALSE,            winhot,3,MUIM_Set,MUIA_Window_Open,FALSE);
        for (l=0;l<10;l++) {
                DoMethod(mainw  ,MUIM_Notify,MUIA_Window_InputEvent, fkey[l],strhot[l],2,MUIM_CallHook,&HotHook);
                DoMethod(winhot ,MUIM_Notify,MUIA_Window_InputEvent, fkey[l],strhot[l],2,MUIM_CallHook,&HotHook);
                DoMethod(vucfgw ,MUIM_Notify,MUIA_Window_InputEvent, fkey[l],strhot[l],2,MUIM_CallHook,&HotHook);
                
                for (r=0;r<EFFECTS;r++) {
                        DoMethod(winfx[r],MUIM_Notify,MUIA_Window_InputEvent, fkey[l],strhot[l],2,MUIM_CallHook,&HotHook);
                }
        }       
        DoMethod(app, MUIM_Application_Load,MUIV_Application_Load_ENVARC);
        
/* equalizer */         
        for (l=0;l<10;l++) {
                DoMethod(bteq[l],MUIM_Notify,MUIA_Pressed,FALSE,preq[l],3,MUIM_Set,MUIA_Prop_First,500);
                DoMethod(bteqres,MUIM_Notify,MUIA_Pressed,FALSE,preq[l],3,MUIM_Set,MUIA_Prop_First,500);
        }
                
/* effect windows */            
        for (l=0;l<EFFECTS;l++) {
                DoMethod(btfx[l], MUIM_Notify,MUIA_Pressed,FALSE,                       winfx[l],3,MUIM_Set,MUIA_Window_Open,TRUE);
                DoMethod(winfx[l],MUIM_Notify,MUIA_Window_CloseRequest,TRUE,winfx[l],3,MUIM_Set,MUIA_Window_Open,FALSE);
        }
                
/* poppen stuff */      
        for (l=0;l<PENS;l++) {
                for (r=0;r<VUMETERS;r++) {
                        DoMethod(vupop[l],MUIM_Notify,MUIA_Pendisplay_Spec,MUIV_EveryTime,vumeter[r],3,MUIM_Set, ATTR_PENS+l, MUIV_TriggerValue);
                }
                DoMethod(vupop[l],MUIM_Pendisplay_SetMUIPen,l ? MPEN_TEXT : MPEN_SHINE);
        }
        
/* vu notifications */
        for (l=0;l<VUMETERS;l++) {      
                DoMethod(slvures                ,MUIM_Notify,MUIA_Numeric_Value, MUIV_EveryTime, vumeter[l], 3, MUIM_Set,ATTR_RES,MUIV_TriggerValue);
                DoMethod(slvufreq               ,MUIM_Notify,MUIA_Numeric_Value, MUIV_EveryTime, vumeter[l], 3, MUIM_Set,ATTR_FREQ,MUIV_TriggerValue);
        }
        
/* phaser and delay */  
        DoMethod(btpha          ,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,ID_PHASER);
        DoMethod(btdel          ,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,ID_DELAY);
        
/* modes, record, play */       
        DoMethod(cymode         ,MUIM_Notify,MUIA_Cycle_Active,      MUIV_EveryTime,slrate,3,MUIM_Set,MUIA_Disabled,MUIV_TriggerValue);
        DoMethod(cymode         ,MUIM_Notify,MUIA_Cycle_Active,      MUIV_EveryTime,app,2,MUIM_Application_ReturnID,ID_FRATE);
        DoMethod(stfclock       ,MUIM_Notify,MUIA_String_Acknowledge,MUIV_EveryTime,app,2,MUIM_Application_ReturnID,ID_FRATE);
        DoMethod(stfname        ,MUIM_Notify,MUIA_String_Acknowledge,MUIV_EveryTime,app,2,MUIM_Application_ReturnID,ID_QUERY);
        DoMethod(btstart        ,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,ID_START);
        DoMethod(btstop         ,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,ID_STOP);
        
        
/* other notifications */
        DoMethod((Object *)DoMethod(strip,MUIM_FindUData,MEN_MUI      ),MUIM_Notify,MUIA_Menuitem_Trigger,MUIV_EveryTime,MUIV_Notify_Application,2,MUIM_Application_OpenConfigWindow,0);
        DoMethod((Object *)DoMethod(strip,MUIM_FindUData,MEN_HELP     ),MUIM_Notify,MUIA_Menuitem_Trigger,MUIV_EveryTime,MUIV_Notify_Application,5,MUIM_Application_ShowHelp,mainw,guidename,"Main","0");
        
        DoMethod(mainw          ,MUIM_Notify,MUIA_Window_CloseRequest,TRUE,app,2,MUIM_Application_ReturnID,MUIV_Application_ReturnID_Quit);
        
        DoMethod(btsave         ,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,ID_SAVE);
        DoMethod(btuse          ,MUIM_Notify,MUIA_Pressed,FALSE,                        vucfgw,3,MUIM_Set,MUIA_Window_Open,FALSE);
        DoMethod(vucfgw         ,MUIM_Notify,MUIA_Window_CloseRequest,TRUE, vucfgw,3,MUIM_Set,MUIA_Window_Open,FALSE);
        DoMethod((Object *)DoMethod(strip,MUIM_FindUData,MEN_METERS),MUIM_Notify,MUIA_Menuitem_Trigger,MUIV_EveryTime,vucfgw,3,MUIM_Set,MUIA_Window_Open,TRUE);
        
        DoMethod(sldlin         ,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,app,2,MUIM_Application_ReturnID,ID_UNLIN);
        DoMethod(sldgain        ,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,app,2,MUIM_Application_ReturnID,ID_UNLIN);
        
/* load stuff, update gadgets */
        readcfg();
        
        set(mainw,MUIA_Window_Open,TRUE);
        
/*
** Input loop...
*/
        signals=0;

        while (running)
        {
                {
                        int upd=0, freq=0;
                        char c[10];
                        
                        while (msg=(struct DelfMsg *)GetMsg(notifyport)) {
                                tagptr=msg->taglist;
                                msg->result=NULL;
                                while (tag=NextTagItem(&tagptr)) {
                                        switch (tag->ti_Tag) {
                                                case DA_Freq:
                                                        freq=tag->ti_Data;
                                                        break;
                                        
                                                case DA_UpdateMod:
                                                        if (!strcmp(delfmod->node.ln_Name,(char *)tag->ti_Data)) /* own mod? */
                                                                updmod();
                                                        upd++;
                                                        break;
                                        }
                                }
                                ReplyMsg(&msg->msg);
                        }

                        if (freq) {
                                sprintf(c,"%ld",freq);
                                set(txrate,MUIA_Text_Contents,c);
                                for (x=0;x<SLIDERS;x++) set(slider[x],ATTR_FREQ,freq);
                        }
                        if (upd) updpipes();

//------------- New Stuff -----------------

                        while (extmsg=(struct ExtPortMsg *)GetMsg(externport)) {
                                switch(extmsg->effect)
                                {
                                    case EQ:
                                            set(preq[extmsg->param],MUIA_Prop_First,extmsg->value);
                                        break;

                                    case PHASER:
                                            switch(extmsg->param)
                                            {
                                                case RATIO:      // Ratio
                                                set(slider[6],MUIA_Numeric_Value,extmsg->value);
                                                    break;

                                                case PARAM1:      // Amplitude
                                                set(slider[2],MUIA_Numeric_Value,extmsg->value);
                                                    break;

                                                case PARAM2:      // Frequency
                                                set(slider[3],MUIA_Numeric_Value,extmsg->value);
                                                   break;

                                                default:
                                                    break;
                                            }
                                        break;

                                    case DELAY:
                                            switch(extmsg->param)
                                            {
                                                case RATIO:      // Ratio
                                                set(slider[4],MUIA_Numeric_Value,extmsg->value);
                                                    break;

                                                case PARAM1:      // Feedback
                                                set(slider[5],MUIA_Numeric_Value,extmsg->value);
                                                    break;

                                                case PARAM2:      // Delay
                                                set(slider[1],MUIA_Numeric_Value,extmsg->value);
                                                   break;

                                                default:
                                                   break;
                                            }
                                        break;

                                    case DISTORTION:
                                            if (extmsg->param == ATT_GAIN) // Gain
                                            {
                                                set(sldgain,MUIA_Numeric_Value,extmsg->value);
                                            }
                                            else                           // Unlinearity
                                            {
                                                set(sldlin,MUIA_Numeric_Value,extmsg->value);
                                            }
                                        break;

                                    case COMPRESSOR:     // Compressor
                                            if (extmsg->param == ATT_GAIN)  // Attenuate
                                            {
                                                set(slider[7],MUIA_Numeric_Value,extmsg->value);
                                            }
                                            else                            // Sensitivity
                                            {
                                                set(slider[0],MUIA_Numeric_Value,extmsg->value);
                                            }
                                        break;

                                    case EFFECT_LV:           // Effect Level
                                            set(slider[8],MUIA_Numeric_Value,extmsg->value);
                                        break;

                                    case PASSTHRU_LV:         // Passthrough level
                                            set(slider[9],MUIA_Numeric_Value,extmsg->value);
                                        break;

                                    case OUTPUT_BAL:          // Output Effects Balance
                                            set(slider[10],MUIA_Numeric_Value,extmsg->value);
                                            break;

                                    case INPUT_BAL:           // Input Effects Balance
                                            set(slider[11],MUIA_Numeric_Value,extmsg->value);
                                            break;

                                    case NOISE_GATE:          // Noise Gate
                                            set(slider[12],MUIA_Numeric_Value,extmsg->value);
                                            break;

                                    case QUIT_PROG:           // Quit Delfx
                                            running=FALSE;
                                            break;

                                    case RECORD_START:
                                    play_rec(TRUE, FALSE);
                                            break;

                                    case PLAY_START:
                                    play_rec(FALSE, FALSE);
                                            break;

                                    case STOP:
                                    if (!active) break;

                                    submsg.Command=C_STOP;
                                    PutMsg(subport,&submsg.msg);
                                    while (!GetMsg(mainport)) WaitPort(mainport);

                                    set(cymode,MUIA_Disabled,FALSE);
                                    set(stfname,MUIA_Disabled,FALSE);
                                    set(stfclock,MUIA_Disabled,FALSE);
                                    set(cyftype,MUIA_Disabled,FALSE);
                                    active=0;
                                            break;

                                    default:
                                        break;
                                }

                                ReplyMsg((struct Message *)extmsg);
                        }
//-------------------------------------------------------------------------------------------------------
                }

                id = DoMethod(app,MUIM_Application_NewInput,&signals);
                switch (id)
                {
                        case MEN_ABOUT:
                                MUI_Request(app,mainw,0,NULL,"*_OK",VERS" ("DATE")\n\n� 1999 by Petsoff Limited Partnership\nBars&Pipes interface by Giles Jones");
                                break;
                                
                        case MEN_QUIT:
                        case MUIV_Application_ReturnID_Quit:
                                running=FALSE;
                                break;
                                
                        case ID_SAVE:   
                                writecfg();
                                set(vucfgw,MUIA_Window_Open,FALSE);
                                break;
                                
                        case ID_UNLIN:
                                get(sldlin,MUIA_Numeric_Value,&l);
                                get(sldgain,MUIA_Numeric_Value,&r);
                                setlin(l,r);
                                break;
                                
                        case ID_FXLOAD:
                                get(strfxrd,MUIA_String_Contents,&name);
                                l=(int)name;
                                if (l) l=strlen(name);
                                if (l) {
                                        readfx(name);
                                } else {        
                                        DoMethod(popfxrd,MUIM_Popstring_Open);
                                }
                                break;
                                
                        case ID_FXSAVE:
                                get(strfxwr,MUIA_String_Contents,&name);
                                l=(int)name;
                                if (l) l=strlen(name);
                                if (l) {
                                        writefx(name);
                                } else {        
                                        DoMethod(popfxwr,MUIM_Popstring_Open);
                                }
                                break;
                                
                        
                        case ID_START:
                                {
                                long l;
                                get(cymode,MUIA_Cycle_Active,&l);

                                if(l==1)
                                    play_rec(FALSE, TRUE);
                                else if(l==2)
                                    play_rec(TRUE, TRUE);
                                }
                                break;
                                
                        case ID_STOP:
                                if (!active) break;
        
                                submsg.Command=C_STOP;
                                PutMsg(subport,&submsg.msg);
                                while (!GetMsg(mainport)) WaitPort(mainport);
                                
                                set(cymode,MUIA_Disabled,FALSE);
                                set(stfname,MUIA_Disabled,FALSE);
                                set(stfclock,MUIA_Disabled,FALSE);
                                set(cyftype,MUIA_Disabled,FALSE);
                                active=0;
                                break;
                                
                        case ID_QUERY:
                                if (active) break;
                                
                                get(stfname,MUIA_String_Contents,&name);
                                if (!strlen(name)) break;

                                submsg.Command=C_QUERY;
                                submsg.Data=(ULONG)name;
                                PutMsg(subport,&submsg.msg);
                                while (!GetMsg(mainport)) WaitPort(mainport);
                                querydata=(struct FileData*)submsg.Data;
                                
                                if (querydata) {
                                        set(stfclock,MUIA_String_Integer,querydata->filefreq);
                                        set(cyftype,MUIA_Cycle_Active,querydata->stereo);
                                        status("Header loaded.");
                                }
                        case ID_FRATE:
                                get(cymode,MUIA_Cycle_Active,&l);
                                if (!l) break;
                                
                                get(stfclock,MUIA_String_Integer,&r);
                                if (r<4000 || r>96000) {
                                        status("Freq range 4-96kHz!");
                                        break;
                                }
                                
                                if (l==1) {
                                        if          (r<=8000)  r=8000;
                                        else if (r<=9600)  r=9600;
                                        else if (r<=16000) r=16000;
                                        else if (r<=27429) r=27429;
                                        else if (r<=32000) r=32000;
                                        else               r=48000;
                                }
                                set(slrate,MUIA_Numeric_Value,r);
                                break;
                                
                        case ID_PHASER:         /* check if we need a new phaser buf */
                                get(slider[2],MUIA_Numeric_Value,&l);
                                l*=40;
                                Delf_Run(prg->prog+4,0,0,0,delaybuf,0,delaysize-1);
                                if (phaserbuf) Delf_FreeMem(phaserbuf,DMEMF_XDATA);
                                l=(l+1023) & 0xfffffc00;
                                if (l) {
                                        r=10;
                                        for (x=1024;x<l;x*=2) r++;
                                        r<<=24;
                                        phaserbuf=Delf_AllocMem(l,DMEMF_XDATA|DMEMF_CLEAR|r);
                                        if (phaserbuf) {
                                                phasersize=l/2;
                                                Delf_Run(prg->prog+4,0,0,phaserbuf,delaybuf,l-1,delaysize-1);
                                                sprintf(str,"%d k",l/1024);
                                                DoMethod(slider[2],METHOD_UPDATE);
                                        } else {
                                                phasersize=0;
                                                sprintf(str,"No mem!\n");
                                                status("Phaser failure");
                                        }
                                } else {
                                        phaserbuf=0;
                                        phasersize=0;
                                        sprintf(str,"Flushed\n");
                                }
                                set(txpha,MUIA_Text_Contents,str);
                                break;
                                
                        case ID_DELAY:          /* check if we need a new delay buf */
                                get(slider[1],MUIA_Numeric_Value,&l);
                                l*=120;
                                Delf_Run(prg->prog+4,0,0,phaserbuf,0,phasersize*2-1,0);
                                if (delaybuf) Delf_FreeMem(delaybuf,DMEMF_YDATA);
                                l=(l+1023) & 0xfffffc00;
                                if (l) {
                                        r=10;
                                        for (x=1024;x<l;x*=2) r++;
                                        r<<=24;
                                        delaybuf=Delf_AllocMem(l,DMEMF_YDATA|DMEMF_CLEAR|r);
                                        if (delaybuf) {
                                                delaysize=l;
                                                Delf_Run(prg->prog+4,0,0,phaserbuf,delaybuf,phasersize*2-1,delaysize-1);
                                                sprintf(str,"%d k",l/1024);
                                                DoMethod(slider[1],METHOD_UPDATE);
                                        } else {
                                                delaysize=0;
                                                sprintf(str,"No mem!\n");
                                                status("Delay failure");
                                        }
                                } else {
                                        delaysize=0;
                                        delaybuf=0;
                                        sprintf(str,"Flushed\n");
                                }
                                set(txdel,MUIA_Text_Contents,str);
                                break;
                }
                if (running && signals) signals=Wait(signals | (1<<notifyport->mp_SigBit) | (1<<externport->mp_SigBit));
        }

        set(mainw,MUIA_Window_Open,FALSE);

/*
** Shut down...
*/
        
        MUI_DisposeObject(app);
        fail(NULL,NULL);
}

void play_rec(BOOL record, BOOL showreq)
{

int l, r;
char *name;
BPTR lock;
struct FileData fdata;

if (active) return;

    if (record) set(cymode,MUIA_Cycle_Active,2);
    else  set(cymode,MUIA_Cycle_Active,1);

                                get(cymode,MUIA_Cycle_Active,&l);
                                get(stfname,MUIA_String_Contents,&name);
                                if (!l) return;
                                if (!strlen(name)) {
                                        status("Select a file!\n");
                                        return;
                                }

                                if (l==2) if (lock=Lock(name,SHARED_LOCK)) {
                                        UnLock(lock);

                                    if (showreq){
                                            if (!MUI_Request(app,mainw,0,NULL,"*_Yes|_No","File '%s' exists.\nDo you wish to overwrite it?",name)) {
                                                    status("Cancelled..");
                                                    return;
                                            }
                                    }
                                }

                                get(stfclock,MUIA_String_Integer,&r);
                                if (r<4000 || r>96000) {
                                        status("Freq range 4-96kHz!");
                                        return;
                                }
                                fdata.filefreq=r;

                                if (l==1) {
                                        if          (r<=8000)  r=8000;
                                        else if (r<=9600)  r=9600;
                                        else if (r<=16000) r=16000;
                                        else if (r<=27429) r=27429;
                                        else if (r<=32000) r=32000;
                                        else               r=48000;
                                }

                                set(slrate,MUIA_Numeric_Value,r);
                                r=Delf_GetAttr(DA_Freq,0);

                                submsg.Command=C_START;
                                submsg.Data=(ULONG)&fdata;
                                fdata.name=name;
                                fdata.mode=l;
                                fdata.codecfreq=r;
                                get(cyftype,MUIA_Cycle_Active,&fdata.stereo);
                                PutMsg(subport,&submsg.msg);
                                while (!GetMsg(mainport)) WaitPort(mainport);
                                if (active=submsg.Data) {
                                        set(cymode,MUIA_Disabled,TRUE);
                                        set(stfname,MUIA_Disabled,TRUE);
                                        set(stfclock,MUIA_Disabled,TRUE);
                                        set(cyftype,MUIA_Disabled,TRUE);
                                }
}
