enum{               // Main effect type
        EQ,
        PHASER,
        DELAY,
        DISTORTION,
        COMPRESSOR,
        EFFECT_LV,     // Effect dB Level
        PASSTHRU_LV,   // Passthrough dB Level
        INPUT_BAL,
        OUTPUT_BAL,
        NOISE_GATE,
        QUIT_PROG,
        RECORD_START,
        PLAY_START,
        STOP
        

};

enum{               // EQ Parameters
        BAND1,
        BAND2,
        BAND3,
        BAND4,
        BAND5,
        BAND6,
        BAND7,
        BAND8,
        BAND9,
        BAND10
};

enum{              // Delay/Phaser Parameters
        RATIO,
        PARAM1,    // Feedback/Amplitude
        PARAM2,    // Delay/Frequency
};

enum{               // Distortion/Compressor Parameters
        ATT_GAIN,       // Attenuate/Gain
        SENS_UNLIN, // Sensitivity/Unlinearity
};
