
	xdef	_recrout
	xdef	@recrout
	xdef	_playrout
	xdef	@playrout

	xref	_subtask
	xref	_bufsig
	xref	_diskbuf
	xref	_currbuf
	xref	_currptr
	xref	_buflen
	xref	_nextbuflen
	xref	_LVOSignal
	xref	_copyflags
	xref	_disksize
	
Delf_CopyMem = -48

_playrout
@playrout
	movem.l	a3/a4,-(a7)
	move.l	a1,a4
	move.l	d0,a1			;a1=Dest
	
	move.l	_currptr(a4),a0
	move.l	_disksize(a4),d0		;number of bytes
	
	move.l	_currbuf(a4),d1
	add.l	_buflen(a4),d1		;d1= buffer end
	
	lea	(a0,d0.w),a3		;a3= buffer pos after this
	cmp.l	a3,d1
	bhi.s	.notlast
	
	move.l	_copyflags(a4),d1
	jsr	Delf_CopyMem(a6)
	
	move.l	_diskbuf(a4),_currptr(a4)	;dbuffer
	move.l	_currbuf(a4),_diskbuf(a4)
	move.l	_currptr(a4),_currbuf(a4)
	move.l	_nextbuflen(a4),_buflen(a4)
	
	move.l	_bufsig(a4),d0
	move.l	_subtask(A4),a1
	move.l	4,a6
	jsr	_LVOSignal(a6)	
	movem.l	(a7)+,a3/a4
	rts
	
.notlast	
	move.l	a3,_currptr(a4)
	move.l	_copyflags(a4),d1
	jsr	Delf_CopyMem(a6)
	movem.l	(a7)+,a3/a4
	rts
	
	
_recrout
@recrout
	movem.l	a3/a4,-(a7)
	move.l	a1,a4
	move.l	d0,a0			;a0=source
	
	move.l	_currptr(a4),a1
	move.l	_disksize(a4),d0		;number of bytes
	
	move.l	_currbuf(a4),d1
	add.l	_buflen(a4),d1		;d1= buffer end
	
	lea	(a1,d0.w),a3		;a3= buffer pos after this
	cmp.l	a3,d1
	bhi.s	.notlast
	beq.s	.noclear
	
	exg	a3,d1			;a3= ptr
	sub.l	a3,d1			;d1=buffer len in bytes
	lsr.l	#2,d1
	subq.w	#1,d1
.loop
	clr.l	(a3)+
	dbf	d1,.loop
	
.noclear	
	move.l	_copyflags(a4),d1
	jsr	Delf_CopyMem(a6)
	
	move.l	_diskbuf(a4),_currptr(a4)	;dbuffer
	move.l	_currbuf(a4),_diskbuf(a4)
	move.l	_currptr(a4),_currbuf(a4)
	move.l	_nextbuflen(a4),_buflen(a4)
	
	move.l	_bufsig(a4),d0
	move.l	_subtask(A4),a1
	move.l	4,a6
	jsr	_LVOSignal(a6)	
	movem.l	(a7)+,a3/a4
	rts
	
.notlast	
	move.l	a3,_currptr(a4)
	move.l	_copyflags(a4),d1
	jsr	Delf_CopyMem(a6)
	movem.l	(a7)+,a3/a4
	rts

