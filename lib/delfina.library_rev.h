#define VERSION		4
#define REVISION	19
#define VERS		"delfina.library 4.19"
#define VSTRING		"delfina.library 4.19 (15.11.21)\r\n"
#define VERSTAG		"\0$VER: delfina.library 4.19 (15.11.21)\r\n"
