

struct ahistuff {
	struct DelfModule       *delfmod;	/* Delfina module */
	DELFPTR					delfrecbuf; /* rec buffer on delfina */
	struct AHIAudioCtrlDrv  *actrl;     /* pointer to our daddy */
	struct Hook				*playerfunc;/* functions */
	struct Hook				*mixerfunc;
	struct Hook				*samplerfunc;
	struct DelfPrg			*prg;		/* delfina program */
	WORD 			   		*mixbuf;	/* mix buffer */
	int						buffdiv;	/* number of bufs between playerfunc calls */
	int						buffdivcnt;
	int						buffoffset;  
	int						buffsamples;/* number of samples in Delfina mixbuf */
	int						buffsize;	/* Delfina mixbuf size in bytes */
	int						copyflags;	/* CopyMem flags */
	struct AHIRecordMessage	recmsg;		/* record message */
	struct Interrupt    	playint;	/* player interrupt */
	struct Interrupt		recint;		/* recorder interrupt */
	int						cookie;
};

/* prototypes for other functions*/
void subtaskcode(void);
void runprg(DELFPTR, ULONG, ULONG, ULONG, ULONG);
struct Interrupt *findkey(int);
void ClearMem(ULONG, DELFPTR, ULONG);
struct DelfPrg *LoadPrg(struct DelfPrg *, struct DelfObj *);
void poke(DELFPTR,ULONG,ULONG);
ULONG peek(DELFPTR,ULONG);
int bestfreq(int);
int getv(int);
int getdb(int);
void saveprefs(char *);
void loadprefs(char *);
void update(void);
void CheckNotifyA(struct TagItem *);
void CheckNotify(ULONG, ...);
BOOL setpipe(struct DelfModule *, ULONG, struct DelfModule *, ULONG);
BOOL setpipeq(struct DelfModule *, ULONG, struct DelfModule *, ULONG);
BOOL sortmods(void);
BOOL delfmod(void);
void remchunk(int,struct membl *);
int newbuf(UBYTE *);
void cause(ULONG);

__asm __saveds void __UserLibCleanup(void);

/* prototypes for the asm functions */

extern int __asm ramsey(void);
extern void __asm put24( register __d0 int);
extern void __asm put16( register __d0 int);
extern void __asm put8( register __d0 int);

extern int __asm get24( void);
extern int __asm get16( void);
extern int __asm get8( void);

extern void __asm runvec(register __d0 int);
extern void __asm startDA(register __d0 int, register __d1 int);
extern void __asm startAD(register __d0 int, register __d1 int);
extern int __asm endDA(void);
extern int __asm endAD(void);

//extern void __asm changeport(void);
//extern void __asm changeintport(void);

extern void __asm litead32l(register __a0 APTR, register __d0 int);
extern void __asm litead32h(register __a0 APTR, register __d0 int);
extern void __asm litead24(register __a0 APTR, register __d0 int);
extern void __asm litead16(register __a0 APTR, register __d0 int);
extern void __asm litead8(register __a0 APTR, register __d0 int);

extern void __asm plusad32l(register __a0 APTR, register __d0 int);
extern void __asm plusad32h(register __a0 APTR, register __d0 int);
extern void __asm plusad24(register __a0 APTR, register __d0 int);
extern void __asm plusad16(register __a0 APTR, register __d0 int);
extern void __asm plusad8(register __a0 APTR, register __d0 int);

extern void __asm liteda32l(register __a0 APTR, register __d0 int);
extern void __asm liteda32h(register __a0 APTR, register __d0 int);
extern void __asm liteda24(register __a0 APTR, register __d0 int);
extern void __asm liteda16(register __a0 APTR, register __d0 int);
extern void __asm liteda8(register __a0 APTR, register __d0 int);

extern __asm __saveds void *reada4(void);
extern void DelfinaInt(void);
extern void RunInt(void);
extern void LiteRunInt(void);
extern void LiteInt(void);
extern void DogInt(void);
extern void SigInt(void);

extern void __asm copyDA32L(	register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm copyDA32H(	register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm copyDA24(		register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm copyDA16L(	register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm copyDA16H(	register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm copyDA8(		register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm copyAD32L(	register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm copyAD24(		register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm copyAD16L(	register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm copyAD16H(	register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm copyAD8(		register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm LcopyDA32L(	register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm LcopyDA32H(	register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm LcopyDA24(	register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm LcopyDA16L(	register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm LcopyDA16H(	register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm LcopyDA8(		register __a0 void *,	register __a1 int,		register __d0 ULONG);
extern void __asm LcopyAD32L(	register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm LcopyAD32H(	register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm LcopyAD24(	register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm LcopyAD16L(	register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm LcopyAD16H(	register __a0 int,		register __a1 void *,	register __d0 ULONG);
extern void __asm LcopyAD8(		register __a0 int,		register __a1 void *,	register __d0 ULONG);

extern void __asm writeio(register __d0 int);
extern void __asm writeint(register __d0 int);

/* DelfinaPrefs */
 
struct dprefs {
	UWORD control;
	ULONG data1;
	ULONG data2;
};

/* the private extended notify struct */
struct notify {
	struct Message	msg;		/* a DelfMsg in the beginning.. */
	struct TagItem *taglist;
	ULONG			result;
	struct notify  *next;		/* new fields here */
	struct MsgPort *port;
	ULONG 		   *tagids;
	UWORD 			len;		/* total len of memory allocated */
	UWORD			flags;		/* see below */
};

#define NF_INUSE 1
#define NF_ALLOC 2


#define MODNAMESIZE 64	

/* DelfModule flags */
#define DF_SHAREIO 1		/* shared io */

#define CF_MLB		0x1000
#define CF_OLB		0x0800
#define CF_ST		0x0004
#define CF_HPF		0x0080

#define CF_DF		0x0003
#define CF_16bit	0x0000
#define CF_8bit		0x0003
#define CF_uLaw		0x0001
#define CF_ALaw		0x0002

#define CF_DFR		0x003c

#define CF_GLOBAND	~(CF_DF|CF_DFR)
#define CF_GLOBOR	CF_ST|CF_48000Hz|0x400
#define CF_MASK		(CF_MLB|CF_OLB|CF_HPF)
#define DF1_LO		0x3f0000
#define DF1_RO		0x003f00

#define DF1_LE		0x400000
#define DF1_HE		0x800000

#define DF2_LG		0x0f0000
#define DF2_RG		0x000f00
#define DF2_MO		0x00f000


#define DF2_IS		0x100000
#define DF2_OVR		0x200000

/* delfina y space variables */
#define y_runx1		(DELFPTR)(0xc0 + varbase)
#define y_runx0		(DELFPTR)(0xc1 + varbase)
#define y_runy1		(DELFPTR)(0xc2 + varbase)
#define y_runy0		(DELFPTR)(0xc3 + varbase)
#define y_runaddr	(DELFPTR)(0xc4 + varbase)
#define y_runmode	(DELFPTR)(0xc5 + varbase)
#define y_intkey	(DELFPTR)(0xc6 + varbase)
#define y_intparam	(DELFPTR)(0xc7 + varbase)
#define y_rddata1	(DELFPTR)(0xc8 + varbase)
#define y_rddata2	(DELFPTR)(0xc9 + varbase)
#define y_wrdata1	(DELFPTR)(0xca + varbase)
#define y_wrdata2	(DELFPTR)(0xcb + varbase)
#define y_wrctrl	(DELFPTR)(0xcc + varbase)
#define y_idlel		(DELFPTR)(0xd1 + varbase)
#define y_idleh		(DELFPTR)(0xd2 + varbase)
#define y_error		(DELFPTR)(0xe0 + varbase)
#define y_iobuf		(DELFPTR)(0xe1 + varbase)
#define y_cpupanic	(DELFPTR)(0xe2 + varbase)
#define y_outovr	(DELFPTR)(0xe3 + varbase)
#define y_delfser	(DELFPTR)(0xe4 + varbase)
#define y_memsize (DELFPTR)(0xe5 + varbase)
#define y_codecvars (DELFPTR)(0xf0 + varbase)

/* delfina private interrupt keys */
#define key_done	0
#define key_runprg	1
#define key_watchdog 3

/* delfina private kernel vectors */
#define delf_null		(DELFPTR)0x1e
#define delf_causeq		(DELFPTR)0x28
#define delf_resetcodec	(DELFPTR)0x2b
#define delf_remkernel	(DELFPTR)0x2c
#define delf_divide		(DELFPTR)0x2e
#define delf_extworld	(DELFPTR)0x2f

/* these are at $200 */
#define delf_setbuf		(DELFPTR)(vecbase)
#define delf_calibrate	(DELFPTR)(vecbase+2)
#define delf_setmixvol	(DELFPTR)(vecbase+4)
#define delf_watchdog	(DELFPTR)(vecbase+6)

/* interrupt vectors */

#define v_inth	0xa0
#define v_intme	0xa1
#define v_copyAD	0xa2
#define v_copyDA	0xa3
#define v_lcopyAD	0xa4
#define v_lcopyDA	0xa5
#define v_copyend	0xa6
#define v_initrun	0xa7
#define v_runprg	0xa8
#define v_encopy	0xa9
#define v_flushput	0xaa
#define v_copyADh	0xab
#define v_copyDAh	0xac
#define v_lcopyADh	0xad
#define v_lcopyDAh	0xae
#define v_getptr	0xaf

#define FREQUENCIES	4

/* definitions for Delf_Cause() */
#define IRQF_IRQA    1L
#define IRQF_IRQB    2L
#define IRQF_RESET   4L

/* minimun allocation size */
#define D_MINALLOC 32

/* memoryblock structure */
struct membl {
	UWORD	next;		/* index of next membl, 0 = end */
	UWORD	size;		/* allocation size in D_MINALLOC units */
};

struct modinfo {
	ULONG	inlist;		// input list
	ULONG	outlist;	// output list
	ULONG	imaskh;	// input mask h
	ULONG	imaskl;	// input mask l
	ULONG	omaskh;
	ULONG	omaskl;
	ULONG 	code;
};

#define BUFFERS 256

#define DA_CodecFreq TAG_USER+0x1101

// lite base registers clockport
#define ICR 0
#define CVR 18
#define ISR 4
#define IVR 22
#define DH  26 
#define DM  12
#define DL  30
// lite base registers localport
#define ICRlp 0
#define CVRlp 32
#define ISRlp 2
#define IVRlp 34
#define DHlp  36
#define DMlp  6
#define DLlp  38
 
// hostctl flags
#define HC_RESET 0x80
#define HC_HENCLK 0x40
#define HC_QUERYVER 0x20
#define HC_IRQDIS   0x10
#define HC_CODECSEL 0x01