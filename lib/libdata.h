#include <exec/types.h>
#include <exec/tasks.h>
#include <exec/ports.h>
#include <exec/memory.h>
#include <exec/lists.h>
#include <exec/semaphores.h>
#include <exec/execbase.h>
#include <exec/alerts.h>
#include <exec/libraries.h>
#include <exec/interrupts.h>
#include <exec/resident.h>
#include <proto/exec.h>
#include <dos/dos.h>

#include <libraries/delfina.h>

#ifdef __PPC__
#include <emul/emulinterface.h>
#include <emul/emulregs.h>

#include <public/quark/quark.h>
#include <public/proto/quark/syscall_protos.h>
#endif


struct DelfinaBase
{
	struct Library		 Lib;
	BPTR			 SegList;
};

#ifdef __PPC__
#define REG(x)
#define ASM
#else
#define REG(x) register x
#define ASM __saveds __asm
#endif

struct Library * ASM    LIB_Open(REG(__a6) struct DelfinaBase *DelfinaBase);
ULONG       ASM         LIB_Close(REG(__a6) struct DelfinaBase *DelfinaBase);
ULONG	    ASM         LIB_Expunge(REG(__a6) struct DelfinaBase *DelfinaBase);
ULONG       ASM  	LIB_Reserved(void);

ULONG ASM            LIB_Private0(void);
ULONG ASM            LIB_Delf_Peek(REG(__a1) DELFPTR , REG(__d0) ULONG );
void  ASM            LIB_Delf_Poke(REG(__a1) DELFPTR , REG(__d0) ULONG , REG(__d1) ULONG );
void  ASM            LIB_Delf_CopyMem(REG(__a0) int , REG(__a1) int , REG(__d0) ULONG , REG(__d1) ULONG );
DELFPTR  ASM         LIB_Delf_AllocMem(REG(__d0) ULONG , REG(__d1) ULONG );
void  ASM            LIB_Delf_FreeMem(REG(__a1) DELFPTR , REG(__d0) ULONG );
ULONG  ASM           LIB_Delf_AvailMem(REG(__d1) ULONG );
struct DelfPrg * ASM LIB_Delf_AddPrg(REG(__a1) struct DelfObj * );
void  ASM            LIB_Delf_RemPrg(REG(__a1) struct DelfPrg *);
ULONG  ASM           LIB_Delf_Run(REG(__a1) DELFPTR , REG(__d0) long , REG(__d1) ULONG , REG(__d2) ULONG , REG(__d3) ULONG , REG(__d4) ULONG , REG(__d5) ULONG );
ULONG ASM            LIB_Private1(void);
ULONG  ASM           LIB_Delf_AddIntServer(REG(__d0) ULONG , REG(__a1) struct Interrupt * );
struct Interrupt * ASM LIB_Delf_RemIntServer(REG(__d0) ULONG );
ULONG ASM            LIB_Private2(void);
ULONG ASM            LIB_Private3(void);
struct MsgPort * ASM LIB_Delf_StartNotifyA(REG(__a0) ULONG * );
void  ASM            LIB_Delf_EndNotify(REG(__a1) struct MsgPort * );
ULONG  ASM           LIB_Delf_GetAttr(REG(__d0) int , REG(__d1) int );
void  ASM            LIB_Delf_SetAttrsA(REG(__a0) struct TagItem * );
struct DelfModule * ASM LIB_Delf_AddModuleA(REG(__a0) struct TagItem * );
void  ASM            LIB_Delf_RemModule(REG(__a1) struct DelfModule * );
struct Node *  ASM   LIB_Delf_FindModule(REG(__a0) char * );
short  ASM           LIB_Delf_SetPipe(REG(__a0) struct DelfModule * , REG(__d0) ULONG , REG(__a1) struct DelfModule * , REG(__d1) long );
void  ASM            LIB_Delf_SetModAttrsA(REG(__a0) struct DelfModule * , REG(__a1) struct TagItem * );
struct Node *  ASM   LIB_Delf_AHIModule(REG(__a0) struct AHIAudioCtrlDrv * );

