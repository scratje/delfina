; check if we have ramsey or not. can be used to detect A1200 vs A4000
; returns 0 or ramsey revision

	xdef _ramsey
	xdef @ramsey


_LVOCacheClearE	equ	-$282
_LVODisable	equ	-$78
_LVOEnable	equ	-$7e

_ramsey
@ramsey
	move.l	4,a6
	lea	.null(pc),a0
	moveq     #4,d0
	move.l    #$800,d1 ;cleard
	jsr	_LVOCacheClearE(a6)	; make sure the null trick works
	jsr	_LVODisable(a6)	; disable

	move.l	a6,a0
	lea	$de0043,a1
	moveq	#1,d1

; we want these instructions to be in instr cache, 
; but .null not in data cache. So loop over them to
; cache them, then read null the second time.
;
; null must be read to make data bus float to 000000...
; otherwise $de0043 will contain garbage on A1200.
.loop
	move.l	(a0),d0
	move.b	(a1),d0
	lea	.null(pc),a0
	dbf	d1,.loop

	jsr	_LVOEnable(a6)

	cmp.w	#13,d0		; validity checking
	blo.b	.nochip
	cmp.w	#20,d0
	bhi.b	.nochip
	rts
.nochip
	moveq	#0,d0
	rts

	cnop	0,4
.null	dc.l	0


