#define __USE_SYSBASE 1

#include "libdata.h"
#include <proto/expansion.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include <clib/alib_protos.h>

#include <libraries/configvars.h>
#include <devices/timer.h>
#include <dos/dostags.h>
#include <dos/dosextens.h>
#include <intuition/intuition.h>
#include <hardware/intbits.h>
#include <libraries/delfina.h>
#include <libraries/ahi_sub.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dos.h>


#define CONSTVECBASE 0x200
#define CONSTVARBASE 0x200

#ifndef CONSTVECBASE
int vecbase;
#else
#define vecbase CONSTVECBASE
#endif

#ifndef CONSTVARBASE
int varbase;
#else
#define varbase CONSTVARBASE
#endif

#include "internal.h"

//#define X441

//#define DEBUG

#ifdef DEBUG
FILE *dfh;
#define DBG(text) {if (!intmode) {Forbid();Disable();fprintf(dfh,text);fflush(dfh);Permit();}}
#define DVAL(text,val) {if (!intmode) {Forbid();fprintf(dfh,text,val);fflush(dfh);Enable();Permit();}}
#else
#define DBG(text)
#define DVAL(text,val)
#endif

/* prefs variables */
struct dprefs prefs = {0x24b0, 0x400000, 0xc0f000 };
int oldcontrol=-1,prefsloaded,d1200,plus,flipper,localport,listsize,tempvar,inputsel=0,intdisable;
int nosem=0;

/* frequencies */

#ifndef X441
const int litefreq[] = {8000,-1,16000,-1,27429,-1,32000,-1,-1,-1,-1,-1,48000,-1,9600,-1};
const int litetable[] = {16000,27429,32000,48000};
int currfreq=48000;
#else
const int litefreq[] = {5510,-1,11025,-1,18900,-1,22050,-1,37800,-1,44100,-1,33075,-1,6620,-1};
const int litetable[] = {22050,33075,37800,44100};
int currfreq=44100;
#endif

//const int d1200freq[] = {6000,4000,12000,8000,20571,13714,24000,16000,41143,27429,48000,32000,36000,24000,7200,4800};
const int d1200freq[] = {6000,-1,12000,-1,20571,-1,24000,-1,41143,-1,48000,-1,36000,-1,7200,-1};
const int d1200table[] = {12000,24000,36000,48000};

const int flipfreq[] = {8000,5510,16000,11025,27429,18900,32000,22050,-1,37800,-1,44100,48000,33075,9600,6620};
const int fliptable[] = {22050,32000,44100,48000};

int *codecfreq = litefreq,*freqtable=litetable;

/* current codec frequency, overrange */
int inputclip=0, outputclip=0, extpipes=0,outpipes=0;


extern int LiteCode[1024];

volatile int memsize,dspbusy=0 ;
unsigned volatile char *base,*hostctl;
unsigned char spacetab[] = { 0x8,0x8,0x8,0x8,0x0,0x0,0x0,0x0 };
unsigned char spaces[]={1,2,4,8};

int cpupr=0,uptime[2]={0,0},olduptime[2]={0,0},dsp74=0;
double freetime=0,dspclock=4.0;

volatile int runres, prevsynch=1, runsig=0;
int currkey,mainsig=0,quitsig=0,wuhwuh=1;

volatile struct Task *runtask, *maintask=NULL;
struct Process *subtask=0;

struct Interrupt (*inthash[256]);
struct Interrupt intserver, runint, dogint;
struct SignalSemaphore boardsemaphore, memlistsemaphore, notifysemaphore, modulesemaphore, 
            intsemaphore, prgsemaphore;
struct ExecBase *SysBase;
struct Library *UtilityBase=NULL, *DelfinaBase;
struct IntuitionBase *IntuitionBase=NULL;
struct DosLibrary *DOSBase;

struct List ModList;
struct DelfModule *ExtWorld;

struct EasyStruct crashreq = {sizeof(struct EasyStruct),0,"Delfina watchdog",
           "%s","Restart|Continue"};
           
struct EasyStruct panicreq = {sizeof(struct EasyStruct),0,"Delfina watchdog",
           "%s","Continue"};
   
int intmode=FALSE,killhim=0;

int runbuffer[5*256],*runrd=runbuffer,*runwr=runbuffer,*runend=&runbuffer[5*256];

/* Delfina mem alloc stuff */
struct membl *XPlist, *Ylist;
struct membl IPlist[0x22], IXlist[0x101], IYlist[0x101];

/* global preferences, in tag form now! */
struct TagItem GlobalTags[] = {
   DA_InputGainL,        0,
   DA_InputGainR,        0,
   DA_OutputVolL,        65536,
   DA_OutputVolR,        65536,
   DA_MonitorVol,        0,
   DA_PassthruVol,        65536,
   DA_LineMixVol,        0,
   DA_MicMixVol,        0,
   DA_CDMixVol,        0,
   DA_MuteActive,        TRUE,
   DA_MixVol,      65536,
   DA_LineOut,                TRUE,
   DA_HeadphoneOut,0,
   DA_InputSel,        0,
   DA_HighLevel,        TRUE,
   DA_MicIsLine,        0,
   DA_HighPass,        TRUE, 0 };

struct TagItem DefaultTags[] = {
   DA_InputGainL,        0,
   DA_InputGainR,        0,
   DA_OutputVolL,        65536,
   DA_OutputVolR,        65536,
   DA_MonitorVol,        0,
   DA_PassthruVol,        65536,
   DA_LineMixVol,        0,
   DA_MicMixVol,        0,
   DA_CDMixVol,        0,
   DA_MuteActive,        TRUE,
   DA_MixVol,      65536,
   DA_LineOut,                TRUE,
   DA_HeadphoneOut,0,
   DA_InputSel,        0,
   DA_HighLevel,        TRUE,
   DA_MicIsLine,        0,
   DA_HighPass,        TRUE, 0 };


/* who owns notifylist owns these too */
struct notify *notifylist=NULL;
struct TagItem TempTags[64];
struct MsgPort *notifyport=NULL;

/* module stuff */
UWORD modbufs[BUFFERS];
DELFPTR iobuf,audiomodbuf=0,audiolist=0;
int modbufcnt=2;

void __regargs __chkabort(void);
void __regargs __chkabort(void) { }

/* autocfg madness */
struct boardinfo {
  UWORD manufacturer;
  UWORD product;
  UWORD offset;
  UWORD flags;
};

#define BI_LITE 1
#define BI_PLUS 2
#define BI_1200 4

// note, BI_1200 can be normal delfina or flipper

struct boardinfo boarddata[] = {
  { 14501,  1, 0,      BI_LITE },  // lite
  { 14501,  2, 0,      BI_PLUS },  // plus
  { 4626,  23, 0xa001, BI_1200 },  // xsurf clockport 1
  { 4626,  23, 0xc000, BI_1200 },  // xsurf clockport 2
  { 4626,  23, 0x9000, BI_1200 },  // xsurf 26pin port
  { 4626,   5, 0x8000, BI_1200 },  // isdn surfer clockport
  { 4626,   5, 0xa000, BI_1200 },  // isdn surfer 26pin port
  { 4626,   0,  0xe00, BI_1200 },  // buddha
  { 4626,  42,  0xe00, BI_1200 },  // catweasel z2
  { 4626,   7, 0x8000, BI_1200 },  // vario
  { 4626,  10, 0x8000, BI_1200 },  // KickFlash OS4
  { 2145, 200, 0x4000, BI_1200 },  // highway usb
  { 3643,  48, 0x4000, BI_1200 },  // Prisma Megamix
  { 2588, 124, 0x8000, BI_1200 },  // A1K ZorroLanIDE
  { 5191,  10, 0x4000, BI_1200 },  // Freeway USB
  { 0 }
};
  
 

/* 
 * malloc() and free() suck, so here are the new functions: 
 */

void *malloc(size_t size) 
{
   int *res;

   res=AllocMem(size+8,0);
   *res++=size+8;
   *res++;
   return((void *)res);
}

void free(void *addr)
{
   int *res;
   
   res=(int *)addr;
   res-=2;
   FreeMem(res,res[0]);
}

/* 
 * Some basic mem handling functions
 */

/* allocate and free memory bus */
//#define alloclite() if(!intmode) { ObtainSemaphore(&boardsemaphore);base[ICR]=0;}Forbid();Disable();
#define alloclite() if (!intmode){int u=0;while(!AttemptSemaphore(&boardsemaphore)){u++;if(u>10){nosem=1;break;}}base[ICR]=0;} Forbid();Disable();


//#define freelite() if (1) {base[ICR]=1; if(!intmode) ReleaseSemaphore(&boardsemaphore);}Permit();Enable();
#define freelite()base[ICR]=1;if(!intmode){ if (nosem==0){ ReleaseSemaphore(&boardsemaphore);}nosem=0;}Permit();Enable();



/******************************************/
/* Detect delfina model and set variables */
int detectdelf(void) {
   struct ConfigDev *mydev;
   struct Library *ExpansionBase;
   int x,found=0;

   struct boardinfo *bi;

   ExpansionBase=OpenLibrary("expansion.library",0);

   /* Test for IComp's Kickflash card - according to the docs, the clockport needs to be enabled before use */
   if( (mydev = FindConfigDev(NULL, 4626, 10)) ) {
      base = ((char*) mydev->cd_BoardAddr);

      *(base+0x007c) = 0xFF; /* Any write to the Clockport activation register will enable the clockport */
   }

/* Get board base address */
   bi=boarddata;
   while (bi->manufacturer && !found) {
     mydev=FindConfigDev(NULL,bi->manufacturer,bi->product);
     if (mydev) {
        base=((char*) mydev->cd_BoardAddr) + bi->offset;
        
        if (bi->flags & (BI_PLUS|BI_LITE)) {
          plus= (bi->flags & BI_PLUS) ? 1:0;
          hostctl=&base[0x40];
          found=1;
        } else {
          hostctl=&base[8];
          if (*hostctl == 0xb5) {
        		    found=d1200=1;
        			 } else{ 
				     if (*hostctl == 0xb6)found=d1200=flipper=1;}

            hostctl=&base[0x1c];  // check if we are on 26pin port
          if (*hostctl == 0xb6) 
                     found=d1200=flipper=localport=1;
             }   // else
      }
      bi++;
   }
        
   if (localport==1){
      base+=0x18;hostctl=&base[4];
      changeport(); 
      changeintport();
      /**warning** self modifing code
      ugly hack to change delfina register to 26pin localport on catweaselz2/buddha
      **warning** self modifing code**/
  	}
	else
   	hostctl=&base[8];

   if (!found) {
	   /* Search for Delfina on Hamag's "Dicke Olga" ;-) */
	   if (FindConfigDev(NULL, 2588, 208)) {
         if (*((unsigned char *)0xd90009) == 0xb5) {found=d1200=1;base=(char *)0xd90001; }
         else if (*((unsigned char *)0xd94009) == 0xb5) {found=d1200=1;base=(char *)0xd94001; }
         else if (*((unsigned char *)0xd98009) == 0xb5) {found=d1200=1;base=(char *)0xd98001; }
         else if (*((unsigned char *)0xd9c009) == 0xb5) {found=d1200=1;base=(char *)0xd9c001; }
         else if (*((unsigned char *)0xd90009) == 0xb6) {found=flipper=d1200=1;base=(char *)0xd90001; }
         else if (*((unsigned char *)0xd94009) == 0xb6) {found=flipper=d1200=1;base=(char *)0xd94001; }
         else if (*((unsigned char *)0xd98009) == 0xb6) {found=flipper=d1200=1;base=(char *)0xd98001; }
         else if (*((unsigned char *)0xd9c009) == 0xb6) {found=flipper=d1200=1;base=(char *)0xd9c001; }
	   }

      if (!ramsey()) {  // if we don't have a ramsey, we are A1200, check for clockport
         if (*((unsigned char *)0xd80009) == 0xb5)      {found=d1200=1;base=(char *)0xd80001; }
         else if (*((unsigned char *)0xd84009) == 0xb5) {found=d1200=1;base=(char *)0xd84001; }
         else if (*((unsigned char *)0xd88009) == 0xb5) {found=d1200=1;base=(char *)0xd88001; }
         else if (*((unsigned char *)0xd8c009) == 0xb5) {found=d1200=1;base=(char *)0xd8c001; }
         else if (*((unsigned char *)0xd90009) == 0xb5) {found=d1200=1;base=(char *)0xd90001; }
         else if (*((unsigned char *)0xd80009) == 0xb6) {found=flipper=d1200=1;base=(char *)0xd80001; }
         else if (*((unsigned char *)0xd84009) == 0xb6) {found=flipper=d1200=1;base=(char *)0xd84001; }
         else if (*((unsigned char *)0xd88009) == 0xb6) {found=flipper=d1200=1;base=(char *)0xd88001; }
         else if (*((unsigned char *)0xd8c009) == 0xb6) {found=flipper=d1200=1;base=(char *)0xd8c001; }
         else if (*((unsigned char *)0xd90009) == 0xb6) {found=flipper=d1200=1;base=(char *)0xd90001; }
     }
   }

   hostctl = &base[8];

   CloseLibrary(ExpansionBase);

   if (!found) return 1;  // failed
   
   /* plus and flipper are 74MHz. Older delf1200 and lite are not. detect d1200 version.. */
   if (plus || flipper) dsp74=1;
   else if (d1200) {
     *hostctl=HC_QUERYVER;
     x=*hostctl;
     *hostctl=0;
  
     if      (x==0) dsp74=1;
     else if (x!=0xb5) return(1);
   }

   if (flipper || d1200) plus=1;
 
   if (plus) {
    if (flipper) {
      codecfreq=flipfreq;
      freqtable=fliptable;
    } else {
      codecfreq=d1200freq;
      freqtable=d1200table;
    }
    
    
    dspclock=3.3868;		//Prototyp Flipper war 3.6864;
    if (dsp74) dspclock*=2;
    prefs.control=0x24a8;
   }
 
 //  if (!plus) base[0x44]=0;
   *hostctl=HC_RESET;
   Delay(1);
   *hostctl=0;
       
   return 0;
}        
   
ASM  int __UserLibInit(REG(__a6) struct DelfinaBase *libbase)
{
   int x;

   SysBase = *(struct ExecBase **)4;
   DelfinaBase=(struct Library *)libbase;

/* Open libraries */
   UtilityBase=OpenLibrary("utility.library",37);
   DOSBase=(struct DosLibrary *)OpenLibrary("dos.library",37);
   IntuitionBase=(struct IntuitionBase *)OpenLibrary("intuition.library",37);
   if (!(UtilityBase && DOSBase && IntuitionBase)) {
      CloseLibrary(UtilityBase);
      CloseLibrary((struct Library *)DOSBase);
      CloseLibrary((struct Library *)IntuitionBase);
      return 1;
   }
   
   if (detectdelf()) {
      CloseLibrary(UtilityBase);
      CloseLibrary((struct Library *)DOSBase);
      CloseLibrary((struct Library *)IntuitionBase);
      return 1;
   }


#ifdef DEBUG
   dfh=fopen("con:10/300/400/300/Output/WAIT/AUTO/CLOSE","wb");
   DBG("Library opened.\n");
   DVAL("Base address: 0x%lx\n", base);
   DVAL("Plus: %ld\n", plus);
   DVAL("1200: %ld\n", d1200);
   DVAL("Flip: %ld\n", flipper);

#endif    

/* set up interrupt server */
   memset(&intserver,0,sizeof(struct Interrupt));
   (&intserver)->is_Node.ln_Type=NT_INTERRUPT;
   (&intserver)->is_Node.ln_Name="delfina.library";
   (&intserver)->is_Data=&intmode;
   (&intserver)->is_Code=LiteInt;
   if (d1200) {
      (&intserver)->is_Node.ln_Pri=0;
      AddIntServer(INTB_EXTER,&intserver);
   } else {
      (&intserver)->is_Node.ln_Pri=127;
      AddIntServer(INTB_PORTS,&intserver);
   }
   
   DBG("Added interrupt server.\n");

/* setup lists */
   NewList(&ModList);
   
/* set up runprg interrupt */
   memset(&runint,0,sizeof(struct Interrupt));
   (&runint)->is_Node.ln_Type=NT_INTERRUPT;
   (&runint)->is_Node.ln_Pri=-127;
   (&runint)->is_Data=0;
   (&runint)->is_Code=LiteRunInt;
   
/* set up watchdog interrupt */
   memset(&dogint,0,sizeof(struct Interrupt));
   (&dogint)->is_Node.ln_Type=NT_INTERRUPT;
   (&dogint)->is_Node.ln_Pri=-127;
   (&dogint)->is_Data=0;
   (&dogint)->is_Code=DogInt;
   
   DBG("Delfina interrupts initialized\n");

/* set up semaphores */
   memset(&notifysemaphore,0,sizeof(struct SignalSemaphore));
   memset(&modulesemaphore,0,sizeof(struct SignalSemaphore));
   memset(&boardsemaphore,0,sizeof(struct SignalSemaphore));
   memset(&memlistsemaphore,0,sizeof(struct SignalSemaphore));
   memset(&intsemaphore,0,sizeof(struct SignalSemaphore));
   memset(&prgsemaphore,0,sizeof(struct SignalSemaphore));
   InitSemaphore(&notifysemaphore);
   InitSemaphore(&modulesemaphore);
   InitSemaphore(&boardsemaphore);
   InitSemaphore(&memlistsemaphore);
   InitSemaphore(&intsemaphore);
   InitSemaphore(&prgsemaphore);
   
   DVAL("modulesemaphore: $%x\n",&modulesemaphore);
   DVAL("notifysemaphore: $%x\n",&notifysemaphore);
   DVAL("boardsemaphore: $%x\n",&boardsemaphore);
   DVAL("memlistsemaphore: $%x\n",&memlistsemaphore);
   DVAL("intsemaphore: $%x\n",&intsemaphore);
   DVAL("prgsemaphore: $%x\n",&prgsemaphore);

/* interrupt hash table */
   runrd=runwr=runbuffer;
   runtask=NULL;
   memset(&inthash,0,256*4); 
   currkey=0x10000;
   LIB_Delf_AddIntServer(1,&runint);
   LIB_Delf_AddIntServer(3,&dogint);
   
   DBG("Delfina interrupts added.\n");

/* Load and start delfina kernel */
   DBG("Init test\n");
   Disable();
   for(x=0;x<100;x++)  *hostctl=HC_RESET|HC_IRQDIS;        /* reset */
   for(x=0;x<1000;x++) *hostctl=0;                          /* reset */
   while (base[ICR] & 0x80);
   base[ICR]=0;
   Enable();

   DBG("Init test passed\n");

   for (x=0;x<0x480;x++) {
     if (!(x&15)) DVAL("boot: %d\n",x);
     put24(LiteCode[x]);
   }
           
   put24(0);
   if (flipper) {
     put24(0x361005);
   } else if (dsp74) {
     put24(0x361013);
   } else { 
     put24(0);
   }

   base[ICR]=0x1;                     /* enable receive int */
 //  if (!plus) base[0x44]=0x80;
//   *hostctl=HC_HENCLK;                /* enable HEN clocking, used in some Lites */

   DBG("Kernel started\n");
   
/* check how much memory we have */
   memsize=LIB_Delf_Peek(0x2e5,DMEMF_YDATA);
   
   switch (memsize) {
     case 128:
       listsize=0x10000/D_MINALLOC;
       break;
                   
     case 32:
       listsize=0x4400/D_MINALLOC;
       break;

     default:
       DVAL("Invalid mem size %d\n",memsize);
       __UserLibCleanup();
       return(1);                
   }
           
/* Allocate memory for mem lists */        
   XPlist=AllocMem(listsize*sizeof(struct membl),MEMF_CLEAR|MEMF_PUBLIC);
   Ylist=AllocMem(listsize*sizeof(struct membl),MEMF_CLEAR|MEMF_PUBLIC);
   if (!XPlist || !Ylist) {
      __UserLibCleanup();
      return 1;
   }        
   
   
/* set up internal memory alloc */        
   IPlist[0].size=0x1;
   IPlist[0].next=0x21;
   IPlist[0x21].size=1;
   IPlist[0x21].next=0;
   IXlist[0].size=0x80;
   IXlist[0].next=0x100;
   IXlist[0x100].size=1;
   IXlist[0x100].next=0;
   IYlist[0].size=0x80;
   IYlist[0].next=0x100;
   IYlist[0x100].size=1;
   IYlist[0x100].next=0;
   
/* set up memory alloc and allocate kernel variables */
   switch(memsize) {        
     case 128:
       XPlist[0].size=0x400/D_MINALLOC;
       XPlist[0].next=0xfc00/D_MINALLOC;
       XPlist[0xfc00/D_MINALLOC].next=0;
       XPlist[0xfc00/D_MINALLOC].size=(0x10000-0xfc00)/D_MINALLOC;

       Ylist[0].size=0x200/D_MINALLOC;                /* same for Y */
       Ylist[0].next=0xfc00/D_MINALLOC;
       Ylist[0xfc00/D_MINALLOC].next=0;
       Ylist[0xfc00/D_MINALLOC].size=(0x10000-0xfc00)/D_MINALLOC;
       break;
         
     case 32:
       XPlist[0].size=0x400/D_MINALLOC;
       XPlist[0].next=0x4200/D_MINALLOC;
       XPlist[0x4200/D_MINALLOC].next=0;
       XPlist[0x4200/D_MINALLOC].size=(0x10000-0x4200)/D_MINALLOC;

       Ylist[0].size=0x300/D_MINALLOC;                /* same for Y */
       Ylist[0].next=0x42c0/D_MINALLOC;
       Ylist[0x42c0/D_MINALLOC].next=0;
       Ylist[0x42c0/D_MINALLOC].size=(0x10000-0x42c0)/D_MINALLOC;
   }        
   
/* allocate some buffers for audio.. */        
   iobuf=LIB_Delf_AllocMem(512,DMEMF_LDATA|DMEMF_ALIGN_512);
   modbufs[0]=LIB_Delf_AllocMem(128,DMEMF_LDATA|DMEMF_ALIGN_128);
   
   DVAL("iobuf: $%x\n",iobuf);
   DBG("Audio buffers allocated\n");        
   
/* setup ExtWorld */
   { ULONG tags[]= {DM_Inputs,48,DM_Outputs,1,DM_Code,delf_extworld,DM_NoPipes,TRUE,
                    DM_ShareIO,0,DM_Name,(ULONG)"Connectors",0};
      ExtWorld=LIB_Delf_AddModuleA((struct TagItem *)tags);
      ExtWorld->freq=0;
   }

   DBG("Extworld added\n");
   
/* create subtask */
   if (ExtWorld) subtask=CreateNewProcTags(        NP_Entry, subtaskcode, 
                                                   NP_Name, "delfina.library",
                                                   NP_Priority, 13, 
                                                   NP_WindowPtr, 0, 0);

   if(!subtask) {
quit_here:        
           if (ExtWorld) FreeMem(ExtWorld,ExtWorld->len);
           FreeMem(XPlist, listsize*sizeof(struct membl));
           FreeMem(Ylist, listsize*sizeof(struct membl));
           RemIntServer(d1200? INTB_EXTER:INTB_PORTS,&intserver);
           CloseLibrary(UtilityBase);
           CloseLibrary((struct Library *)DOSBase);
           CloseLibrary((struct Library *)IntuitionBase);
           return(1);
   }

   DBG("Subtask started\n");

/* sort mods */
   ObtainSemaphore(&modulesemaphore);
   sortmods();
   ReleaseSemaphore(&modulesemaphore);

   DBG("Mods sorted\n");

/* wait for notifyport.. */        
   while (!prefsloaded) Delay(2);
   
   DBG("STARTED!\n");
   return(0);
}        

ASM  void __UserLibCleanup(void)
{
   int mainbit;
   
   DBG("Starting cleanup...\n");

   if (!killhim && subtask) {
     killhim=TRUE;
     if ((mainbit=AllocSignal(-1))!=-1) 
     {
        maintask=FindTask(NULL);
        mainsig=1<<mainbit;
        DBG("Signaling task..\n");
        Signal((struct Task *)subtask,quitsig);
        DBG("Waiting..\n");
        Wait(mainsig);
        DBG("Freeing sigbit.\n");
        FreeSignal(mainbit);
     } else {
        DBG("no sigbit..\n");
        maintask=NULL;
        Signal((struct Task *)subtask,quitsig);
        while (!killhim) Delay(10);
     }                
   }
      
  base[ICR]=0;
  *hostctl=HC_IRQDIS;
 // if (!plus) base[0x44]=0;
   
  if (ExtWorld) FreeMem(ExtWorld,ExtWorld->len);                /* can't use Delf_RemModule() */
  if (XPlist)   FreeMem(XPlist, listsize*sizeof(struct membl));
  if (Ylist)    FreeMem(Ylist, listsize*sizeof(struct membl));
   
  RemIntServer(d1200? INTB_EXTER:INTB_PORTS,&intserver);

#ifdef DEBUG
  fprintf(dfh,"Closed!!!\n");
  fclose(dfh);
  dfh=NULL;
#endif
  CloseLibrary(UtilityBase);
  CloseLibrary((struct Library *)DOSBase);
  CloseLibrary((struct Library *)IntuitionBase);
}


ASM  int __UserLibOpen(REG(__a6) struct DelfinaBase *libbase)
{        return(0);
}


ASM  int __UserLibClose(REG(__a6) struct DelfinaBase *libbase)
{        return(0);
}



/************************************
 * Subtask - a watchdog 
 *
 */

void  subtaskcode(void)
{
   int choice,quitbit,sigmask,signals,err;
   struct timerequest *timeio;
   struct MsgPort *timeport;
   struct notify *not;
   char *str;
   int paniccnt=0;
   int x,newfreq;
   BPTR fh;
   int n=0;
   ULONG *ptr=(ULONG *)GlobalTags;
   
   timeport=CreateMsgPort();
   timeio=CreateIORequest(timeport,sizeof(struct timerequest));
   
   if (timeio) {
     if (OpenDevice("timer.device",UNIT_VBLANK,(struct IORequest *)timeio,0)) {
       DeleteIORequest(timeio);
       DeleteMsgPort(timeport);
       killhim=1;
       return(0);
     }
   } else {
     DeleteMsgPort(timeport);
     killhim=1;
     return(0);
   }        
   
   notifyport=CreateMsgPort();
   if (!notifyport) {
     DeleteIORequest(timeio);
     DeleteMsgPort(timeport);
     killhim=1;
     return(0);
   }
   
   quitbit=AllocSignal(-1);
   quitsig=1<<quitbit;
   
   signals= (1<<timeport->mp_SigBit);
   sigmask=quitsig | signals | (1<<notifyport->mp_SigBit);

   Delay(10);
   loadprefs("ENV:DelfinaPrefs");
   prefsloaded=1;
   fh=Open("ENV:DelfinaPrefs",MODE_NEWFILE);                                /* open prefs file here.. */

   DBG("Starting watchdog\n");

   LIB_Delf_Run(delf_watchdog,0,0,0,0,0,0);        /* call once */

   timeio->tr_node.io_Command=TR_ADDREQUEST;
   timeio->tr_time.tv_secs=1;
   timeio->tr_time.tv_micro=0;

   SendIO((struct IORequest *)timeio);
   
   /* Hack to calibrate codec */
   LIB_Delf_Run(delf_resetcodec,0,0,0,0,0,0);
   { ULONG tags[] = {DA_CodecFreq,16000,0}; LIB_Delf_SetAttrsA((struct TagItem *)tags);}
   { ULONG tags[] = {DA_CodecFreq,48000,0}; LIB_Delf_SetAttrsA((struct TagItem *)tags);}

   DBG("Subtask mainloop starting.\n");

   while (!(signals & quitsig) && !killhim) {
     if (killhim) break;
     if (signals & (1<<timeport->mp_SigBit)) {
           
/* start new watchdog timer */
        WaitIO((struct IORequest*)timeio);

        timeio->tr_node.io_Command=TR_ADDREQUEST;
        timeio->tr_time.tv_secs=0;
        timeio->tr_time.tv_micro=500000;

        SendIO((struct IORequest *)timeio);
                   
        if (uptime[1]) {
          uptime[0]++;
          uptime[1]=0;
        } else {
          uptime[1]=500000;
        }
                   
/* check for cpu panic */
        if (LIB_Delf_Peek(y_cpupanic,DMEMF_YDATA)) {
          paniccnt++;
          LIB_Delf_Poke(y_cpupanic,DMEMF_YDATA,0);
          if (paniccnt==2) {
            paniccnt=0;
            if (currfreq==freqtable[0]) {
              CheckNotify(DA_CPUPanic,TRUE,0);
              EasyRequest(NULL,&panicreq,0,
                "Running low of CPU power on Delfina.\n"
                "Please quit some audio programs.");
              CheckNotify(DA_CPUPanic,FALSE,0);
            } else {
              x=FREQUENCIES-1;
              while (freqtable[x]!=currfreq) x--;
              newfreq=freqtable[x-1];
              Delf_SetAttrs(DA_CodecFreq,newfreq,0);
            }
          }
        }
   
/* check for overrange */
        x=inputclip;
        inputclip=(LIB_Delf_Peek(y_rddata2,DMEMF_YDATA) & DF2_OVR) ? 1:0;
        if (x!=inputclip) CheckNotify(DA_InputClip,inputclip,0);
        x=outputclip;
        outputclip=LIB_Delf_Peek(y_outovr,DMEMF_YDATA);
        if (x!=outputclip) CheckNotify(DA_OutputClip,outputclip,0);
        if (outputclip) LIB_Delf_Poke(y_outovr,DMEMF_YDATA,0);

        if (inputclip ||�outputclip) LIB_Delf_Run(delf_watchdog,0,0,0,0,0,0);
      }

/* handle notify stuff */        
      while (not=(struct notify *)GetMsg(notifyport)) {
        if (not->flags & NF_ALLOC) {
          FreeMem(not,not->len);
        } else {
          not->flags=0;
        }
      }                
      signals=Wait(sigmask);
   }
   
exitsub:
   DBG("save prefs\n");

   DBG("calculating tags... \n");
   while(ptr[n]) {
     n+=2;
     DVAL("count: %d\n",n/2);
   }
   
   DBG("Writing file\n");
   Write(fh,ptr,n*4+4);
   DBG("Closing file\n");
   Close(fh);
   DBG("Prefs written\n");

   DBG("Aborting timeio\n");
   if (!CheckIO((struct IORequest *)timeio)) {
     AbortIO((struct IORequest *)timeio);
     WaitIO((struct IORequest *)timeio);
   }
   DBG("Closedevice\n");
   CloseDevice((struct IORequest *)timeio);
   DBG("Del IOREQ\n");
   DeleteIORequest(timeio);
   DeleteMsgPort(timeport);
   DBG("free quitsig\n");
   FreeSignal(quitbit);
   DBG("DEleteport\n");
   DeleteMsgPort(notifyport);

   DBG("signaling maintask\n");
   if (maintask) Signal(maintask,mainsig);
   DBG("subtask exiting\n");
   killhim=TRUE;
}

   
/*
 * Memory copy functions
 */
ASM  ULONG LIB_Delf_Peek(        REG(__a1) DELFPTR addr,
                                 REG(__d0) ULONG space )
{
   int r,e,x;

   alloclite();
   
   for (x=0;x<5;x++) {
     startDA(v_copyDA,addr | ((space & DMEMF_YDATA) ? 0x800000 : 0));
     r=get24();
     e=endDA() & 0xffff;
     if (e-addr == 2 || plus) break;
   }
   freelite();
   return(r);
}


ASM  void LIB_Delf_Poke(        REG(__a1) DELFPTR addr,
                                REG(__d0) ULONG space,
                                REG(__d1) ULONG val )
{
   register int e,x;

   alloclite();
   for(x=0;x<5;x++) {
     startAD(v_copyAD,addr | ((space & DMEMF_YDATA) ? 0x800000 : 0));
     put24(val);
     e=endAD() & 0xffff;
     if (e-addr == 1 || plus) break;
   }
   freelite();
}

/*
 * Delf_CopyMem -- Copy memory between Delfina and Amiga
 */

ASM  void LIB_Delf_CopyMem(        REG(__a0) int from,
                                   REG(__a1) int to,
                                   REG(__d0) ULONG size,
                                   REG(__d1) ULONG mode )
{
   int vec,e,x,s;
   DELFPTR delf;
   APTR amy;
   
   alloclite();

   DBG("Copy start\n");

   if (plus && !d1200) {
     intdisable=1;
     for (x=0;x<5;x++) {
       if (mode & DCPF_TO_AMY) {
           delf=((DELFPTR)from) | ((mode & DMEMF_YDATA) ? 0x800000 : 0);
           amy=(APTR)to;
           if (mode & DMEMF_LDATA) vec=v_lcopyDA; else vec=v_copyDA;
           if ((mode & DCPF_METHOD_MASK) == DCPF_16BITH) vec+=(v_copyADh-v_copyAD);
           startDA(vec,delf);
           
           switch(mode & DCPF_METHOD_MASK) {
               case DCPF_16BITH:
               case DCPF_16BITL:
                  liteda16(amy,size); s=(size+1)>>1; break;
                  
               case DCPF_32BITH:
                  liteda32h(amy,size); s=(size+3)>>2; break;
                  
               case DCPF_32BITL:
                  liteda32l(amy,size);s=(size+3)>>2; break;
                  
               case DCPF_8BIT:
                  liteda8(amy,size);  s=size; break;
                  
               case DCPF_24BIT:
                  liteda24(amy,size); s=(size+2)/3; break;
           }
           e=endDA()-1;
       } else {
           delf=((DELFPTR)to) | ((mode & DMEMF_YDATA) ? 0x800000 : 0);
           amy=(APTR)from;
           if (mode & DMEMF_LDATA) vec=v_lcopyAD; else vec=v_copyAD;
           if ((mode & DCPF_METHOD_MASK) == DCPF_16BITH) vec+=(v_copyADh-v_copyAD);
           startAD(vec,delf);
           
           switch(mode & DCPF_METHOD_MASK) {
               case DCPF_16BITH:
                  if (mode & DMEMF_LDATA) {
                     plusad16(amy,size+2);
                     s=(size+3)>>1;
                  } else {
                     plusad16(amy,size);
                     s=(size+1)>>1;
                  }
                  break;
                  
               case DCPF_32BITH:
                  plusad32h(amy,size); s=(size+3)>>2; break;
                  
               case DCPF_32BITL:
                  plusad32l(amy,size);s=(size+3)>>2; break;

               case DCPF_16BITL:
                   plusad16(amy,size); s=(size+1)>>1; break;
                   
               case DCPF_8BIT:
                   plusad8(amy,size);  s=size; break;
                   
               case DCPF_24BIT:
                   plusad24(amy,size); s=(size+2)/3; break;
           }
           e=endAD();
       }

       if (mode & DMEMF_LDATA) {
         delf<<=1;
         e<<=1;
         if ((!(mode & DCPF_16BITH)) && (mode & DCPF_TO_AMY)) e++;
         if (e & 0x1000000) {
            e++;
            DBG("Odd  ");
         }
       }

       e=(e & 0x1ffff)-(delf & 0x1ffff);
       if (e==s) break;
       DVAL("Copymode $%x, ",mode);
       DVAL("size: %d, ",size/s);
       DVAL("Bug: is $%x, ",e);
       DVAL("should be $%x\n",s);
     }
     intdisable=0;
   } else {
     for (x=0;x<5;x++) {
       if (mode & DCPF_TO_AMY) {
           DVAL("Copying to amiga, mode 0x%lx\n",mode);
           delf=((DELFPTR)from) | ((mode & DMEMF_YDATA) ? 0x800000 : 0);
           amy=(APTR)to;
           if (mode & DMEMF_LDATA) vec=v_lcopyDA; else vec=v_copyDA;
           if ((mode & DCPF_METHOD_MASK) == DCPF_16BITH) vec+=(v_copyADh-v_copyAD);
           startDA(vec,delf);
           DBG("Start ok\n");
           
           switch(mode & DCPF_METHOD_MASK) {
               case DCPF_16BITH:
               case DCPF_16BITL:
                  liteda16(amy,size); s=(size+1)>>1; break;
                  
               case DCPF_32BITH:
                  liteda32h(amy,size); s=(size+3)>>2; break;
                  
               case DCPF_32BITL:
                  liteda32l(amy,size);s=(size+3)>>2; break;
                  
               case DCPF_8BIT:
                  liteda8(amy,size);  s=size; break;
                  
               case DCPF_24BIT:
                  liteda24(amy,size); s=(size+2)/3; break;
           }
           DBG("Copy ok\n");
           e=endDA()-1;
       } else {
           DVAL("Copying to delfina, mode 0x%lx\n",mode);
           delf=((DELFPTR)to) | ((mode & DMEMF_YDATA) ? 0x800000 : 0);
           amy=(APTR)from;
           if (mode & DMEMF_LDATA) vec=v_lcopyAD; else vec=v_copyAD;
           if ((mode & DCPF_METHOD_MASK) == DCPF_16BITH) vec+=(v_copyADh-v_copyAD);
           startAD(vec,delf);
           DBG("Start ok\n");
           
           switch(mode & DCPF_METHOD_MASK) {
               case DCPF_16BITH:
                  if (mode & DMEMF_LDATA) {
                     litead16(amy,size+2);
                     s=(size+3)>>1;
                  } else {
                     litead16(amy,size);
                     s=(size+1)>>1;
                  }
                  break;
                  
               case DCPF_32BITH:
                  litead32h(amy,size); s=(size+3)>>2; break;
                  
               case DCPF_32BITL:
                  litead32l(amy,size);s=(size+3)>>2; break;

               case DCPF_16BITL:
                  litead16(amy,size); s=(size+1)>>1; break;
                   
               case DCPF_8BIT:
                  litead8(amy,size);  s=size; break;
                   
               case DCPF_24BIT:
                  litead24(amy,size); s=(size+2)/3; break;
           }
           DBG("Copy ok\n");
           e=endAD();
       }

       if (mode & DMEMF_LDATA) {
          delf<<=1;
          e<<=1;
          if ((!(mode & DCPF_16BITH)) && (mode & DCPF_TO_AMY)) e++;
          if (e & 0x1000000) {
            e++;
            DBG("Odd  ");
          }
       }

       e=(e & 0x1ffff)-(delf & 0x1ffff);
       if (e==s) break;
       DVAL("Copymode $%x, ",mode);
       DVAL("size: %d, ",size/s);
       DVAL("Bug: is $%x, ",e);
       DVAL("should be $%x\n",s);
     }
   }

   freelite();

   DBG("COPY end\n");
}

/************************************************
 *       Delfina programs
 */



/* A private function to load program to delfina memory.
 * returns DelfPrg structure if succeeded
 */
struct DelfPrg *LoadLite(struct DelfPrg *delfprg, struct DelfObj *obj)
{
   int memspace[4],n,a,b,c,addr,cnt,e,bakc;
   int *data=(int *)obj, *prg=(int *)delfprg,*bak;
   int id[4] = {0x50524F47, 0x58444154, 0x59444154, 0x4C444154 };
   
   if (data[0]!=0x44454C46 || data[1]!=0x42696E31)        /* DelfPrg1 ? */
      return(NULL);
   
   DBG("Header ok\n");
   
   for(n=0;n<4;n++) memspace[n]=data[n+2]&0xffff;
   data+=6;

   alloclite();
   
   for (n=0;n<3;n++) {
     if (*data++!=id[n]) {
       freelite();
       DVAL("space %d ID invalid\n",n);
       return(NULL);
     }

     DVAL("Starting space %d\n",n);
     addr=prg[n];
     if (n==2) addr|=0x800000;

     bak=data;
     cnt=0;
     bakc=c=memspace[n];
     if (c>0) startAD(v_copyAD,addr);
     while (c>0) {
       a=*(data++);
       b=a>>24;
       if (b<0) {
         b=-b+1;
         c-=b;
         for (b;b;b--) { put24(a); cnt++; }
       } else {
         c--;
         if (b) a+=prg[b-1];
         put24(a);cnt++;
       }
       if (cnt>=256 || c<=0) {
         e=endAD() - (addr & 0xffff);
         if (e == cnt || plus) {
           DBG("block ok\n");
           addr+=cnt;
           bak=data;
           bakc=c;
         } else {
           DVAL("block failed, e: %d ",e);
           DVAL("cnt: %d\n",cnt);
           data=bak;
           c=bakc;
         }
         cnt=0;
         if (c>0) startAD(v_copyAD,addr);
       }
     }
   }

   /* special case for L data */
   if (*data++!=id[n]) {
     freelite();
     DVAL("space %d ID invalid\n",n);
     return(NULL);
   }


   DVAL("Starting space %d\n",n);
   addr=prg[n];

   bak=data;
   cnt=0;
   bakc=c=memspace[n]*2;
   if (c>0) startAD(v_lcopyAD,addr);
   while (c>0) {
     DVAL("c: %d\n",c);
     a=*(data++);
     b=a>>24;
     if (b<0) {
       b=-b+1;
       c-=b;
       DVAL("b: %d\n",b);
       for (b;b;b--) { put24(a); cnt++; }
     } else {
       c--;
       if (b) a+=prg[b-1];
       put24(a);cnt++;
     }
     if ((cnt>=256 && !(cnt & 1)) || c<=0) {
       e=endAD()<<1;
       if (e & 0x1000000) e++;
       e=(e & 0x1ffff)-(addr<<1 & 0x1ffff);

       if (e == cnt || plus) {
         DBG("Block ok\n");
         addr+=(cnt>>1);
         bak=data;
         bakc=c;
       } else {
         DVAL("block failed, e: %d ",e);
         DVAL("cnt: %d\n",cnt);
         data=bak;
         c=bakc;
       }
       cnt=0;
       if (c>0) startAD(v_lcopyAD,addr);
     }
   }
   
   DBG("Done!\n");
   freelite();
   return(delfprg);
}

/*
 * Allocate memory for a dsp program, and relocate it
 */
ASM  struct DelfPrg *LIB_Delf_AddPrg(REG(__a1) struct DelfObj *obj)
{
   int n,flags[4],size[4];
   int *data= (int *)obj;
   DELFPTR *prg;

   DBG("AddPrg started\n");
   if (data[0]!=0x44454C46 || data[1]!=0x42696E31)        /* DelfPrg1 ? */
     return(NULL);

   DBG("Data header found\n");
   data+=2;
   for (n=0;n<4;n++) {
     flags[n]=(data[n] & DMEMF_ALIGN_MASK) | spaces[n];
     size[n] = data[n] & 0xFFFF;
   }
   prg=malloc(sizeof(struct DelfPrg));        
   if (!prg) return(NULL);

   DBG("DelfPrg allocated\n");
   DVAL("P size: $%x\n",size[0]);
   DVAL("X size: $%x\n",size[1]);
   DVAL("Y size: $%x\n",size[2]);
   DVAL("L size: $%x\n",size[3]);

   memset(prg,0,sizeof(struct DelfPrg));
   
   if (size[3]>size[2]) {
      if (size[3]>size[1]) {
         prg[3]=LIB_Delf_AllocMem(size[3],flags[3]);
         prg[1]=LIB_Delf_AllocMem(size[1],flags[1]);
      } else {
         prg[1]=LIB_Delf_AllocMem(size[1],flags[1]);
         prg[3]=LIB_Delf_AllocMem(size[3],flags[3]);
      }
      prg[2]=LIB_Delf_AllocMem(size[2],flags[2]);
   } else {    
      prg[2]=LIB_Delf_AllocMem(size[2],flags[2]);
      if (size[3]>size[1]) {
         prg[3]=LIB_Delf_AllocMem(size[3],flags[3]);
         prg[1]=LIB_Delf_AllocMem(size[1],flags[1]);
      } else {
         prg[1]=LIB_Delf_AllocMem(size[1],flags[1]);
         prg[3]=LIB_Delf_AllocMem(size[3],flags[3]);
      }
   }      
   prg[0]=LIB_Delf_AllocMem(size[0],flags[0]);
   
   for (n=3;n>=0;n--) {
      if (size[n] && !prg[n]) {
         DVAL("Alloc %d failed\n",n);
         LIB_Delf_RemPrg((struct DelfPrg *)prg);
         return(NULL);
      }
   }
   
      if (!LoadLite((struct DelfPrg *)prg,obj)) {
         DBG("LoadLite failed\n");
         LIB_Delf_RemPrg((struct DelfPrg *)prg);
         return(NULL);
      }
   DBG("Success\n");
   return((struct DelfPrg *)prg);
}

/*
 * Remove a delfina program. Free all delfina memory and
 * DelfPrg structure
 */
ASM  void LIB_Delf_RemPrg( REG(__a1) struct DelfPrg *prg)
{
   int n;
   
   for (n=0;n<4;n++) LIB_Delf_FreeMem(((ULONG *)prg)[n],spaces[n]);
   free(prg);
}

/* 
 * Run a program 
 */
ASM  ULONG LIB_Delf_Run(REG(__a1) DELFPTR addr, REG(__d0) LONG pri,
                        REG(__d1) ULONG mode, REG(__d2) ULONG x1,
                        REG(__d3) ULONG x0,        REG(__d4) ULONG y1,
                        REG(__d5) ULONG y0)
{
   int sigbit,res;
   
   if (mode & DRUNF_ASYNCH) {         /* asynchronous */
      runprg((DELFPTR)(0x80000000 | (int)addr),x1,x0,y1,y0);
      return(0);
   } else {                   /* synchronous */
      DBG("Run start\n");
      sigbit=AllocSignal(-1);
      if (sigbit!=-1) {
         DBG("Getting semaphore\n");
         ObtainSemaphore(&prgsemaphore);
         DVAL("OK, got it.. Calling address 0x%lx\n",addr);
         runsig=1<<sigbit;
         runtask=FindTask(NULL);
         runprg(addr,x1,x0,y1,y0);
         DBG("Wait for return\n");
         Wait(runsig);
         res=runres;
         ReleaseSemaphore(&prgsemaphore);
         FreeSignal(sigbit);
         DBG("Run finished\n");
      }
      return(res);
   }
}


void runprg(DELFPTR addr, ULONG x1, ULONG x0, ULONG y1, ULONG y0)
{
   int buf[6];
   
   Disable();
   if (dspbusy) {
      runwr[0]=(int)addr;     /* add to the wait list.. */
      runwr[1]=x1;runwr[2]=x0;runwr[3]=y1;runwr[4]=y0;
      runwr+=5;
      if (runwr==runend) runwr=runbuffer;
      Enable();
      DVAL("Queue,     runwr=$%x\n",runwr-runbuffer);
      DVAL("     runbuffer=$%x\n",runbuffer);
   } else {
      dspbusy++;
      Enable();
      buf[0]=x1;buf[1]=x0;buf[2]=y1;buf[3]=y0;buf[4]=addr;buf[5]=(((ULONG)addr)>>23);
      LIB_Delf_CopyMem((int)buf,y_runx1,6*4,DCPF_YDATA|DCPF_32BIT);     
      prevsynch=buf[5];
         alloclite();
         runvec(v_initrun);
         runvec(v_runprg);
         freelite();
   }
}



/*** Memory allocation. ***
 *
 */

/* Allocate memory */
ASM  DELFPTR LIB_Delf_AllocMem(         REG(__d0) ULONG len,
                              REG(__d1) ULONG flags)
{
   static int alignsize[16] = { 0,0,0,0,0,0,1,3,7,15,31,63,127,255,511,1023 };
   
   int align,space,ind=0,indy=0,start,starty,stopx,stop,size;
   DELFPTR addr=0;
   struct membl *mem;
   
   if (!len) return(0);

   space=flags & DCPF_MEMTYPE_MASK;
   ObtainSemaphore(&memlistsemaphore);
   
   if (!(flags & DMEMF_INTERNAL)) {                         /* external mem */
      align=alignsize[(flags & DMEMF_ALIGN_MASK)>>24];
      size=(len + D_MINALLOC-1)/D_MINALLOC;
      if (space & DMEMF_LDATA) {
         do {
            start=(ind+XPlist[ind].size+align) & ~align;
            stopx=XPlist[ind].next;
            while (start+size<=stopx) {                           /* found room in X..  */
               while (Ylist[indy].next<=start) indy=Ylist[indy].next;    /* check Y too */
               starty=(indy+Ylist[indy].size+align) & ~align;
               start=max(start,starty);
               stop=min(stopx,Ylist[indy].next);
               
               if (start+size<=stop) {
                  XPlist[start].next=XPlist[ind].next;             /* link */
                  XPlist[ind].next=start;
                  XPlist[start].size=size;
                  Ylist[start].next=Ylist[indy].next;
                  Ylist[indy].next=start;
                  Ylist[start].size=size;
                  addr=start*D_MINALLOC;
                  stopx=0;
               } else {
                  start=stop;
               }
            }
            ind=XPlist[ind].next;
         } while (ind && !addr);
      } else {
         if (space & DMEMF_YDATA) mem=Ylist; else mem=XPlist;
         do {
            start=(ind+mem[ind].size+align) & ~align;
            if (start+size<=mem[ind].next) {       /* found it.. */
               mem[start].next=mem[ind].next;    /* link */
               mem[ind].next=start;
               mem[start].size=size;
               addr=start*D_MINALLOC;
            } else {
               ind=mem[ind].next;
            }
         } while (ind && !addr);
      }      
   } else {                                           /* internal mem */
      align=alignsize[((flags & DMEMF_ALIGN_MASK)>>24)+5];
      if (space & DMEMF_LDATA) {
         do {
            start=(ind+IXlist[ind].size+align) & ~align;
            stopx=IXlist[ind].next;
            while (start+len<=stopx) {                        /* found room in X..  */
               while (IYlist[indy].next<=start) indy=IYlist[indy].next;    /* check Y too */
               starty=(indy+IYlist[indy].size+align) & ~align;
               start=max(start,starty);
               stop=min(stopx,IYlist[indy].next);
               
               if (start+len<=stop) {
                  IXlist[start].next=IXlist[ind].next;             /* link */
                  IXlist[ind].next=start;
                  IXlist[start].size=len;
                  IYlist[start].next=IYlist[indy].next;
                  IYlist[indy].next=start;
                  IYlist[start].size=len;
                  addr=start;
                  stopx=0;
               } else {
                  start=stop;
               }
            }
            ind=IXlist[ind].next;
         } while (ind && !addr);
      } else {
         if (space & DMEMF_PROG) align=0;
         if (space & DMEMF_YDATA) mem=IYlist; else if (space & DMEMF_XDATA) mem=IXlist; else mem=IPlist;
         do {
            start=(ind+mem[ind].size+align) & ~align;
            if (start+len<=mem[ind].next) {       /* found it.. */
               mem[start].next=mem[ind].next;    /* link */
               mem[ind].next=start;
               mem[start].size=len;
               addr=start + ((space & DMEMF_PROG) ? 0x1df : 0);
            } else {
               ind=mem[ind].next;
            }
         } while (ind && !addr);
      }
   }
   
   ReleaseSemaphore(&memlistsemaphore);

   if (!addr) return(NULL);
   
   if (flags & DMEMF_CLEAR && !(flags & DMEMF_INTERNAL)) {
      if (flags & DMEMF_LDATA) {
         ClearMem(DMEMF_XDATA,addr,len);
         ClearMem(DMEMF_YDATA,addr,len);
      } else ClearMem(space,addr, len);
   }

   return(addr);
}

/* Free memory */
ASM  void LIB_Delf_FreeMem(      REG(__a1) DELFPTR addr,
                           REG(__d0) ULONG space)
{
   if (!addr) return;
   
   ObtainSemaphore(&memlistsemaphore);
   if (!(space & DMEMF_INTERNAL)) {
      addr=addr/D_MINALLOC;
   
      if (space & DMEMF_LDATA) {
         remchunk(addr,XPlist);
         remchunk(addr,Ylist);
      } else {
         if (space & DMEMF_YDATA) remchunk(addr,Ylist); else remchunk(addr,XPlist);
      }
   } else {
      if (space & DMEMF_LDATA) {
         remchunk(addr,IXlist);
         remchunk(addr,IYlist);
      } else {
         if (space & DMEMF_YDATA) remchunk(addr,IYlist);
         else if (space & DMEMF_XDATA) remchunk(addr,IXlist);
         else remchunk(addr-0x1df, IPlist);
      }
   }
   ReleaseSemaphore(&memlistsemaphore);
}   

void remchunk(int addr, struct membl *mem)
{
   int ind=0;
   
   do {
      if (mem[ind].next==addr) {      /* found it */
         mem[ind].next=mem[addr].next;
         return;
      } else {
         ind=mem[ind].next;
      }
   } while (ind);
}
   
/* Clear delfina memory */
void ClearMem(ULONG space, DELFPTR delfaddr, ULONG size)
{
   int addr=(int)delfaddr,x;
   
      alloclite();
      startAD(v_copyAD,addr | ((space & DMEMF_YDATA) ? 0x800000 : 0));
      if (!localport){
	base[DH]=0;base[DM]=0;
        }else{
        base[DHlp]=0;base[DMlp]=0;}
      for (x=0;x<size;x++) put8(0);
      endAD();
      freelite();
}

/* AvailMem */
ASM  ULONG LIB_Delf_AvailMem( REG(__d1) ULONG flags)
{
   int sum=0,size,largest=0,ind=0,start,starty,stop,stopx,indy=0;
   struct membl *mem;
   
   if (flags & DMEMF_TOTAL) {
      if (flags & DMEMF_INTERNAL) {
         if (flags & DMEMF_PROG) return(32); else return(128);
      }
      if (memsize==128) return(0xfdc0); 
      else if (memsize==64) return(0x7dc0);
      else return(0x3dc0);
      
   }
   
   ObtainSemaphore(&memlistsemaphore);
   if (!(flags & DMEMF_INTERNAL)) {
      if (flags & DMEMF_LDATA) {
         while (XPlist[ind].next) {
            start=ind+XPlist[ind].size;
            stopx=XPlist[ind].next;
            while (stopx-start) {
               while (Ylist[indy].next<=start) indy=Ylist[indy].next;    /* check Y too */
               starty=indy+Ylist[indy].size;
               start=max(start,starty);
               stop=min(stopx,Ylist[indy].next);
               size=stop-start;
               if (size>0) {
                  if (size>largest) largest=size;
                  sum+=size;
               }
               start=stop;
            }
            ind=XPlist[ind].next;
         }
      } else {
         if (flags & DMEMF_YDATA) mem=Ylist; else mem=XPlist;
         while (mem[ind].next) {
            size=mem[ind].next-mem[ind].size-ind;
            if (size>largest) largest=size;
            sum+=size;
            ind=mem[ind].next;
         }
      }
      ReleaseSemaphore(&memlistsemaphore);
      if (flags & DMEMF_LARGEST) return(largest*D_MINALLOC); else return(sum*D_MINALLOC);
   } else {
      if (flags & DMEMF_LDATA) {
         while (IXlist[ind].next) {
            start=ind+IXlist[ind].size;
            stopx=IXlist[ind].next;
            while (stopx-start) {
               while (IYlist[indy].next<=start) indy=IYlist[indy].next;    /* check Y too */
               starty=indy+IYlist[indy].size;
               start=max(start,starty);
               stop=min(stopx,IYlist[indy].next);
               size=stop-start;
               if (size>0) {
                  if (size>largest) largest=size;
                  sum+=size;
               }
               start=stop;
            }
            ind=IXlist[ind].next;
         }
      } else {
         if (flags & DMEMF_YDATA) mem=IYlist; else if (flags & DMEMF_XDATA) mem=IXlist; else mem=IPlist;
         while (mem[ind].next) {
            size=mem[ind].next-mem[ind].size-ind;
            if (size>largest) largest=size;
            sum+=size;
            ind=mem[ind].next;
         }
      }      
      ReleaseSemaphore(&memlistsemaphore);
      if (flags & DMEMF_LARGEST) return(largest); else return(sum);
   }
}



/***************************/
/* interrupt code    */

/* Cause interrupt to delfina */
void cause(ULONG type)
{
      alloclite();
      uptime[0]=0;
      uptime[1]=0;
      Disable();
      *hostctl=0;
      *hostctl=HC_RESET;        /* toggle reset */
  Delay(1);
      *hostctl=0;
      while (base[ICR] & 0x80);

      base[ICR]=0;
      Enable();
      base[ICR]=8;                /* toggle hf0 */
      base[ICR]=1;
      *hostctl= HC_CODECSEL; //HC_HENCLK|HC_CODECSEL; the Flipper don�t need this
      freelite();
      update();
}


/* Find an interrupt structure based on the key */
struct Interrupt *findkey(int key)
{
   struct Node *node;
   
   node=(struct Node *)inthash[key & 0xff];
   while (node) {
      if ((int)node->ln_Pred==key) return(node);
      node=node->ln_Succ;
   }
   return (0); /* not found */
}
   
/* Add an interrupt server */
ASM  ULONG LIB_Delf_AddIntServer(    REG(__d0) ULONG key,
                                 REG(__a1) struct Interrupt *is)
{
   struct Node *next, *prev, *curr=(struct Node*)is;
   int sigbit;
   
   ObtainSemaphore(&intsemaphore);
   
   if (!key) {
      key=currkey++;
   } else if (findkey(key)) {
      ReleaseSemaphore(&intsemaphore);
      return(0);
   }
   
   
   curr->ln_Type=255;                       /* sigbit number */
   curr->ln_Name=(char *)FindTask(NULL);  /* sigtask */
   
   if(!is->is_Code) {
      if(!is->is_Data) {
         sigbit=AllocSignal(-1);
         if(sigbit==-1) {
            ReleaseSemaphore(&intsemaphore);
            return(0);
         }
         is->is_Data=(void *)(1<<sigbit);          
         curr->ln_Type=sigbit;   /* we allocated a signal bit */
      }
      is->is_Code=SigInt;           /* use the default server */
   }
   prev=(struct Node *)&(inthash[key & 0xff]);
   next=prev->ln_Succ;
   while (next && curr->ln_Pri<next->ln_Pri)     
   {
      prev=next;
      next=prev->ln_Succ;
   }
   prev->ln_Succ=curr;        
   curr->ln_Succ=next;
   curr->ln_Pred=(struct Node *)key;          /* abuse the node structure */
   
   ReleaseSemaphore(&intsemaphore);
   return(key);
}

/* remove an interrupt server */
ASM  struct Interrupt *LIB_Delf_RemIntServer(REG(__d0) ULONG key)
{
   struct Node *node,*prev;
   
   ObtainSemaphore(&intsemaphore);
   
   prev=(struct Node *)&inthash[key & 0xff];
   while (prev) {
      node=prev->ln_Succ;
      if ((int)node->ln_Pred==key) {
         prev->ln_Succ=node->ln_Succ;
         node->ln_Pred=NULL;
         node->ln_Succ=NULL;
         if (node->ln_Type!=-1) {
            FreeSignal(node->ln_Type);
         }
         node->ln_Type=0;
         ReleaseSemaphore(&intsemaphore);
         return(node);
      }
      prev=node;
   }
   ReleaseSemaphore(&intsemaphore);
   return(NULL);              /*not found */
}


/***********************************************************************
 *
 * Get and set attributes, see tags in delfina.h
 *
 */

ULONG  ASM LIB_Delf_GetAttr(      REG(__d0) int tag, 
                              REG(__d1) int arg )
{
   int data,*ptr,x;
//int load=0;
   switch (tag) {
      default:
         return(GetTagData(tag,0,GlobalTags));

  case DA_DSPClock:
    return (int)(dspclock*10000000.0);

      case DA_HWInfo:
         if (flipper) return(HWF_Flipper);
         if (d1200) return(HWF_1200);
         if (LIB_Delf_Peek(y_delfser,DMEMF_YDATA)) {�
           if (plus) return(HWF_LitePlus|HWF_Serial);
           else return(HWF_LitePlus|HWF_Serial); 
         } else {
           if (plus) return(HWF_Lite|HWF_Serial);
           else return(HWF_Lite|HWF_Serial); 
         }

      case DA_InputGain:
         return((GetTagData(DA_InputGainL,0,GlobalTags) +
                GetTagData(DA_InputGainR,0,GlobalTags))>>1);

      case DA_OutputVol:
         return((GetTagData(DA_OutputVolL,0,GlobalTags) +
                GetTagData(DA_OutputVolR,0,GlobalTags))>>1);

      case DA_Frequencies:
         return(FREQUENCIES);
      
      case DA_Index:
         if (arg>=FREQUENCIES || arg<0) return(0);
         return(freqtable[arg]);   
         
      case DA_Freq:
         if (arg)
            return(codecfreq[bestfreq(arg)]);
         else
            return(currfreq);
            
      case DA_InputClip:
         return(inputclip);
         
      case DA_OutputClip:
         return(outputclip);
         
      case DA_CPULoad:
         return(0);
      
      case DA_Uptime:
         ptr=(int *)arg; 
         if (ptr) {
            ptr[0]=uptime[0];
            ptr[1]=uptime[1];
         }
         return(uptime[0]);
         
      case DA_Freetime:
         LIB_Delf_Run(delf_watchdog,0,0,0,0,0,0);
         freetime=(LIB_Delf_Peek(y_idleh,DMEMF_YDATA)*16777216.0 + (double)LIB_Delf_Peek(y_idlel,DMEMF_YDATA))/dspclock;
         ptr=(int *)arg; 
         data=(int)(freetime/1000000.0);
         if (ptr) {
            ptr[0]=data;
            ptr[1]=(int)(freetime-(data*1000000.0));
         }
         return(data);
      
   }         
   return(0);
}

void  ASM LIB_Delf_SetAttrsA( REG(__a0) struct TagItem *tags)
{
   struct TagItem *tag,*tagptr;
   ULONG *dest;
   int freq,n;
   
   if (FindTagItem(DA_Defaults,tags)) {
      prefs.control&=~CF_MASK;
      prefs.data1=0x400000;
      prefs.data2=0xc0f000;
      ApplyTagChanges(GlobalTags, DefaultTags);
      CheckNotifyA(GlobalTags);
      if (outpipes && extpipes) n=LIB_Delf_GetAttr(DA_MonitorVol,0);
      else n=LIB_Delf_GetAttr(DA_PassthruVol,0);
      prefs.data2&= ~DF2_MO;
      prefs.data2|= ~((n/4369)<<12) & DF2_MO;
      update();          
      return;
   }      
   
   if (FindTagItem(DA_Load,tags)) {
      loadprefs("ENVARC:DelfinaPrefs");
      return;
   }
   
   ObtainSemaphore(&notifysemaphore);
   
   dest=(ULONG *)TempTags;
   tagptr=tags;                            /* fix taglist! */
   while (tag=NextTagItem(&tagptr)) {
      switch(tag->ti_Tag) {
         case DA_InputGain:
            if (tag->ti_Data>0x10000) tag->ti_Data=0x10000;
            if (tag->ti_Data<0) tag->ti_Data=0;
            if (GetTagData(DA_InputGainL,tag->ti_Data,GlobalTags)!=tag->ti_Data) {
               *dest++=DA_InputGainL;
               *dest++=tag->ti_Data;
            }
            if (GetTagData(DA_InputGainR,tag->ti_Data,GlobalTags)!=tag->ti_Data) {
               *dest++=DA_InputGainR;
               *dest++=tag->ti_Data;
            }
            break;

         case DA_OutputVol:
            if (tag->ti_Data>0x10000) tag->ti_Data=0x10000;
            if (tag->ti_Data<0) tag->ti_Data=0;
            if (GetTagData(DA_OutputVolL,tag->ti_Data,GlobalTags)!=tag->ti_Data) {
               *dest++=DA_OutputVolL;
               *dest++=tag->ti_Data;
            }
            if (GetTagData(DA_OutputVolR,tag->ti_Data,GlobalTags)!=tag->ti_Data) {
               *dest++=DA_OutputVolR;
               *dest++=tag->ti_Data;
            }
            break;
            
         case DA_CodecFreq:
            freq=codecfreq[bestfreq(tag->ti_Data)];
            if (freq==currfreq) break;
            *dest++=DA_Freq;
            *dest++=freq;
            currfreq=freq;
            break;
   
         case DA_LineMixVol:
         case DA_MicMixVol:
         case DA_CDMixVol:
         case DA_InputGainL:
         case DA_InputGainR:
         case DA_OutputVolL:
         case DA_OutputVolR:
         case DA_MonitorVol:
         case DA_PassthruVol:
         case DA_MixVol:
            if (tag->ti_Data>0x10000) tag->ti_Data=0x10000;
            if (tag->ti_Data<0) tag->ti_Data=0;

         default:
            if (GetTagData(tag->ti_Tag,tag->ti_Data-1,GlobalTags)!=tag->ti_Data) {
               *dest++=tag->ti_Tag;
               *dest++=tag->ti_Data;
            }
      }
   }
   if (dest == (ULONG *)TempTags) {          /* no changes done */
      ReleaseSemaphore(&notifysemaphore);
      return;
   }
   
   *dest=NULL;
   ApplyTagChanges(GlobalTags,TempTags);     /* modify global tags */
   
   CheckNotifyA(TempTags);

   tag=TempTags;
   while (tag->ti_Tag) {
      switch (tag->ti_Tag) {
         case DA_InputGainL:
            prefs.data2&= ~DF2_LG;
            prefs.data2|= (tag->ti_Data/4369)<<16;
            break;
            
         case DA_InputGainR:        
            prefs.data2&= ~DF2_RG;
            prefs.data2|= (tag->ti_Data/4369)<<8;
            break;
            
         case DA_OutputVolL:        
            prefs.data1&= ~DF1_LO;
            prefs.data1|= ~((tag->ti_Data/1040)<<16) & DF1_LO;
            break;
            
         case DA_OutputVolR:
            prefs.data1&= ~DF1_RO;
            prefs.data1|= ~((tag->ti_Data/1040)<<8) & DF1_RO;
            break;
            
         case DA_MonitorVol:        
            if (!outpipes || !extpipes) break;
            prefs.data2&= ~DF2_MO;
            prefs.data2|= ~((tag->ti_Data/4369)<<12) & DF2_MO;
            break;
            
         case DA_PassthruVol:        
            if (outpipes && extpipes) break;
            prefs.data2&= ~DF2_MO;
            prefs.data2|= ~((tag->ti_Data/4369)<<12) & DF2_MO;
            break;
            
         case DA_MixVol:
            if (!extpipes) break;
            LIB_Delf_Run(delf_setmixvol, 0,0,0,0,
            (int)(pow(2.0,(-log((double)extpipes)*1.44269504089*(LIB_Delf_GetAttr(DA_MixVol,0)/65536.0)))*8388607.0),0);
            break;

         case DA_LineOut:
            if(tag->ti_Data) prefs.data1|= DF1_LE; else prefs.data1&=~DF1_LE;
            break;
            
         case DA_HeadphoneOut:      
            if(tag->ti_Data) prefs.data1|= DF1_HE; else prefs.data1&=~DF1_HE;
            break;
            
         case DA_InputSel:
            inputsel=tag->ti_Data;
            if(tag->ti_Data) prefs.data2|= DF2_IS; else prefs.data2&=~DF2_IS;
            break;
            
         case DA_HighLevel:
            if(!tag->ti_Data) prefs.control|= CF_OLB; else prefs.control&=~CF_OLB;
            break;
            
         case DA_MicIsLine:
            if(tag->ti_Data) prefs.control|= CF_MLB; else prefs.control&=~CF_MLB;
            break;
            
         case DA_HighPass:
            if(tag->ti_Data) prefs.control|= CF_HPF; else prefs.control&=~CF_HPF;
            break;
            
         case DA_Freq:
            prefs.control&=~CF_DFR;
            prefs.control|=bestfreq(tag->ti_Data)<<2;
            break;
         
         case DA_Save:
            saveprefs("ENVARC:DelfinaPrefs");
            break;
      }
      tag++;    
   }
   update();
   ReleaseSemaphore(&notifysemaphore);
}

void  ASM LIB_Delf_SetModAttrsA( REG(__a0) struct DelfModule *mod,
                                 REG(__a1) struct TagItem *tags)
{
   struct TagItem *tag,*tagptr;
   int freq=-1,len,n=2;
   struct DelfModule *scan=ExtWorld;
   char name[MODNAMESIZE];
   
   tagptr=tags;                            /* scan taglist! */
   while (tag=NextTagItem(&tagptr)) {
      switch(tag->ti_Tag) {
         case DM_Image: 
            mod->image=(struct Image *)tag->ti_Data;
            break;
            
         case DM_Freq:
            freq=tag->ti_Data;
            if (freq) freq=codecfreq[bestfreq(freq)];
            mod->freq=freq;
            break;
            
         case DM_MsgPort:     
            mod->port=(struct MsgPort *)tag->ti_Data;
            break;
            
         case DM_Name:
            strcpy(name,(char *)tag->ti_Data);
            len=strlen(name);
            ObtainSemaphore(&modulesemaphore);
            while (LIB_Delf_FindModule(name)) sprintf(&name[len],"%d",n++);
            strcpy(mod->node.ln_Name,name);
            ReleaseSemaphore(&modulesemaphore);
            break;
      }
   }
   
   CheckNotify(DA_UpdateMod,mod->node.ln_Name,0);
         
   if (freq==-1) return;
   if (mod->freq==currfreq) return;
   
   ObtainSemaphore(&modulesemaphore);
   while (scan->node.ln_Succ) {            /* find highest frequency */
      if (scan->freq>freq) freq=scan->freq;
      scan=(struct DelfModule *)scan->node.ln_Succ;
   }
   ReleaseSemaphore(&modulesemaphore);
   
   if (freq==0) freq=freqtable[FREQUENCIES-1];

   Delf_SetAttrs(DA_CodecFreq,freq,0);
}

/* choose best frequency, as close as possible to freq */
int bestfreq(int freq)
{
   int diff=1000000,x,d,best;
   
   for (x=0;x<FREQUENCIES;x++) {
      d=abs(freqtable[x]-freq);
      if (d<diff) {
         diff=d;
         best=freqtable[x];
      }
   }
   
   for (x=0;x<16;x++)
      if (codecfreq[x]==best) return(x);
      
   return(0);
}

void saveprefs(char *name)
{
   BPTR fh;
   int n=0;
   ULONG *ptr=(ULONG *)GlobalTags;
   
   DBG("calculating tags... \n");
   while(ptr[n]) {
      n+=2;
      DVAL("count: %d\n",n/2);
   }
   
   DBG("Opening file\n");
   if (!(fh=Open(name,MODE_NEWFILE))) return;
   DBG("Writing file\n");
   Write(fh,ptr,n*4+4);
   DBG("Closing file\n");
   Close(fh);
   DBG("Prefs written\n");
}

void loadprefs(char *name)
{
   BPTR fh;
   struct TagItem tag[2]={0,0,0,0};
   
   if (!(fh=Open(name,MODE_OLDFILE))) return;
   
   while (Read(fh,&tag,8)==8)
      LIB_Delf_SetAttrsA(tag);

   Close(fh);
}

/* update delfina parameters */
void update()
{
   int newcontrol;
      int input,mixvol[3],x,delfbuf[10];

   DBG("UPdate starting..\n");
      
      if (inputsel>2 || inputsel<0) inputsel=1;
      switch(inputsel) {        
         case 0:
            input=0;break;
         case 1:
            input=0x800000;break;
         case 2:
            input=0x400000;break;
      }
      if (!(prefs.control & CF_MLB)) input|=0x200000;
      
      mixvol[0]=32-(GetTagData(DA_LineMixVol,0,GlobalTags)>>11);
      mixvol[1]=32-(GetTagData(DA_MicMixVol,0,GlobalTags)>>11);
      mixvol[2]=32-(GetTagData(DA_CDMixVol,0,GlobalTags)>>11);
      
      if (GetTagData(DA_MuteActive,0,GlobalTags) && outpipes && extpipes) mixvol[inputsel]=128;
      
      for (x=0;x<3;x++) {
         if (mixvol[x]==32) mixvol[x]=128;
      }
      
      delfbuf[0]=(prefs.data2 & DF2_LG) | input;
      delfbuf[1]=((prefs.data2 & DF2_RG)<<8) | input;
      delfbuf[2]=mixvol[2]<<16;
      delfbuf[3]=mixvol[1]<<16;
      delfbuf[4]=prefs.data1 & DF1_LO;
      delfbuf[5]=(prefs.data1 & DF1_RO)<<8;
      delfbuf[6]=((~prefs.control & CF_OLB)<<12) | 0x010000;
      delfbuf[7]=((prefs.control & CF_HPF)<<9);
      delfbuf[8]=mixvol[0]<<16;
      delfbuf[9]=((prefs.control & (CF_DFR))<<14) | 0xD00000;
      newcontrol=prefs.control & CF_DFR;
      x=(oldcontrol!=newcontrol ? 1:0);
      oldcontrol=newcontrol;
      LIB_Delf_CopyMem((int)&delfbuf,y_codecvars,40,DCPF_YDATA|DCPF_32BIT);

      if (prefs.control & 4) 
//         *hostctl=HC_HENCLK; // The Flipper don�t need this, right?
 //     else
         *hostctl=HC_CODECSEL;//HC_HENCLK|HC_CODECSEL;

      LIB_Delf_Run(delf_calibrate,0,0,currfreq,(ULONG)0x80*000000/(ULONG)currfreq,x,0);

    DBG("UPdate done\n");

}

/******************************************************************/
/* Notify stuff                       */
/******************************************************************/

struct MsgPort *  ASM LIB_Delf_StartNotifyA( REG(__a0) ULONG *tagids)
{                                 
   int n=0,len;
   struct notify *not;
   
/* calculate len for notify, allocate memory */
   while (tagids[n]) n++;
   n++;
   len=n*12+sizeof(struct notify);
   if (!(not=AllocMem(len,MEMF_CLEAR|MEMF_PUBLIC))) return(FALSE);
   
/* initialize fields */        
   not->len=len;
   not->tagids=(ULONG *)((ULONG)not+sizeof(struct notify));
   not->taglist=(struct TagItem *)((ULONG)not->tagids+n*4);
   CopyMem(tagids,not->tagids, n*4);
   
/* initialize DelfMsg */   
   not->msg.mn_Node.ln_Type=NT_MESSAGE;
   not->msg.mn_ReplyPort=notifyport;
   not->msg.mn_Length=sizeof(struct DelfMsg);
   
/* Create a msgport */        
   if (!(not->port=CreateMsgPort())) {
      FreeMem(not,not->len);
      return(FALSE);
   }
   
   ObtainSemaphore(&notifysemaphore);
   not->next=notifylist;
   notifylist=not;
   ReleaseSemaphore(&notifysemaphore);
   return(not->port);
}

void  ASM LIB_Delf_EndNotify(       REG(__a1) struct MsgPort *port)
{                                 
   struct notify *not, *prev;
   struct Message *msg;

   while (msg=GetMsg(port)) ReplyMsg(msg);

   ObtainSemaphore(&notifysemaphore);
   not=notifylist;
   if (!not) {
      ReleaseSemaphore(&notifysemaphore);
      return;
   }
   
/* find item, remove it from the list */  
   if (not->port == port) {
      notifylist=not->next;
   } else {
      while (not && (not->port != port)) {
         prev=not;
         not=not->next;
      }
      if (!not) {
         ReleaseSemaphore(&notifysemaphore);
         return;
      }
      prev->next=not->next;
   }
   ReleaseSemaphore(&notifysemaphore);
   
/* if notify still active, wait a while */
   while (not->flags & NF_INUSE) Delay(1);

/* Delete msgport ,free mem */
   DeleteMsgPort(not->port);
   FreeMem(not,not->len);
}

/********************************************/
/* Check tags for match in notify requests. */
void CheckNotify(ULONG tag, ...)
{
   CheckNotifyA((struct TagItem *)&tag);
}

void CheckNotifyA(struct TagItem *tags)
{
   struct TagItem *tag;
   struct notify *not,*msg;
   ULONG *dest;
   struct Task *task;

   task=FindTask(NULL);
   
   ObtainSemaphore(&notifysemaphore);          /* obtain access to list */
   not=notifylist;
   
   while (not) {
      if (not->port->mp_SigTask!=task) {       /* don't notify the caller */
         dest=NULL;
         tag=tags;
         while (tag->ti_Tag) {
            if (TagInArray(tag->ti_Tag,not->tagids)) {
               if (!dest) {
                  if (not->flags & NF_INUSE) { 
                     if (msg=AllocMem(not->len,MEMF_PUBLIC)) {
                        CopyMem(not,msg,not->len);
                        msg->tagids=(ULONG *)(((ULONG)not->tagids)-((ULONG)not)+((ULONG)msg));
                        msg->taglist=(struct TagItem *)(((ULONG)not->taglist)-((ULONG)not)+((ULONG)msg));
                        msg->flags=NF_INUSE|NF_ALLOC;
                     } else goto cn_next;
                     
                  } else {
                     msg=not;
                     msg->flags=NF_INUSE;
                  }
                  dest=(ULONG *)msg->taglist;
               }
               *dest++=tag->ti_Tag;
               *dest++=tag->ti_Data;
            }
            tag++;
         }
         if (dest) {
            dest[0]=NULL;
            PutMsg(msg->port,&msg->msg);
         }
      }
cn_next:
      not=not->next;
   }
   
   ReleaseSemaphore(&notifysemaphore);
}

/********************************/
/* Find DelfModule      */
struct Node *  ASM LIB_Delf_FindModule( REG(__a0) char *name)
{
   if (name) {
      return(FindName(&ModList,name));
   }
   return(ExtWorld);
}

/******************************/
/* Find AHI DelfModule        */
struct Node *  ASM LIB_Delf_AHIModule( REG(__a0) struct AHIAudioCtrlDrv *actrl)
{
   struct ahistuff *as;
   
   /* complex validity check to see if actrl is a Delfina mode */
   if (!actrl) return(NULL);
   if (!(as=actrl->ahiac_DriverData)) return(NULL);
   if (!TypeOfMem(as)) return(NULL);
   if (as->actrl!=actrl) return(NULL);
   if (as->cookie!=0x5FA37B9E) return(NULL);
   
   return(as->delfmod);
}

/********************************/
/* Add a new module     */

struct DelfModule *  ASM LIB_Delf_AddModuleA( REG(__a0) struct TagItem *tags)
{
   struct DelfModule *mod;
   struct TagItem *tagptr, *tag;
   int len,n=2,nopipes=0,inputs,outputs;
   struct Task *task;
   char *name;
   static char namebuf[256];
   
/* must know number of sockets here */
   inputs=GetTagData(DM_Inputs,0,tags);
   outputs=GetTagData(DM_Outputs,0,tags);
   if (inputs>255 || outputs>255 || (!inputs && !outputs)) return(FALSE);
   
/* get name */
   name=(char *)GetTagData(DM_Name,0,tags);
   if (!name) {
      name=namebuf;
      task=FindTask(NULL);
      strcpy(name, task->tc_Node.ln_Name);
      if (task->tc_Node.ln_Type == NT_PROCESS) {
         if (((struct Process *)task)->pr_TaskNum)
            GetProgramName(name,MODNAMESIZE);
      }
   } else {
      strcpy(namebuf,name);
      name=namebuf;
   }
   len=strlen(name);
   
/* make name unique */           
   ObtainSemaphore(&modulesemaphore);
   while (LIB_Delf_FindModule(name)) sprintf(&name[len],"%d",n++);
   
/* calculate the buffer lenght */    
   len=         sizeof(struct DelfModule) + 
         sizeof(struct DelfSocket)*inputs +
         sizeof(UBYTE)*outputs +
         MODNAMESIZE;
         
/* allocate memory for our module */       
   mod=AllocMem(len,MEMF_PUBLIC|MEMF_CLEAR);
   if (!mod) {
      ReleaseSemaphore(&modulesemaphore);
      return(NULL);
   }
   
/* initialize some fields */      
   mod->semaphore=&modulesemaphore;
   mod->inputs=inputs;
   mod->outputs=outputs;   
   mod->len=len;
   mod->sockets=(struct DelfSocket *)((ULONG)mod+sizeof(struct DelfModule));
   mod->osock=(UBYTE *)(&mod->sockets[inputs]);
   mod->node.ln_Name=name=&mod->osock[outputs];
   strcpy(name,namebuf);
   
/* process rest of the tags */         
   tagptr=tags;
   while (tag=NextTagItem(&tagptr)) {
      switch (tag->ti_Tag) {
         case DM_NoPipes:
            nopipes=tag->ti_Data;
            break;
            
         case DM_ShareIO:
            if (tag->ti_Data) mod->flags|=DF_SHAREIO; else mod->flags&=~DF_SHAREIO;
            break;

         case DM_Code:
            mod->code=(DELFPTR)tag->ti_Data;
            break;
            
         case DM_Image:
            mod->image=(struct Image *)tag->ti_Data;
            break;
            
         case DM_MsgPort:
            mod->port=(struct MsgPort *)tag->ti_Data;
            break;
            
         case DM_Freq:
            mod->freq=tag->ti_Data;
            break;
      }
   }
   
/* add to the list */        
   AddTail(&ModList,&mod->node);
   
/* Set pipes if needed, then exit */       
   if (!nopipes) {
      for (n=0;n<mod->inputs;n++)   setpipeq(ExtWorld,0,mod,n);
      for (n=0;n<mod->outputs;n++) setpipeq(mod,n,ExtWorld,DSOCKET_NEXT);
   }
   if (ExtWorld) n=sortmods(); else n=TRUE;
      
   ReleaseSemaphore(&modulesemaphore);
   
   if (!n) {
      LIB_Delf_RemModule(mod);      /* no pipes, fail */
      return(NULL);
   }
   if (ExtWorld) {
  Delf_SetModAttrs(mod,DM_Freq,mod->freq,0); 
   }

   return(mod);
}
      
/********************************/
/* Remove a module      */   
void  ASM LIB_Delf_RemModule( REG(__a1) struct DelfModule *mod)
{
   struct DelfModule *imod;
   ULONG isocket;

   ObtainSemaphore(&modulesemaphore);
   
/* remove all output pipes */
   imod=ExtWorld;
   while (imod->node.ln_Succ) {
      for (isocket=0;isocket<imod->inputs;isocket++) {
         if (imod->sockets[isocket].module == mod ) {
               CheckNotify(DA_UpdateMod,imod->node.ln_Name,0);
               imod->sockets[isocket].module=NULL;
               imod->sockets[isocket].socket=0;
         }
      }
      imod=(struct DelfModule*)imod->node.ln_Succ;
   }

/* notify all input pipes */
   for (isocket=0;isocket<mod->inputs;isocket++) {
      if(mod->sockets[isocket].module)
            CheckNotify(DA_UpdateMod,mod->sockets[isocket].module->node.ln_Name,0);
   }

/* Remove from list, sort mods */
   Remove(&mod->node);
   sortmods();       /* should work, we are _removing_ a module */
   ReleaseSemaphore(&modulesemaphore);
   
/* tell everyone we are gone */
   CheckNotify(DA_UpdateMod,mod->node.ln_Name,0);
   
/* Free mem */
   FreeMem(mod,mod->len);

/* update sample freq */
   { ULONG tags[] = {DM_Freq,0,0}; LIB_Delf_SetModAttrsA(ExtWorld,(struct TagItem *)tags); }
}

/********************************/
/* Set pipes         */
BOOL  ASM LIB_Delf_SetPipe(       REG(__a0) struct DelfModule *omod,
                              REG(__d0) ULONG osocket,
                              REG(__a1) struct DelfModule *imod,
                              REG(__d1) LONG isocket)
{                              
   int r;
   
   ObtainSemaphore(&modulesemaphore);
   r=setpipe(omod,osocket,imod,isocket);
   ReleaseSemaphore(&modulesemaphore);
   if (!r) return(r);
   
/* notify */
   if (omod) CheckNotify(DA_UpdateMod,omod->node.ln_Name,0);
   if (imod) CheckNotify(DA_UpdateMod,imod->node.ln_Name,0);
   return(r);
}

/**********************************************/
/* private setpipe. must own ModList         */
BOOL setpipe(struct DelfModule *omod, ULONG osocket, struct DelfModule *imod, ULONG isocket)
{
   struct DelfModule *oldm;
   ULONG olds;
   
   if (imod) {
      if (isocket == DSOCKET_NEXT) {
         isocket=0;
         while(imod->sockets[isocket].module) {
            isocket++;
            if (isocket==imod->inputs) return(FALSE);      /* no free socket */
         }
      }
      if (isocket>=imod->inputs) return(FALSE);
      if (omod) if (osocket>=omod->outputs) return(FALSE);
      
      oldm=imod->sockets[isocket].module;       /* if something goes wrong.. */
      olds=imod->sockets[isocket].socket;
      
      imod->sockets[isocket].module=omod;
      imod->sockets[isocket].socket=osocket;
      
      if (!sortmods()) {                    /* ... we can restore it here. */
         imod->sockets[isocket].module=oldm;
         imod->sockets[isocket].socket=olds;
         return(FALSE);
      }
         
      return(TRUE);
   }
   
/* sigh, imod was NULL. We have to scan all modules, yippee! */             
   if (!omod) return(FALSE);
   imod=ExtWorld;
   
   while (imod->node.ln_Succ) {
      for (isocket=0;isocket<imod->inputs;isocket++) {
         if ((imod->sockets[isocket].module == omod ) &&
            (imod->sockets[isocket].socket == osocket)) {
               imod->sockets[isocket].module=NULL;
               imod->sockets[isocket].socket=0;
         }
      }
      imod=(struct DelfModule*)imod->node.ln_Succ;
   }
   return(sortmods()); /* again, should work because we are removing a pipe */
}

/**********************************************/
/* nearly same, but doesn't sortmods()        */
BOOL setpipeq(struct DelfModule *omod, ULONG osocket, struct DelfModule *imod, ULONG isocket)
{
   if (imod) {
      if (isocket == DSOCKET_NEXT) {
         isocket=0;
         while(imod->sockets[isocket].module) {
            isocket++;
            if (isocket==imod->inputs) return(FALSE);      /* no free socket */
         }
      }
      if (isocket>=imod->inputs) return(FALSE);
      if (omod) if (osocket>=omod->outputs) return(FALSE);
      imod->sockets[isocket].module=omod;
      imod->sockets[isocket].socket=osocket;
      return(TRUE);
   }
   
/* sigh, imod was NULL. We have to scan all modules, yippee! */             
   if (!omod) return(FALSE);
   imod=ExtWorld;
   
   while (imod->node.ln_Succ) {
      for (isocket=0;isocket<imod->inputs;isocket++) {
         if ((imod->sockets[isocket].module == omod ) &&
            (imod->sockets[isocket].socket == osocket)) {
               imod->sockets[isocket].module=NULL;
               imod->sockets[isocket].socket=0;
         }
      }
      imod=(struct DelfModule*)imod->node.ln_Succ;
   }
   return(TRUE);
}
            
/****************************************/
/* Sort the ModList to execution order! */
/* Must own ModList!       */
/* Fails if a loop is detected!     */
/* Fails if out of Delfina mem!     */
BOOL sortmods(void)
{
   struct DelfModule *mod,*next,*head=NULL,*wait=NULL;
   int n,pipes=0;
   
/* clear ocnt fields. */
   mod=ExtWorld;
   while (mod->node.ln_Succ) {
      mod->ocnt=0;
      mod=(struct DelfModule *)mod->node.ln_Succ;
   }
      
/* count output pipes on every module, result to ocnt */
   mod=ExtWorld;
   while (mod->node.ln_Succ) {
      for (n=0;n<mod->inputs;n++) {
         if (mod->sockets[n].module) {
            mod->sockets[n].module->ocnt++;       /* increment pipe count */
            pipes++;
         }
      }
      mod=(struct DelfModule *)mod->node.ln_Succ;
   }
/* store number of pipes in extworld */
   outpipes=ExtWorld->ocnt;
   
/* Add one fake pipe to ExtWorld. */
/* This is required to make the recursion stop there. */
   ExtWorld->ocnt++;

/* check all nodes. mark non-connected modules, link heads */       
   mod=ExtWorld;
   while (mod->node.ln_Succ) {
      if (!mod->ocnt) {     /* no outputs,    */
         mod->link=head;     /* link to head list */
         head=mod;
      }
      mod=(struct DelfModule *)mod->node.ln_Succ;
   }
   
   
/* main loop. start from ExtWorld. */
   mod=ExtWorld;
   for (;;) {
   
/* dec counters on all inputs */ 
      for (n=0;n<mod->inputs;n++) {
         if (mod->sockets[n].module) {
            mod->sockets[n].module->ocnt--;
            pipes--;
         }
      }         
      
/* choose next module */      
      n=0;next=NULL;
      while (!next && n<mod->inputs) {    /* select a module with 0 opipes */
         next=mod->sockets[n].module;
         if (next) if (next->ocnt) next=NULL;
         n++;
      }
      if (!next) {                   /* if not found, pick one from the stack */
         next=wait;
         if (wait) {
            wait=wait->link;
         } else {             /* stack empty! */
            next=head;             /* pick a new head. */
            if (!next) {
               if (pipes) return(FALSE); /* loop detected.. */
               mod->link=NULL;     /* all done. */
               goto sort_done;
            }
            head=head->link;
         }
      }
      
/* store rest to the stack. Only store if ocnt=0 */         
      while (n<mod->inputs) {
         if (mod->sockets[n].module) {
            if (mod->sockets[n].module != next) {
               if (!mod->sockets[n].module->ocnt) {
                  mod->sockets[n].module->link=wait;
                  wait=mod->sockets[n].module;
               }
            }
         }
         n++;
      }
      
/* set link */          
      mod->link=next;
      mod=next;
   }
   
sort_done:    
/* A miracle; it worked.       */         
/* Modules are now linked backwards */
/* with mod->link.          */
/* Now call delfmod() to init stuff */
/* in Delfina.           */
   if (!delfmod()) return(FALSE);

/* Link nodes properly */
   mod=ExtWorld->link;
   next=(struct DelfModule *)&ModList.lh_Tail;
   next->node.ln_Pred=(struct Node *)mod;
   
   while (mod) {
      mod->node.ln_Succ=(struct Node*)next;
      mod->node.ln_Pred=(struct Node*)mod->link;
      next=mod;
      mod=mod->link;
   }
   ExtWorld->node.ln_Succ=(struct Node *)next;        
   ExtWorld->node.ln_Pred=NULL;
   if (next) next->node.ln_Pred=(struct Node*)ExtWorld;
   
   return(TRUE);  /* Copy stuff to delfina */
}
      
   
/**************************************/
/* Create code and buffers in Delfina */        
/* Fails if out of memory        */

BOOL delfmod(void)
{
   struct DelfModule *mod,*next;
   static UWORD buflist[1024];
   static UBYTE bufcnt[BUFFERS];
   DELFPTR modbuf,list,modptr,inlist,outlist;
   UWORD *outptr=buflist,*inptr;
   int n,mods=0,inputs,bufnum,highbuf=0,insocks=0,outsocks=0;
   struct modinfo modinfo;
   
/* first clear all osock fields, count modules and sockets */
   mod=ExtWorld;
   while (mod) {
      mods++;
      insocks+=mod->inputs;
      outsocks+=mod->outputs;
      if (mod->inputs>24) {
         mod->imaskh=(1<<(mod->inputs-24))-1; 
         mod->imaskl=0xffffff;
      } else {
         mod->imaskh=0;
         if (mod->inputs) mod->imaskl=(1<<mod->inputs)-1; else mod->imaskl=0;
      }
      if (mod->outputs>24) {
         mod->omaskh=(1<<(mod->outputs-24))-1; 
         mod->omaskl=0xffffff;
      } else {
         mod->omaskh=0;
         if (mod->outputs) mod->omaskl=(1<<mod->outputs)-1; else mod->omaskl=0;
      }
      memset(mod->osock,0,mod->outputs);
      mod=mod->link;
   }
   inptr=&outptr[outsocks];

/* Init ExtWorld output pipe */
   ExtWorld->osock[0]=1;

/* generate lists, find out how many buffers are needed */
   mod=ExtWorld;
   while (mod) {
/* Fix trash outputs, calculate real outputs,   */      
/* create output list.           */
/* If DF_SHAREIO is set, free all outputs!   */
      for (n=0;n<mod->outputs;n++) {
         if (!(bufnum=mod->osock[n])) {
            bufnum=newbuf(bufcnt);         /* get trash buf */
            bufcnt[bufnum]=0;              /* buf is free again! */
            if    (n<24) mod->omaskl &= ~(1<<n);
            else if (n<48) mod->omaskh &= ~(1<<(n-24));
         } else {
            if (mod->flags & DF_SHAREIO) bufcnt[bufnum]=0;
         }
         *outptr++=bufnum;
         if (bufnum>highbuf) highbuf=bufnum; /* highest bufnum */
      }      
      
/* Allocate input buffers, create input list */      
      for (n=0;n<mod->inputs;n++) {
         if (next=mod->sockets[n].module) {
            bufnum=next->osock[mod->sockets[n].socket];
            if (!bufnum) {
               bufnum=newbuf(bufcnt);
               next->osock[mod->sockets[n].socket]=bufnum;
            }
         } else {
            bufnum=0;                      /* null input */
            if    (n<24) mod->imaskl &= ~(1<<n);
            else if (n<48) mod->imaskh &= ~(1<<(n-24));
         }
         *inptr++=bufnum;
         if (bufnum>highbuf) highbuf=bufnum; /* highest bufnum */
      }
      
/* if DF_SHAREIO is not set, free outputs here. */      
      if (!(mod->flags & DF_SHAREIO)) {       
         for (n=0;n<mod->outputs;n++) {
            bufcnt[mod->osock[n]]=0;         /* buf is free again! */
         }
      }
         
      mod=mod->link;
   }
   
/* Highbuf now is the real needed amount of buffers. */
   highbuf++;
   
/* allocate memory for auto-generated program and lists */    
   modbuf=LIB_Delf_AllocMem(mods*(sizeof(struct modinfo)/4),DMEMF_XDATA);
   if (!modbuf) return(FALSE);
   list=LIB_Delf_AllocMem(insocks*2+outsocks*2,DMEMF_YDATA|DMEMF_CLEAR);
   if (!list) { 
      LIB_Delf_FreeMem(modbuf,DMEMF_XDATA);
      return(FALSE);
   }
   
/* Check how many buffers we already have, and       */         
/* allocate more if needed.. Can fail here.       */
   for (n=modbufcnt;n<highbuf;n++) {
      modbufs[n]=LIB_Delf_AllocMem(128,DMEMF_LDATA|DMEMF_ALIGN_128);
      if (!modbufs[n]) {
         n--;
         while (n>=modbufcnt) {
            LIB_Delf_FreeMem(modbufs[n],DMEMF_LDATA);
            n--;
         }
         LIB_Delf_FreeMem(list,DMEMF_YDATA);
         LIB_Delf_FreeMem(modbuf,DMEMF_PROG);
         return(FALSE);
      }
   }
      
/* fix list to real Delfina addresses. in two parts.. */  
   modbufs[1]=iobuf;
   for (n=0;n<insocks+outsocks;n++) {
      buflist[n]=modbufs[buflist[n]];
   }
   LIB_Delf_CopyMem((int)buflist,list,(insocks+outsocks)*2,DCPF_YDATA|DCPF_16BIT);
   for (n=0;n<insocks+outsocks;n++) {
      if (buflist[n]==iobuf) buflist[n]=iobuf+128;
   }
   LIB_Delf_CopyMem((int)buflist,(list+insocks+outsocks),(insocks+outsocks)*2,DCPF_YDATA|DCPF_16BIT);
   
/* Copy modinfos to Delfina */
   mod=ExtWorld;
   outlist=list;
   inlist=list+outsocks;
   modptr=modbuf+(mods-1)*(sizeof(struct modinfo)/4);
   while (mod) {
      modinfo.inlist=inlist;
      modinfo.outlist=outlist;
      modinfo.imaskl=mod->imaskl;
      modinfo.imaskh=mod->imaskh;
      modinfo.omaskl=mod->omaskl;
      modinfo.omaskh=mod->omaskh;
      modinfo.code=mod->code;      /* Copy to Delfina */
      LIB_Delf_CopyMem((int)&modinfo,modptr,sizeof(struct modinfo),DCPF_XDATA|DCPF_32BIT);
      
      inlist+=mod->inputs;              /* increment pointers */
      outlist+=mod->outputs;
      modptr-=sizeof(struct modinfo)/4;
      mod=mod->link;
   }
   
/* Count inputs on ExtWorld */         
   inputs=0;
   for (n=0;n<ExtWorld->inputs;n++) {
      if (ExtWorld->sockets[n].module) inputs++;
   }
   extpipes=inputs;
   if (!inputs) inputs=1;
   
/* Tell Delfina the new parameters! */   
   LIB_Delf_Run(delf_setbuf, 0,0, modbuf, insocks+outsocks,
      (int)(pow(2.0,(-log((double)inputs)*1.44269504089*(LIB_Delf_GetAttr(DA_MixVol,0)/65536.0)))*8388607.0),0);
   Delay(1); //new buffer should be in use after this

/* Free old modbuf and list */            
   if (audiomodbuf) {
      LIB_Delf_FreeMem(audiomodbuf,DMEMF_PROG);
      LIB_Delf_FreeMem(audiolist,DMEMF_YDATA);
   }
   audiomodbuf=modbuf;
   audiolist=list;
   
/* Free unused buffers */   
   for (n=highbuf;n<modbufcnt;n++) {
      LIB_Delf_FreeMem(modbufs[n],DMEMF_LDATA);
   }
   modbufcnt=highbuf;           
   
/* Fix monitor volume */
   if (outpipes && extpipes)
      n=LIB_Delf_GetAttr(DA_MonitorVol,0);
   else
      n=LIB_Delf_GetAttr(DA_PassthruVol,0);
   
   prefs.data2&= ~DF2_MO;
   prefs.data2|= ~((n/4369)<<12) & DF2_MO;
   update();
   
   return(TRUE);
}
               
/* find a free buffer, make it non-free */                 
int newbuf(UBYTE *bufs) 
{
   int n;
   
   for (n=2;n<BUFFERS;n++) {
      if (!bufs[n]) {
         bufs[n]++;
         return(n);
      }
   }
   return(0); 
}

ULONG ASM LIB_Private0(void) { return 0; }
ULONG ASM LIB_Private1(void) { return 0; }
ULONG ASM LIB_Private2(void) { return 0; }
ULONG ASM LIB_Private3(void) { return 0; }
