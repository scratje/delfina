
	include	'delfequ.inc'

; internal L ram variables

x_inptr		equ	0
y_outptr		equ	0

x_listofs		equ	1	;current list
;y_srate		equ	1	;current samplerate
x_extvol		equ	2	;extworld volume
;y_srate_inv	equ	2	;1/srate
x_extbuf		equ	3	;extworld output buffer
y_listlen		equ	3	;current list len
x_modptr    	equ 	4
y_modbuf    	equ 	4   	;ptr to module infos

x_srate_inv256	equ	5	;256/srate

x_flags		equ	6	;see definitions below

y_irqwr		equ	5	;next free slot in queue
y_irqrd		equ	6	;next irq to send

y_intha2		equ	7	;register storage
y_intha1		equ	8
y_intha0		equ	9
y_inthr0		equ	10
y_inthm0		equ	11
y_inthx0		equ	12

x_audioa2		equ	13
y_audion0		equ	13
l_audiob10	equ 	14
y_audiom0		equ	15
x_audior0		equ	15
l_audioa10	equ	16
l_audiox		equ	17

l_idlecount	equ	18
l_runa		equ	19

x_rddata1		equ	20
x_rddata2		equ	21
x_wrdata1		equ	22
x_wrdata2		equ	23

x_hosta2		equ	20
y_hostr0		equ	20
l_hosta10		equ	23
x_hostptr		equ 	21
y_hostm0		equ	21
x_hosttmp		equ	22
x_hosttmp2	equ	8

y_sery0		equ	24	;used by par too..
x_serr0		equ	24 
l_sera10		equ	25
x_serx0		equ	26
x_serm0		equ	27
x_sern0		equ	28

x_irqhead		equ	29	;pointer to first quick irq
x_irqtail		equ	30	;pointer to last quick irq

x_audioflags	equ	31	;flags for audio system

l_audioprg_reg	equ	32	;storage for audio registers

;x_flags:
f_irqdis		equ	0	;irq disabled
f_irqpend		equ	1	;irq pending


;interrupt queue is in y ram, locations 64-127:
y_irqqueue	equ	64

; external Y ram variables:

y_runx1		equ	($c0 + varbase)	;run parameters
y_runx0		equ	($c1 + varbase)
y_runy1		equ	($c2 + varbase)
y_runy0		equ	($c3 + varbase)
y_runaddr		equ	($c4 + varbase)
y_runmode		equ	($c5 + varbase)
y_intkey		equ	($c6 + varbase)	;interrupt key
y_intparam	equ	($c7 + varbase)	;parameter
y_rddata1		equ	($c8 + varbase)	;audio control parameters
y_rddata2		equ	($c9 + varbase)
y_wrdata1		equ	($ca + varbase)
y_wrdata2		equ	($cb + varbase)
y_wrctrl		equ	($cc + varbase)
y_idlel		equ	($d1 + varbase)
y_idleh		equ	($d2 + varbase)
y_error		equ	($e0 + varbase)	;error code
y_iobuf		equ	($e1 + varbase)	;address of our io buffer
y_cpupanic	equ	($e2 + varbase)	;cpupanic occurred?
y_outovr		equ 	($e3 + varbase)
y_delfser		equ	($e4 + varbase)
y_memsize      equ   ($e5 + varbase)
y_codecvars 	equ 	($f0 + varbase)

; private interrupt keys

key_done		equ	0	;amiga sets this
key_runprg	equ	1	;runprg finished (or startup done)
key_watchdog	equ	3	;watchdog interrupt

; private kernel vectors

delf_null		equ	$1e
delf_remkernel	equ	$2c
delf_extworld   	equ 	$2f

delf_setbuf	equ	(vecbase)
delf_calibrate	equ	(vecbase+2)
delf_setmixvol	equ	(vecbase+4)
delf_watchdog	equ	(vecbase+6)

;bits in the causeq intword
I_PENDING		EQU	23


; format for storereg/restorereg
reg_M0_x		equ	0	;m0 in x
reg_R0_y		equ	0	;r0 in y
reg_size		equ	17	;17 words, 32word aligned!