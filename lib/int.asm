
ICR equ 0
CVR equ 18
ISR equ 4
IVR equ 22
DH  equ 26
DM  equ 12
DL  equ 30


*****************************
* Delfina INT2 server


    xdef     @LiteInt
    xdef     _LiteInt
    xref    _DelfinaBase
    xref    _intmode
    xref    _inthash
    xref    _intcounter

    xref    _hosticr
    xref    _hostcvr
    xref    _hostisr
    xref    _hostivr
    xref    _hostctl
    xref    _hostdh
    xref    _hostdm
    xref    _hostdl
    xref    _intdisable
    xref    _base

    xdef    @changeintport
    xdef    _changeintport


DI_notme
    moveq    #$0,d0
    rts
DI_done
    tst.b    CVR(a5)
    bmi.s    DI_done
    moveq    #1,d0
    rts
    
@LiteInt
_LiteInt
    tst.l    _intdisable
    bne.s    DI_notme

    move.l    _base,a5        ;a5=base register
    tst.b    ISR(a5)
    bpl.s    DI_notme

sendcmd
    tst.b    DL(a5)
    btst    #0,ISR(a5)
    bne.s    sendcmd

    tst.b    CVR(a5)
    bmi.s    sendcmd

    btst    #3,ISR(a5)
    beq.s    DI_done

    move.b    #$a0,CVR(a5)    ;request data from delfina

getkey    

    btst    #3,ISR(a5)
    beq.s    DI_done
    btst    #0,ISR(a5)
    beq.s    getkey

    moveq    #0,d1
    move.b    DH(a5),d1
    swap    d1
    move.b    DM(a5),d1
    lsl.w    #8,d1
    move.b    DL(a5),d1        ;d1=key
    tst.l    d1
    beq.s    sendcmd        ;null key not allowed

getparam    

    btst    #3,ISR(a5)
    beq.s    DI_done
    btst    #0,ISR(a5)
    beq.s    getparam    
    
    moveq    #0,d0
    move.b    DH(a5),d0
    swap    d0
    move.b    DM(a5),d0
    lsl.w    #8,d0
    move.b    DL(a5),d0
    move.l    d0,a6        ;a6=param

    moveq    #0,d0
    move.b    d1,d0
    lea    _inthash,a0    ;a0=hash table
    
    move.l    (a0,d0.w*4),d0    ;d0=ptr to first item
    
    beq    sendcmd        ;invalid key

.hashloop
    move.l    d0,a0        ;a0=item
    cmp.l    4(a0),d1
    beq.s    .found
    move.l    (a0),d0
    bne.s    .hashloop    ;not found

    bra    sendcmd

.found
    move.l    d1,(a1)
    movem.l    a1/a5,-(a7)
    move.l    a6,d0            ;data passed from Delfina
    move.l    _DelfinaBase,a6    ;delfbase
    movem.l    $e(a0),a1/a5        ;is_data,is_code
    nop
    jsr    (a5)
    nop
    movem.l    (a7)+,a1/a5
    clr.l    (a1)
    bra    sendcmd
    
    
*********************************************
* server called after Delf_Run has finished

    xdef    _LiteRunInt
    xdef    @LiteRunInt
    
    xref    _runtask
    xref    _runsig
    xref    _runres
    
    xref    _SysBase
    xref    _LVOSignal
    
    xref    _runbuffer
    xref    _runwr
    xref    _runrd
    xref    _runend
    
    xref    _prevsynch
    xref    _dspbusy
    
_LiteRunInt
@LiteRunInt

    tst.l    _prevsynch        ; was previous program synch?
    bne.s    .asynch
    
    move.l    _runtask,d1
    beq.s    .asynch
    clr.l    _runtask
    move.l    d0,_runres
    move.l    _runsig,d0
    move.l    _SysBase,a6
    move.l    d1,a1
    jsr    _LVOSignal(a6)
.asynch
    move.l    _runrd,a1        ;a1=param buffer
    cmp.l    _runwr,a1
    beq.w    .bufferempty
    
    move.l    _base,a0    ;set address

    move.b    #$aa,d0
    bsr    quickcmd
    nop
.loopp    tst.b    CVR(a0)        ;start copy
    bmi.s    .loopp

    move.l    #$8002c0,d0
    bsr.s    quickput
    
    move.b    #$a2,d0
    bsr    quickcmd
    
    move.l    (a1)+,a6        ;a6=addr
    move.l    (a1)+,d0        ;x1
    bsr.s    quickput
    move.l    (a1)+,d0        ;x0
    bsr.s    quickput
    move.l    (a1)+,d0        ;y1
    bsr.s    quickput
    move.l    (a1)+,d0        ;y0
    bsr.s    quickput
    moveq    #0,d1
    move.l    a6,d0        ;addr
    smi    d1
    bsr.s    quickput
    move.l    d1,_prevsynch
    move.l    d1,d0
    bsr.s    quickput
    nop
.loop 
    btst    #2,ISR(a0)
    beq.s    .loop
    
    move.b    #$a6,d0        ;end copy
    bsr.s    quickcmd
    
    move.b    #$a7,d0
    bsr.s    quickcmd        ;init run
    
    move.b    #$a8,d0
    bsr.s    quickcmd        ;run
    nop
.wait   
    tst.b    CVR(a0)
    bmi.s    .wait

    cmp.l    _runend,a1        ;end of buffer?
    bne.s    .bufok
    lea    _runbuffer,a0
    move.l    a0,_runrd
    rts
.bufferempty
    clr.l    _dspbusy
.exit
    rts
.bufok     
    move.l    a1,_runrd        ;update pointer
    rts
    
    
quickput
    nop
.loop
    
    btst    #1,ISR(a0)
    beq.s    .loop

    swap    d0
    move.b    d0,DH(a0)
    rol.l    #8,d0
    move.b    d0,DM(a0)
    rol.l    #8,d0
    move.b    d0,DL(a0)

    rts

quickcmd
.loop    tst.b    CVR(a0)        ;start copy
    bmi.s    .loop
    move.b    d0,CVR(a0)
    rts
    

**********************************
* Watchdog interrupt
*
    xdef    _DogInt
    xdef    @DogInt
    xref    _wuhwuh
_DogInt
@DogInt
    move.l    a6,_wuhwuh
    rts
    
***********************************
* the default interrupt server
*
* a1=sigmask
* a0=interrupt

    xdef    _SigInt
    xdef    @SigInt
_SigInt
@SigInt
    move.l    a1,d0        ;sigmask
    move.l    $a(a0),a1    ;task
    move.l    4,a6
    jmp    _LVOSignal(a6)
    

ICRlp equ 0;; 0
CVRlp equ 32;; 18
ISRlp equ 2;; 4
IVRlp equ  34;; 22 
DHlp  equ 36;; 26
DMlp  equ  6;; 12
DLlp  equ 38;; 30
   

**************************
* Self modifing code
* Ugly hack for changing registers to 26pin expansionsport
*
_changeintport
@changeintport
	move.l #DI_notme,a0    ; Start of delf xferprgs
	move.l #_changeintport,a1 ;
	moveq #0,d0

changeloop
	cmp.l a1,a0
	bge.w ende		;end of change?  a0<changeport
	move.w (a0)+,d0
	cmp.w #$122d,d0		;check for move.b x(a5),d1
	beq.s change	;
	cmp.w #$102d,d0		;check for move.b x(a5),d0
	beq.s change	;
	cmp.w #$1140,d0		;check for move.b d0,x(a0)
	beq.s change	;

	cmp.w #$4a2d,d0		;check for tst.b x(a5)
	beq.s change
	cmp.w #$4a28,d0		;check for tst.b x(a0)
	beq.s change

	cmp.w #$1b7c,d0		;check for move.b #x,x(a5)
	beq.s changeop
	cmp.w #$0828,d0		;check for btst.b #x,x(a0)
	beq.s changeop
	cmp.w #$082d,d0		;check for btst.b #x,x(a5)
	beq.s changeop

	bra.s changeloop
changeop
	addq #2,a0
change	move.w (a0),d0		; get the Token
	cmp.w #CVR,d0
	bne.s notcvr
	move.w #CVRlp,(a0)

notcvr	cmp.w #ISR,d0		; check for ISR register
	bne.s notisr		
	move.w #ISRlp,(a0)	; yes change it to localport register


notisr	cmp.w #DH,d0
	bne.s notdh
	move.w #DHlp,(a0)

notdh	cmp.w #DM,d0
	bne.s notdm
	move.w #DMlp,(a0)

notdm	cmp.w #DL,d0
	bne.s notdl
	move.w #DLlp,(a0)
notdl	bra.s changeloop 
ende	rts
