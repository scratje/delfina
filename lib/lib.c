#include	"libdata.h"
#include "delfina.library_rev.h"

void	LibEnd(void);

#ifdef __PPC__
struct Library* LIB_Init(void);
#else
struct Library* ASM LIB_Init(REG(__d0) struct DelfinaBase *DelfinaBase,
                           REG(__a0) BPTR	      		SegList,
                           REG(__a6)  struct ExecBase	*SBase);
#endif


extern void	*LibFuncTable;
extern	ULONG	Dummy;

int  ASM __UserLibInit   (REG(__a6) struct DelfinaBase *libbase);
void ASM __UserLibCleanup(REG(__a6) struct DelfinaBase *libbase);
int  ASM __UserLibOpen	(REG(__a6) struct DelfinaBase *libbase);
int  ASM __UserLibClose	(REG(__a6) struct DelfinaBase *libbase);

struct LibInitStruct
{
	ULONG	LibSize;
	void	*FuncTable;
	void	*DataTable;
	void	(*InitFunc)(void);
};

struct LibInitStruct LibInitStruct=
{
	sizeof(struct DelfinaBase),
	(void *)&LibFuncTable,
	NULL,
	(void (*)(void)) &LIB_Init
};


struct Resident LibResident=
{
	RTC_MATCHWORD,
	&LibResident,
	&LibResident + 1,
#ifdef __PPC__
	RTF_PPC|RTF_AUTOINIT,
#else
	RTF_AUTOINIT,
#endif
	VERSION,
	NT_LIBRARY,
	0,
	"delfina.library",
	VSTRING,
	&LibInitStruct
};

static char *version= VERSTAG;

/*
 * To tell the loader that this is a new emulppc elf and not
 * one for the ppc.library.
 * ** IMPORTANT **
 */
#ifdef __PPC_
ULONG	__amigappc__=1;
#endif

/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/

/* The lib execute init protection which simply returns
 */

ULONG	NoExecute(void)
{
  return(0);
}

/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/

#ifdef __PPC__
struct Library* LIB_Init(void) {
   struct DelfinaBase	*DelfinaBase=(struct DelfinaBase*) REG_D0;
   BPTR Seglist = (BPTR) REG_A0;
   struct ExecBase *SBase = (struct ExecBase*) REG_A6;
#else

struct Library* ASM LIB_Init(REG(__d0) struct DelfinaBase *DelfinaBase,
                           REG(__a0) BPTR	      		SegList,
                           REG(__a6)  struct ExecBase	*SBase)
{
#endif

  DelfinaBase->SegList	=	SegList;
  DelfinaBase->Lib.lib_Flags=LIBF_SUMUSED | LIBF_CHANGED;
  DelfinaBase->Lib.lib_Revision=REVISION;

  if (__UserLibInit(DelfinaBase) != 0) return NULL;

  return(&DelfinaBase->Lib);
}



/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/

ULONG	ASM LIB_Expunge(REG(__a6) struct DelfinaBase *DelfinaBase)
{
BPTR	MySegment;

  MySegment	=	DelfinaBase->SegList;

  if (DelfinaBase->Lib.lib_OpenCnt)
  {
    DelfinaBase->Lib.lib_Flags |= LIBF_DELEXP;
    return(NULL);
  }

   __UserLibCleanup(DelfinaBase);

  /* We don`t need a forbid() because Expunge and Close
   * are called with a pending forbid.
   * But let`s do it for savety if somebody does it by hand.
   */
  Forbid();
  Remove(&DelfinaBase->Lib.lib_Node);
  Permit();

  FreeMem((APTR)((ULONG)(DelfinaBase) - (ULONG)(DelfinaBase->Lib.lib_NegSize)),
          DelfinaBase->Lib.lib_NegSize + DelfinaBase->Lib.lib_PosSize);

  return((ULONG) MySegment);
}

/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/

ASM struct Library*	LIB_Open(REG(__a6) struct DelfinaBase	*DelfinaBase) {

  DelfinaBase->Lib.lib_Flags	&=	~LIBF_DELEXP;
  DelfinaBase->Lib.lib_OpenCnt++;
   if (__UserLibOpen(DelfinaBase)) return NULL;

  return(&DelfinaBase->Lib);
}

/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/

ASM ULONG	LIB_Close(REG(__a6) struct DelfinaBase	*DelfinaBase) {

   __UserLibClose(DelfinaBase);
  if ((--DelfinaBase->Lib.lib_OpenCnt) < 1)
  {
    if (DelfinaBase->Lib.lib_Flags & LIBF_DELEXP)
    {
      return(LIB_Expunge(DelfinaBase));
    }
  }
  return(0);
}

/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/

ASM ULONG	LIB_Reserved(void)
{
  return(0);
}

