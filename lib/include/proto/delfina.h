/* Automatically generated header! Do not edit! */

#ifndef PROTO_DELFINA_H
#define PROTO_DELFINA_H

#include <clib/delfina_protos.h>

#ifdef __GNUC__
#ifdef __PPC__
#include <ppcinline/delfina.h>
#else
#include <inline/delfina.h>
#endif /* __PPC__ */
#else
#include <pragmas/delfina_pragmas.h>
#endif /* __GNUC__ */

#ifndef __NOLIBBASE__
extern struct Library *
#ifdef __CONSTLIBBASEDECL__
__CONSTLIBBASEDECL__
#endif /* __CONSTLIBBASEDECL__ */
DelfinaBase;
#endif /* !__NOLIBBASE__ */

#endif /* !PPCPROTO_DELFINA_H */
