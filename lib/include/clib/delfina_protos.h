#ifndef CLIB_DELFINA_PROTOS_H
#define CLIB_DELFINA_PROTOS_H

ULONG               Delf_AddIntServer(ULONG,struct Interrupt *);
struct DelfModule * Delf_AddModuleA(struct TagItem *);
struct DelfModule * Delf_AddModule(ULONG, ULONG, ... );
struct DelfPrg *    Delf_AddPrg(struct DelfObj *);
struct DelfModule * Delf_AHIModule(struct AHIAudioCtrl *);
DELFPTR             Delf_AllocMem(ULONG,ULONG);
ULONG               Delf_AvailMem(ULONG);
void                Delf_CopyMem(void *,void *,ULONG,ULONG);
void                Delf_EndNotify(struct MsgPort *);
struct DelfModule * Delf_FindModule(char *);
void                Delf_FreeMem(DELFPTR,ULONG);
ULONG               Delf_GetAttr(ULONG, ULONG);
ULONG               Delf_Peek(DELFPTR,ULONG);
void                Delf_Poke(DELFPTR,ULONG,ULONG);
struct Interrupt *  Delf_RemIntServer(ULONG);
void                Delf_RemModule(struct DelfModule *);
void                Delf_RemPrg(struct DelfPrg *);
ULONG               Delf_Run(DELFPTR,LONG,ULONG,ULONG,ULONG,ULONG,ULONG);
void                Delf_SetAttrsA(struct TagItem *);
void                Delf_SetAttrs(ULONG, ULONG, ... );
void                Delf_SetModAttrsA(struct DelfModule *,struct TagItem *);
void                Delf_SetModAttrs(struct DelfModule *,ULONG, ULONG, ...);
BOOL                Delf_SetPipe(struct DelfModule *, ULONG, struct DelfModule *, LONG);
struct MsgPort *    Delf_StartNotifyA(struct TagItem * );
struct MsgPort *    Delf_StartNotify(ULONG, ... );

#endif
