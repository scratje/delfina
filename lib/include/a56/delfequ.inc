; Vectors in internal P ram:

delf_cause       equ   $26   ;cause an interrupt to Amiga (vec changed in V4)
delf_causeq      equ   $28   ;cause an interrupt to Amiga, quick   (V3)
delf_ratesamples equ   $2a   ;query minimum buffer size            (V4)
delf_recrate     equ   $24   ;sample rate conversion for recording (V4)
delf_playrate    equ   $25   ;sample rate conversion for playback  (V4)
delf_checkovr    equ   $2d   ;check for input overrange            (V4)
delf_div         equ   $2e   ;divide 24-bit integers               (V4) 

; Variables in internal data ram (y_ for y mem and x_ for x mem):
y_srate		equ	1	;current samplerate
y_srate_inv	equ	2	;1/srate
