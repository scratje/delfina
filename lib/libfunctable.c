#include "libdata.h"

#ifdef __PPC__
// ppc stubs for library functions

struct Library *     PPC_LIB_Open(void) {
   return LIB_Open((struct DelfinaBase *) REG_A6);
}

ULONG                PPC_LIB_Close(void) {
   return LIB_Close((struct DelfinaBase *) REG_A6);
}

ULONG	               PPC_LIB_Expunge(void) {
   return LIB_Expunge((struct DelfinaBase *)REG_A6);
}

ULONG             	PPC_LIB_Reserved(void) {
   return LIB_Reserved();
}

ULONG                PPC_LIB_Private0(void) {
   return LIB_Private0();
}

ULONG                PPC_LIB_Delf_Peek(void) {
   return LIB_Delf_Peek((DELFPTR)REG_A1, (ULONG )REG_D0);
}

void                 PPC_LIB_Delf_Poke(void) {
   return LIB_Delf_Poke((DELFPTR)REG_A1, (ULONG)REG_D0, (ULONG )REG_D1);
}

void                 PPC_LIB_Delf_CopyMem(void) {
   return LIB_Delf_CopyMem((int)REG_A0, (int)REG_A1, (ULONG)REG_D0, (ULONG )REG_D1);
}

DELFPTR              PPC_LIB_Delf_AllocMem(void) {
   return LIB_Delf_AllocMem((ULONG)REG_D0, (ULONG )REG_D1);
}

void                 PPC_LIB_Delf_FreeMem(void) {
   return LIB_Delf_FreeMem((DELFPTR)REG_A1, (ULONG )REG_D0);
}

ULONG                PPC_LIB_Delf_AvailMem(void) {
   return LIB_Delf_AvailMem((ULONG )REG_D1);
}

struct DelfPrg *     PPC_LIB_Delf_AddPrg(void) {
   return LIB_Delf_AddPrg((struct DelfObj * )REG_A1);
}

void                 PPC_LIB_Delf_RemPrg(void) {
   return LIB_Delf_RemPrg((struct DelfPrg *)REG_A1);
}

ULONG                PPC_LIB_Delf_Run(void) {
   return LIB_Delf_Run((DELFPTR)REG_A1, (long)REG_D0, (ULONG)REG_D1, (ULONG)REG_D2, (ULONG)REG_D3, (ULONG)REG_D4, (ULONG )REG_D5);
}

ULONG                PPC_LIB_Private1(void) {
   return LIB_Private1();
}

ULONG                PPC_LIB_Delf_AddIntServer(void) {
   return LIB_Delf_AddIntServer((ULONG)REG_D0, (struct Interrupt * )REG_A1);
}

struct Interrupt *     PPC_LIB_Delf_RemIntServer(void) {
   return LIB_Delf_RemIntServer((ULONG )REG_D0);
}

ULONG                PPC_LIB_Private2(void) {
   return LIB_Private2();
}

ULONG                PPC_LIB_Private3(void) {
   return LIB_Private3();
}

struct MsgPort *     PPC_LIB_Delf_StartNotifyA(void) {
   return LIB_Delf_StartNotifyA((ULONG * )REG_A0);
}

void                 PPC_LIB_Delf_EndNotify(void) {
   return LIB_Delf_EndNotify((struct MsgPort * )REG_A1);
}

ULONG                PPC_LIB_Delf_GetAttr(void) {
   return LIB_Delf_GetAttr((int)REG_D0, (int )REG_D1);
}

void                 PPC_LIB_Delf_SetAttrsA(void) {
   return LIB_Delf_SetAttrsA((struct TagItem * )REG_A0);
}

struct DelfModule *     PPC_LIB_Delf_AddModuleA(void) {
   return LIB_Delf_AddModuleA((struct TagItem * )REG_A0);
}

void                 PPC_LIB_Delf_RemModule(void) {
   return LIB_Delf_RemModule((struct DelfModule * )REG_A1);
}

struct Node *        PPC_LIB_Delf_FindModule(void) {
   return LIB_Delf_FindModule((char * )REG_A0);
}

short                PPC_LIB_Delf_SetPipe(void) {
   return LIB_Delf_SetPipe((struct DelfModule *)REG_A0, (ULONG)REG_D0, (struct DelfModule *)REG_A1, (long )REG_D1);
}

void                 PPC_LIB_Delf_SetModAttrsA(void) {
   return LIB_Delf_SetModAttrsA((struct DelfModule *)REG_A0, (struct TagItem * )REG_A1);
}

struct Node *        PPC_LIB_Delf_AHIModule(void) {
   return LIB_Delf_AHIModule((struct AHIAudioCtrlDrv * )REG_A0);
}


ULONG	LibFuncTable[]=
{
	FUNCARRAY_32BIT_NATIVE,
   (ULONG) &PPC_LIB_Open,
   (ULONG) &PPC_LIB_Close,
   (ULONG) &PPC_LIB_Expunge,
   (ULONG) &PPC_LIB_Reserved,
   (ULONG) &PPC_LIB_Private0,
   (ULONG) &PPC_LIB_Delf_Peek,
   (ULONG) &PPC_LIB_Delf_Poke,
   (ULONG) &PPC_LIB_Delf_CopyMem,
   (ULONG) &PPC_LIB_Delf_AllocMem,
   (ULONG) &PPC_LIB_Delf_FreeMem,
   (ULONG) &PPC_LIB_Delf_AvailMem,
   (ULONG) &PPC_LIB_Delf_AddPrg,
   (ULONG) &PPC_LIB_Delf_RemPrg,
   (ULONG) &PPC_LIB_Delf_Run,
   (ULONG) &PPC_LIB_Private1,
   (ULONG) &PPC_LIB_Delf_AddIntServer,
   (ULONG) &PPC_LIB_Delf_RemIntServer,
   (ULONG) &PPC_LIB_Private2,
   (ULONG) &PPC_LIB_Private3,
   (ULONG) &PPC_LIB_Delf_StartNotifyA,
   (ULONG) &PPC_LIB_Delf_EndNotify,
   (ULONG) &PPC_LIB_Delf_GetAttr,
   (ULONG) &PPC_LIB_Delf_SetAttrsA,
   (ULONG) &PPC_LIB_Delf_AddModuleA,
   (ULONG) &PPC_LIB_Delf_RemModule,
   (ULONG) &PPC_LIB_Delf_FindModule,
   (ULONG) &PPC_LIB_Delf_SetPipe,
   (ULONG) &PPC_LIB_Delf_SetModAttrsA,
   (ULONG) &PPC_LIB_Delf_AHIModule,
   0xffffffff
};

#else

void (*LibFuncTable[])(void)=
{
   (void (*)(void)) &LIB_Open,
   (void (*)(void)) &LIB_Close,
   (void (*)(void)) &LIB_Expunge,
   (void (*)(void)) &LIB_Reserved,
   (void (*)(void)) &LIB_Private0,
   (void (*)(void)) &LIB_Delf_Peek,
   (void (*)(void)) &LIB_Delf_Poke,
   (void (*)(void)) &LIB_Delf_CopyMem,
   (void (*)(void)) &LIB_Delf_AllocMem,
   (void (*)(void)) &LIB_Delf_FreeMem,
   (void (*)(void)) &LIB_Delf_AvailMem,
   (void (*)(void)) &LIB_Delf_AddPrg,
   (void (*)(void)) &LIB_Delf_RemPrg,
   (void (*)(void)) &LIB_Delf_Run,
   (void (*)(void)) &LIB_Private1,
   (void (*)(void)) &LIB_Delf_AddIntServer,
   (void (*)(void)) &LIB_Delf_RemIntServer,
   (void (*)(void)) &LIB_Private2,
   (void (*)(void)) &LIB_Private3,
   (void (*)(void)) &LIB_Delf_StartNotifyA,
   (void (*)(void)) &LIB_Delf_EndNotify,
   (void (*)(void)) &LIB_Delf_GetAttr,
   (void (*)(void)) &LIB_Delf_SetAttrsA,
   (void (*)(void)) &LIB_Delf_AddModuleA,
   (void (*)(void)) &LIB_Delf_RemModule,
   (void (*)(void)) &LIB_Delf_FindModule,
   (void (*)(void)) &LIB_Delf_SetPipe,
   (void (*)(void)) &LIB_Delf_SetModAttrsA,
   (void (*)(void)) &LIB_Delf_AHIModule,
	(void (*)(void)) 0xffffffff
};

#endif
