;; include init.gs
   include    'delfina.i'
   ;; include    'macros.i'
  

;; the NOPs are important here, they force pipeline flush in 060 and also
;; in morphos nop flushes PPC pipeline with "sync" instruction. 
;; Without nop it's possible that writes are still pending while CPU 
;; starts to read and check for status bits etc..
;;
;; In short, always "nop" before checking status.


ICR equ    0
CVR equ   18
ISR equ    4
IVR equ    22 
DH  equ   26
DM  equ    12
DL  equ   30


TURBO equ 32

    xdef    _put24
    xdef    _put16
    xdef    _put8
    xdef    _get24
    xdef    _get16
    xdef    _get8
    xdef    _runvec
    xdef    _startAD
    xdef    _startDA
    xdef    _endAD
    xdef    _endDA
    xdef    @put24
    xdef    @put16
    xdef    @put8
    xdef    @get24
    xdef    @get16
    xdef    @get8
    xdef    @runvec
    xdef    _startAD
    xdef    _startDA
    xdef    _endAD
    xdef    _endDA
    xdef    _changeport
    xdef    @changeport

    xref    _base
;;_base equ 500 
   
*************************
* private stubs, just return NULL

    xdef    _LIB_Delf_Private0
    xdef    _LIB_Delf_Private1
    xdef    _LIB_Delf_Private2
    xdef    _LIB_Delf_Private3
    xdef    __XCEXIT
_LIB_Delf_Private0
_LIB_Delf_Private1
_LIB_Delf_Private2
_LIB_Delf_Private3
__XCEXIT
    moveq    #0,d0
    rts


turboen    macro    \1
;;    move.b    #$11,$40-(\1)(a1)
;;    move.b    #$2,-(\1)(a1)
    endm

turbodis macro    \1
;;    clr.b    -(\1)(a1)
;;    move.b    #$1,$40-(\1)(a1)
    endm


waitcmd    macro    
    nop
\@  tst.b    CVR(a0)
    bmi.s    \@
    endm

    xref    _LVODisable
    xref    _LVOEnable

*******************************
* start DA copy
* d0=vec, d1=addr
_startDA
@startDA
    bsr.s    writeaddr

.flush
    tst.b    DL(a0)    ;flush remaining data
 
    btst    #0,ISR(a0)
    bne.s    .flush
.flushok    

    move.b    #$a9,CVR(a0) ;enable int
    waitcmd
    clr.b    DL(a0)      ;get data

    rts
    
_startAD
@startAD

writeaddr
    move.l    _base,a0

    waitcmd
    move.b    #$aa,CVR(a0)
    waitcmd

    nop
.loop2
    btst    #1,ISR(a0)
    beq.s    .loop2
    swap    d1
    move.b    d1,DH(a0)
    rol.l    #8,d1
    move.b    d1,DM(a0)
    rol.l    #8,d1
    move.b    d1,DL(a0)
    move.b    d0,CVR(a0)    ;start copy
    waitcmd

    nop
.wait
    btst    #4,ISR(a0) ;wait for copy to start
    beq.s    .wait
    rts
    
_endAD
@endAD
    move.l    _base,a0
    nop
.loop
    btst    #2,ISR(a0)
    beq.s    .loop
_endDA
@endDA
    move.l    _base,a0
    waitcmd
    move.b    #$a6,CVR(a0)
    waitcmd

    nop
.wait
    btst    #4,ISR(a0)
    bne.s    .wait
.clean    

    tst.b    DL(a0)
    nop
    btst    #0,ISR(a0)
    bne.s    .clean
    move.b    #$af,CVR(a0)
    waitcmd

    moveq    #0,d0
    nop
.getptr
    btst    #0,ISR(a0)
    beq.s    .getptr
    move.b    DH(a0),d0
    swap    d0
    move.b    DM(a0),d0
    lsl.w    #8,d0
    move.b    DL(a0),d0
    move.b    #$a1,CVR(a0)
    waitcmd

    rts    
_put24    
@put24    
    move.l    _base,a0
    nop
.loop 
    btst    #1,ISR(a0)
    beq.s    .loop
    swap    d0
    move.b    d0,DH(a0)
    rol.l    #8,d0
    move.b    d0,DM(a0)
    rol.l    #8,d0
    move.b    d0,DL(a0)
    rts
    
_put16    
@put16    
    move.l    _base,a0
    nop
.loop 
    btst    #1,ISR(a0)
    beq.s    .loop
    rol.w    #8,d0
    move.b    d0,DM(a0)
    rol.w    #8,d0
    move.b    d0,DL(a0)
    rts
_put8    
@put8    
    move.l    _base,a0
    nop
.loop 
    btst    #1,ISR(a0)
    beq.s    .loop
    move.b    d0,DL(a0)
    rts
    
_get24    
@get24    
    moveq    #0,d0
    move.l    _base,a0
    nop
.loop    
    btst    #2,ISR(a0)
    beq.s    .loop
    move.b    DH(a0),d0
    swap    d0
  nop
    move.b    DM(a0),d0
    lsl.w    #8,d0
  nop
    move.b    DL(a0),d0
    nop
    clr.b    DL(a0)
    clr.b    DM(a0)
    clr.b    DH(a0)
    rts
    
_get16    
@get16    
    moveq    #0,d0
    move.l    _base,a0
    nop
.loop    
    btst    #2,ISR(a0)
    beq.s    .loop
    move.b    DM(a0),d0
    lsl.w    #8,d0
    move.b    DL(a0),d0
    nop
    clr.b    DL(a0)
    rts
    
_get8    
@get8    
    moveq    #0,d0
    move.l    _base,a0
    nop
.loop    
    btst    #2,ISR(a0)
    beq.s    .loop
    move.b    DL(a0),d0
    nop
    clr.b    DL(a0)
    rts
    
;*****************************    
;* run a DSP interrupt
_runvec
@runvec
    move.l    _base,a0
    waitcmd
    nop
    move.b    d0,CVR(a0)
    waitcmd
    rts
    
;*****************************    
;* a0 = amy ptr, d0=bytes
;* copy routines    
    
    xdef    _liteda32l
    xdef    _liteda32h
    xdef    _liteda24
    xdef    _liteda16
    xdef    _liteda8
    xdef    _litead32l
    xdef    _litead32h
    xdef    _litead24
    xdef    _litead16
    xdef    _litead8
    xdef    _plusad32l
    xdef    _plusad32h
    xdef    _plusad24
    xdef    _plusad16
    xdef    _plusad8
    xdef    @liteda32l
    xdef    @liteda32h
    xdef    @liteda24
    xdef    @liteda16
    xdef    @liteda8
    xdef    @litead32l
    xdef    @litead32h
    xdef    @litead24
    xdef    @litead16
    xdef    @litead8
    xdef    @plusad32l
    xdef    @plusad32h
    xdef    @plusad24
    xdef    @plusad16
    xdef    @plusad8
    
*****************************    
* 32 bit copy, D->A
_liteda32h
@liteda32h
    move.l    _base,a1
    ror.l    #2,d0
    bra.s    .endloop
    cnop    0,8
.loop    
    moveq    #0,d1
    nop
.wait   
    btst    #2,ISR(a1)
    beq.s    .wait 
    move.b    DH(a1),d1
    swap    d1
    move.b    DM(a1),d1
    lsl.w    #8,d1
    move.b    DL(a1),d1
    nop
    clr.b    DL(a1)
    move.l    d1,(a0)+
.endloop
    dbf    d0,.loop
    
    rol.l    #2,d0
    and.w    #3,d0
    subq.w    #1,d0
    bmi.s    .exit
    
    clr.b    (a0)+
    subq.w    #1,d0
    bmi.s    .exit
    
    nop
.wait2 
    btst    #2,ISR(a1)
    beq.s    .wait2
    
    move.b    DH(a1),(a0)+
    subq.w    #1,d0
    bmi.s    .exit
    move.b    DM(a1),(a0)+
.exit
    rts
    
*****************************    
* 32 bit copy, D->A, shift left
_liteda32l
@liteda32l
    move.l    _base,a1
    ror.l    #2,d0
    bra.s    .endloop
    cnop    0,8
.loop    
    nop
.wait 
    btst    #2,ISR(a1)
    beq.s    .wait
    
    move.b    DH(a1),d1
    lsl.w    #8,d1
    move.b    DM(a1),d1
    swap    d1
    move.b    DL(a1),d1
    nop
    clr.b    DL(a1)
    lsl.w    #8,d1
    move.l    d1,(a0)+
.endloop
    dbf    d0,.loop
    
    rol.l    #2,d0
    and.w    #3,d0
    beq.s    .exit
    
    nop
.wait2 
    btst    #2,ISR(a1)
    beq.s    .wait2
    
    move.b    DH(a1),(a0)+
    subq.w    #1,d0
    bmi.s    .exit
    move.b    DM(a1),(a0)+
    subq.w    #1,d0
    bmi.s    .exit
    move.b    DL(a1),(a0)+
.exit
    rts
        
************************
* 24-bit copy, D->A
_liteda24
@liteda24
    divs    #3,d0
    move.l    _base,a1
    bra.s    .endloop
    cnop    0,8
.loop
    nop
.wait 
    btst    #2,ISR(a1)
    beq.s    .wait
    move.b    DH(a1),(a0)+
    move.b    DM(a1),(a0)+
    move.b    DL(a1),(a0)+
    nop
    clr.b    DL(a1)
.endloop 
    dbf    d0,.loop
    
    swap    d0   
    subq.w    #1,d0
    bmi.s    .exit

    nop
.wait2  
    btst    #2,ISR(a1)
    beq.s    .wait2
    
    move.b    DH(a1),(a0)+
    subq.w    #1,d0
    bmi.s    .exit
    move.b    DM(a1),(a0)+
.exit
jumprts2
    rts
    
**************************
* 16-bit copy, D->A
_liteda16
@liteda16
    move.l    _base,a1
    
    ror.l    #2,d0
    beq.s    jumprts2
    
    move.l    a0,d1
    btst    #1,d1
    beq.s    .endloop
    
    rol.l    #2,d0

    nop    
.wait1    
    btst    #2,ISR(a1)
    beq.s    .wait1
    move.b    DM(a1),d1
    lsl.w    #8,d1
    move.b    DL(a1),d1
    nop
    clr.b    DL(a1)
    move.w    d1,(a0)+
    subq.l    #2,d0
    
    ror.l    #2,d0
    bra.s    .endloop
    
    cnop    0,8

.loop
    nop
.wait2 
    btst    #2,ISR(a1)
    beq.s    .wait2
    move.b    DM(a1),d1
    lsl.w    #8,d1
    move.b    DL(a1),d1
    nop
    clr.b    DL(a1)
    swap    d1
    nop
.wait3  
    btst    #2,ISR(a1)
    beq.s    .wait3
    move.b    DM(a1),d1
    lsl.w    #8,d1
    move.b    DL(a1),d1
    nop
    clr.b    DL(a1)
    move.l    d1,(a0)+
.endloop 
    dbf    d0,.loop
    
    rol.l    #2,d0
    and.w    #3,d0
    subq.w    #1,d0
    bmi.s    .exit
    
    nop
.wait4  
    btst    #2,ISR(a1)
    beq.s    .wait4
    
    move.b    DM(a1),(a0)+
    subq.w    #1,d0
    bmi.s    .exit
    move.b    DL(a1),(a0)+
    nop
    clr.b    DL(a1)
    dbf    d0,.wait4
.exit    
jumprts
    rts

**************************
* 8-bit copy, D->A
_liteda8
@liteda8
    move.l    _base,a1
    bra.s    .endloop    
    cnop    0,8
.loop    
    nop
.wait  
    btst    #2,ISR(a1)
    beq.s    .wait
    move.b    DH(a1),(a0)+
    tst.b    DL(a1)
    nop
    clr.b    DL(a1)
.endloop 
    dbf    d0,.loop
    rts

   
*****************************    
* 32 bit copy, A->D
_litead32h
@litead32h
    subq.l    #1,d0
    bmi.s    .exit
    
    move.l    _base,a1
    lsr.l    #2,d0
.loop    
    move.l    (a0)+,d1
    swap    d1
    nop
.wait  
    btst    #1,ISR(a1)
    beq.s    .wait
    move.b    d1,DH(a1)
    rol.l    #8,d1
    move.b    d1,DM(a1)
    rol.l    #8,d1
    move.b    d1,DL(a1)
    dbf    d0,.loop
.exit
    rts
    
*****************************    
* 32 bit copy, A->D, shift right
_litead32l
@litead32l
    subq.l    #1,d0
    bmi.s    .exit
    
    move.l    _base,a1
    lsr.l    #2,d0
    
.loop    
    move.l    (a0)+,d1
    rol.l    #8,d1
    nop
.wait 
    btst    #1,ISR(a1)
    beq.s    .wait
    move.b    d1,DH(a1)
    rol.l    #8,d1
    move.b    d1,DM(a1)
    rol.l    #8,d1
    move.b    d1,DL(a1)
    dbf    d0,.loop
.exit
    rts    
    
************************
* 24-bit copy, A->D
_litead24
@litead24
    subq.l    #1,d0
    bmi.s    .exit
    
    divs    #3,d0
    move.l    _base,a1
.loop
    nop
.wait 
    btst    #1,ISR(a1)
    beq.s    .wait
    move.b    (a0)+,DH(a1)
    move.b    (a0)+,DM(a1)
    move.b    (a0)+,DL(a1)
    dbf    d0,.loop
    
.exit
    rts
    
**************************
* 16-bit copy, A->D
_litead16
@litead16
    tst.l    d0
    beq.s    .exit

    move.l    _base,a1
    clr.b    DH(a1)

    move.l    a0,d1
    btst    #1,d1
    beq.s    .start
    
    nop
.wait1 
    btst    #1,ISR(a1)
    beq.s    .wait1
    move.b    (a0)+,DM(a1)
    move.b    (a0)+,DL(a1)
    subq.l    #2,d0

.start
    ror.l    #2,d0
    bra.s    .endloop
    cnop    0,8
.loop
    move.l    (a0)+,d1
    rol.l    #8,d1
    nop
.wait2
    btst    #1,ISR(a1)
    beq.s    .wait2
    move.b    d1,DM(a1)
    rol.l    #8,d1
    move.b    d1,DL(a1)
    rol.l    #8,d1
    nop
.wait3   
    btst    #1,ISR(a1)
    beq.s    .wait3
    move.b    d1,DM(a1)
    rol.l    #8,d1
    move.b    d1,DL(a1)    
.endloop
    dbf    d0,.loop
    
    tst.l    d0
    bpl.s    .exit
    
    nop
.wait4
    btst    #1,ISR(a1)
    beq.s    .wait4
    
    move.b    (a0)+,DM(a1)
    move.b    (a0)+,DL(a1)
.exit    rts

**************************
* 8-bit copy, A->D
_litead8
@litead8
    move.l    _base,a1
    clr.b    DM(a1)
    bra.s    .endloop    
    cnop    0,8

.loop    
    nop
.wait  
    btst    #1,ISR(a1)
    beq.s    .wait
    move.b    (a0)+,DH(a1)
    clr.b    DL(a1)

.endloop
    dbf    d0,.loop

    rts
    

*****************************    
* 32 bit copy, A->D
_plusad32h
@plusad32h
    subq.l    #1,d0
    bmi.s    .exit
    
    move.l    _base,a1
    turboen    0
    lsr.l    #2,d0
    lea    DH+TURBO(a1),a1
    nop
.loop    
    move.l    (a0)+,(a1)
    dbf    d0,.loop

    turbodis    DH+TURBO
.exit
    rts
    
*****************************    
* 32 bit copy, A->D, shift right
_plusad32l
@plusad32l
    subq.l    #1,d0
    bmi.s    .exit
    
    move.l    _base,a1
    turboen    0
    lsr.l    #2,d0
    lea    DH+TURBO(a1),a1
    nop
.loop    
    move.l    (a0)+,d1
    lsr.l    #8,d1
    move.l    d1,(a1)
    dbf    d0,.loop
    turbodis    DH+TURBO
.exit
    rts    
    
************************
* 24-bit copy, A->D
_plusad24
@plusad24
    subq.l    #1,d0
    bmi.s    .exit
    
    divs    #3,d0
    move.l    _base,a1
    turboen    0
    lea    DH+TURBO(A1),A1
    nop
.loop
    move.b    (a0)+,d1
    swap    d1
    move.b    (a0)+,d1
    lsl.w    #8,d1
    move.b    (a0)+,d1
    move.l    d1,(a1)
    dbf    d0,.loop
    turbodis    DH+TURBO
    
.exit
    rts
    
**************************
* 16-bit copy, A->D
_plusad16
@plusad16
    subq.l    #1,d0
    bmi.s    .exit

    move.l    _base,a1
    clr.b    DH(a1)

    turboen    0
    lsr.l    #1,d0
    
    lea    DM+TURBO(a1),a1
    nop
.loop
    move.w    (a0)+,(a1)
    dbf    d0,.loop

    turbodis    DM+TURBO
.exit    rts

**************************
* 8-bit copy, A->D
_plusad8
@plusad8
    subq.l    #1,d0
    bmi.s    .exit

    move.l    _base,a1
    clr.b    DM(a1)
    turboen    0
    lea    DH+TURBO(a1),a1
    nop
.loop    
    move.b    (a0)+,(a1)
    clr.b    DL-(DH+TURBO)(a1)
    dbf    d0,.loop
    turbodis    DH+TURBO
.exit
    rts

ICRlp equ  0;24
ISRlp equ  2;26
DMlp  equ  6;30

CVRlp equ 32;56
IVRlp equ 34;58 
DHlp  equ 36;60
DLlp  equ 38;62
   

**************************
* Self modifing code
* Ugly hack for changing registers to 26pin expansionsport
*
_changeport
@changeport
	move.l #__XCEXIT,a0    ; Start of delf xferprgs
	moveq #0,d0

changeloop
	cmp.l #_changeport,a0
	bge.w ende		;end of change?  a0<changeport
	move.w (a0)+,d0
	cmp.w #$1140,d0		;check for move.b d0,x(a0)
	beq.s change	;

	cmp.w #$1141,d0		;check for move.b d1,x(a0)
	beq.s change	;

	cmp.w #$1028,d0		;check for move.b x(a0),d0
	beq.s change	;
	cmp.w #$10e9,d0		;check for move.b x(a1),(a0)+
	beq.s change	;
	cmp.w #$1358,d0		;check for move.b (a0)+,x(a1)
	beq.s change	;
	cmp.w #$1341,d0		;check for move.b d1,x(a0)
	beq.s change	;
	cmp.w #$1229,d0		;check for move.b x(a1),d1
	beq.s change	;

	cmp.w #$1340,d0		;check for move.b d0,x(a1)
	beq.s change	;
	cmp.w #$4228,d0		;check for clr.b x(a0)
	beq.s change
	cmp.w #$4229,d0		;check for clr.b x(a1)
	beq.s change
	cmp.w #$4a28,d0		;check for tst.b x(a0)
	beq.s change
	cmp.w #$4a29,d0		;check for tst.b x(a1)
	beq.s change

	cmp.w #$117c,d0		;check for move.b #x,x(a0)
	beq.s changeop
	cmp.w #$137c,d0		;check for move.b #x,x(a1)
	beq.s changeop
	cmp.w #$0828,d0		;check for btst.b #x,x(a0)
	beq.s changeop
	cmp.w #$0829,d0		;check for btst.b #x,x(a1)
	beq.s changeop

	bra.s changeloop
changeop
	addq #2,a0
change	move.w (a0),d0		; get the Token
	cmp.w #CVR,d0
	bne.s notcvr
	move.w #CVRlp,(a0)

notcvr	cmp.w #ISR,d0		; check for ISR register
	bne.s notisr		
	move.w #ISRlp,(a0)	; yes change it to localport register

notisr	cmp.w #IVR,d0
	bne.s notivr
	move.w #IVRlp,(a0)

notivr	cmp.w #DH,d0
	bne.s notdh
	move.w #DHlp,(a0)

notdh	cmp.w #DM,d0
	bne.s notdm
	move.w #DMlp,(a0)

notdm	cmp.w #DL,d0
	bne.s notdl
	move.w #DLlp,(a0)
notdl	bra.w changeloop 
ende	rts

