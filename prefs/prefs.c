#include <exec/types.h>
#include <libraries/mui.h>
#include <dos/dos.h>
#include <graphics/gfxmacros.h>
#include <workbench/workbench.h>
#include <libraries/delfina.h>

#include <clib/alib_protos.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/icon.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/gadtools.h>
#include <proto/utility.h>
#include <proto/asl.h>
#include <proto/muimaster.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "delfprefs_rev.h"

#ifndef max
#define max(a,b) ((a)>(b) ? (a) : (b))
#define min(a,b) ((a)<(b) ? (a) : (b))
#endif

#define INMAX 15
#define OUTMAX 30
#define INVAL 4369
#define OUTVAL 1040
#define MIXMAX 8
#define MIXMIN -24

struct Library *MUIMasterBase=NULL, *DelfinaBase=NULL;
struct UtilityBase *UtilityBase;
struct IntuitionBase *IntuitionBase;

#define MAKE_ID(a,b,c,d) ((ULONG) (a)<<24 | (ULONG) (b)<<16 | (ULONG) (c)<<8 | (ULONG) (d))

static struct MUI_CustomClass *DecibelClass, *DecimalClass;

void update(void);
APTR outputchild,cmmtact,sllnmix,slmcmix,slcdmix,mixchild,app,win1,btsave,btuse,btcancel,strip,cyinput,cyoutput,cmfilt,cmlevel,slinl,slinr,slinm,slinb,sloutl,sloutr,sloutm,sloutb,slmix,slmon,slpass,clin,clout;

LONG __stack = 8192;

enum { MEN_PROJECT=1, MEN_HELP, MEN_ABOUT, MEN_QUIT, MEN_EDIT, MEN_DEFAULT, MEN_LASTSAVED, MEN_RESTORE, MEN_SETTINGS, MEN_MUI, ID_SAVE, ID_USE, ID_CANCEL, ID_INPUT, ID_OUTPUT, ID_FILTER, ID_HIGHLEVEL, ID_INVOL, ID_OUTVOL, ID_PASS, ID_MIX, ID_MUTE};
enum { CYOUT_LINE=0, CYOUT_PHONES, CYOUT_PHONESLINE, CYOUT_NONE };
enum { O_LEFT=0, O_RIGHT, O_MASTER, O_BALANCE };

static struct NewMenu MenuData1[] =
{
	{ NM_TITLE,	"Project"				, 0 ,0,0					,(APTR)MEN_PROJECT	},
	{ NM_ITEM,	"Help..."				, 0 ,0,0					,(APTR)MEN_HELP		},
	{ NM_ITEM,	NM_BARLABEL				, 0 ,0,0					,(APTR)0			},
	{ NM_ITEM,	"About..."				, 0 ,0,0					,(APTR)MEN_ABOUT	},
	{ NM_ITEM,	NM_BARLABEL				, 0 ,0,0					,(APTR)0			},
	{ NM_ITEM,	"Quit"					,"Q",0,0					,(APTR)MEN_QUIT		},
	
	{ NM_TITLE,	"Edit"					, 0 ,0,0					,(APTR)MEN_EDIT		},
	{ NM_ITEM,	"Reset To Defaults"		,"D",0,0					,(APTR)MEN_DEFAULT	},
	{ NM_ITEM,	"Last Saved"			,"L",0,0					,(APTR)MEN_LASTSAVED},
	{ NM_ITEM,	"Restore"				,"R",0,0					,(APTR)MEN_RESTORE	},
	
	{ NM_TITLE, "Settings"            	, 0 ,0,0					,(APTR)MEN_SETTINGS },
	{ NM_ITEM,	"MUI..."				, 0 ,0,0					,(APTR)MEN_MUI	},
	

	{ NM_END,NULL,0,0,0,(APTR) 0 },
};



/*************************/
/* Init & Fail Functions */
/*************************/

static VOID fail(APTR app,char *str)
{
        if (app) MUI_DisposeObject(app);

        if (MUIMasterBase) CloseLibrary(MUIMasterBase);

		if (DelfinaBase) CloseLibrary(DelfinaBase);
				
      if (IntuitionBase) CloseLibrary(IntuitionBase);
      if (UtilityBase) CloseLibrary(UtilityBase);

        if (str)
        {
                puts(str);
                exit(20);
        }
        exit(0);
}

static VOID init(VOID)
{
        if (!(MUIMasterBase = OpenLibrary(MUIMASTER_NAME,MUIMASTER_VMIN)))
                fail(NULL,"Failed to open "MUIMASTER_NAME".");
                
        if (!(DelfinaBase = OpenLibrary("delfina.library",4)))
                fail(NULL,"Failed to open delfina.library V4.");            

        if (!(IntuitionBase = OpenLibrary("intuition.library",36)))
                fail(NULL,"Failed to open intuition.library V36.");
                
        if (!(UtilityBase = OpenLibrary("utility.library",36)))
                fail(NULL,"Failed to open utility.library V36");                
}

/***********************************************/
/* slider hooks. these are rather complex.. :) */

#ifdef __PPC__
#define LRFunc &M68k_LRFunc
void PPC_LRFunc(void);
struct EmulLibEntry M68k_LRFunc={TRAP_LIB,0,PPC_LRFunc};
void PPC_LRFunc(void) {
   Object **objptr=(Object **)REG_A1;
#else
__saveds __asm VOID LRFunc(	register __a1 Object **objptr) {
#endif
	int left,right;
	
	get(objptr[O_LEFT], MUIA_Numeric_Value,&left);
	get(objptr[O_RIGHT],MUIA_Numeric_Value,&right);
	nnset(objptr[O_MASTER],MUIA_Numeric_Value,max(left,right));
	nnset(objptr[O_BALANCE],MUIA_Numeric_Value,right-left);
}	
	
#ifdef __PPC__
#define MBFunc &M68k_MBFunc
void PPC_MBFunc(void);
struct EmulLibEntry M68k_MBFunc={TRAP_LIB,0,PPC_MBFunc};
void PPC_MBFunc(void) {
   Object **objptr=(Object **)REG_A1;
#else
__saveds __asm VOID MBFunc(	register __a1 Object **objptr) {
#endif
	int bal,vol;
	
	get(objptr[O_BALANCE],MUIA_Numeric_Value,&bal);
	get(objptr[O_MASTER], MUIA_Numeric_Value,&vol);

	if (bal>0) {
		nnset(objptr[O_RIGHT],MUIA_Numeric_Value,vol);
		nnset(objptr[O_LEFT], MUIA_Numeric_Value,vol-bal);
	} else {
		nnset(objptr[O_RIGHT],MUIA_Numeric_Value,vol+bal);
		nnset(objptr[O_LEFT], MUIA_Numeric_Value,vol);
	}
}

/*******************************************************************/
/* My own Decibel custom sub class. Show 1.5*realvalue in the knob */

struct DecibelData
{
	char buf[10];
};

#ifdef __PPC__
#define DecibelDispatcher &M68k_DecibelDispatcher
ULONG PPC_DecibelDispatcher(void);
struct EmulLibEntry M68k_DecibelDispatcher={TRAP_LIB,0,PPC_DecibelDispatcher};
ULONG PPC_DecibelDispatcher(void) {
   struct IClass *cl = (struct IClass *)REG_A0;
   Object *obj=(Object *)REG_A2;
   Msg msg=(Msg)REG_A1;
#else
ULONG __saveds __asm DecibelDispatcher(	register __a0 struct IClass *cl,
										register __a2 Object *obj,
										register __a1 Msg msg)
{
#endif
	struct DecibelData *data;
	struct MUIP_Numeric_Stringify *m = (APTR) msg;
	int x=-1000;
		
	if (msg->MethodID==MUIM_Numeric_Stringify)
	{
		data=INST_DATA(cl,obj);
		if (m->value<=-15) { 
			get(obj,MUIA_Numeric_Max,&x);
			if (x<9) get(obj,MUIA_Numeric_Min,&x);
		}
		if (m->value==x) 
			sprintf(data->buf,"Mute");
		else
			sprintf(data->buf,"%.1f",m->value*1.5);
		return((ULONG)data->buf);
	}
	return(DoSuperMethodA(cl,obj,msg));
}

/*******************************************************************/
/* My own Decimal custom sub class. Show 0.1*realvalue in the knob */

struct DecimalData
{
	char buf[10];
};

#ifdef __PPC__
#define DecimalDispatcher &M68k_DecimalDispatcher
ULONG PPC_DecimalDispatcher(void);
struct EmulLibEntry M68k_DecimalDispatcher={TRAP_LIB,0,PPC_DecimalDispatcher};
ULONG PPC_DecimalDispatcher(void) {
   struct IClass *cl = (struct IClass *)REG_A0;
   Object *obj=(Object *)REG_A2;
   Msg msg=(Msg)REG_A1;
#else
ULONG __saveds __asm DecimalDispatcher(	register __a0 struct IClass *cl,
										register __a2 Object *obj,
										register __a1 Msg msg)
{
#endif
	struct DecimalData *data;
	struct MUIP_Numeric_Stringify *m = (APTR) msg;
	
	if (msg->MethodID==MUIM_Numeric_Stringify)
	{
		data=INST_DATA(cl,obj);
		sprintf(data->buf,"%.1f",m->value*0.1);
		return((ULONG)data->buf);
	}
	return(DoSuperMethodA(cl,obj,msg));
}
		
		
static VOID CleanupClasses(VOID)
{
	if (DecibelClass) MUI_DeleteCustomClass(DecibelClass);
	if (DecimalClass) MUI_DeleteCustomClass(DecimalClass);
}

static BOOL SetupClasses(VOID)
{
	DecibelClass = MUI_CreateCustomClass(NULL,MUIC_Slider,NULL,sizeof(struct DecibelData),DecibelDispatcher);
	DecimalClass = MUI_CreateCustomClass(NULL,MUIC_Slider,NULL,sizeof(struct DecimalData),DecimalDispatcher);
	if (DecibelClass && DecimalClass) return(TRUE);
	CleanupClasses();
	return(FALSE);
}

int main(int argc,char *argv[])
{
	ULONG signals;
	BOOL running = TRUE;
	char *InputSel[5];
	char *OutputSel[]	= { "Line","Headphones","Headphones + Line" , "None", NULL };
	static struct Hook LRHook   = { {0,0}, (VOID *)LRFunc,    0, 0 };
	static struct Hook MBHook   = { {0,0}, (VOID *)MBFunc,    0, 0 };
	int oldlnmix,oldmcmix,oldcdmix,oldmtact,hwinfo,oldpass,oldmix,oldmon,oldinl,oldinr,oldoutl,oldoutr,oldloe,oldhoe,oldinput,oldlevel,oldmline,oldhpass;
	char *guidename;
	BPTR lock;
	int id,l,r,x;
	struct MsgPort *notifyport;
	struct DelfMsg *msg;
	struct TagItem *tag, *tagptr;
	
	init();

	if (!SetupClasses())
		fail(NULL,"Could not create custom classes.");

/* get information about delfina configuration */		
	hwinfo=Delf_GetAttr(DA_HWInfo,0);
	if (!hwinfo) hwinfo=HWF_Classic | HWF_Serial | HWF_Parallel;

/* store old parameters */
	oldinl=Delf_GetAttr(DA_InputGainL,0);
	oldinr=Delf_GetAttr(DA_InputGainR,0);
	oldoutl=Delf_GetAttr(DA_OutputVolL,0);
	oldoutr=Delf_GetAttr(DA_OutputVolR,0);
	oldpass=Delf_GetAttr(DA_PassthruVol,0);
	oldmon=Delf_GetAttr(DA_MonitorVol,0);
	oldmix=Delf_GetAttr(DA_MixVol,0);
	oldloe=Delf_GetAttr(DA_LineOut,0);
	oldhoe=Delf_GetAttr(DA_HeadphoneOut,0);
	oldinput=Delf_GetAttr(DA_InputSel,0);
	oldlevel=Delf_GetAttr(DA_HighLevel,0);
	oldmline=Delf_GetAttr(DA_MicIsLine,0);
	oldhpass=Delf_GetAttr(DA_HighPass,0);
	oldlnmix=Delf_GetAttr(DA_LineMixVol,0);
	oldmcmix=Delf_GetAttr(DA_MicMixVol,0);
	oldcdmix=Delf_GetAttr(DA_CDMixVol,0);
	oldmtact=Delf_GetAttr(DA_MuteActive,0);

/* find Amigaguide help file*/
	if (lock=Lock(guidename="progdir:Docs/DelfPrefs.guide",SHARED_LOCK)) UnLock(lock);
	else if (lock=Lock(guidename="help:DelfPrefs.guide",SHARED_LOCK)) UnLock(lock);
	else if (lock=Lock(guidename="help:english/DelfPrefs.guide",SHARED_LOCK)) UnLock(lock);


/* create passthru section depending on Delfina model */
	if (hwinfo & HWF_Classic) {
		InputSel[0]="Line";
		InputSel[1]="Mic";
		InputSel[2]="Mic as line";
		InputSel[3]=NULL;
		mixchild=
			ColGroup(2), GroupFrameT("Output mixer"),
				MUIA_HelpNode, "Mixer",
				Child, Label1((ULONG)"Idle:"),  Child, slpass=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,-INMAX,MUIA_Numeric_Max,0,0),
				Child, Label1((ULONG)"Active:"), Child, slmon=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,-INMAX,MUIA_Numeric_Max,0,0),
			End;
		outputchild=
			HGroup,
				MUIA_HelpNode, "OutputSel",
				Child, cyoutput=CycleObject, MUIA_Cycle_Entries, OutputSel, End,
			End;
			
	} else {
		InputSel[0]="Line1";
		InputSel[1]="Line2";
		InputSel[2]="Line2 as mic";
		InputSel[3]="CD-ROM";
		InputSel[4]=NULL;
		mixchild=
			VGroup, GroupFrameT("Mixer"),
				MUIA_HelpNode, "Mixer",
				Child, ColGroup(2), GroupFrameT("Gain (dB)"),
					Child, Label1((ULONG)"Line1:"), Child, sllnmix=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,MIXMIN,MUIA_Numeric_Max,MIXMAX,0),
					Child, Label1((ULONG)"Line2:"), Child, slmcmix=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,MIXMIN,MUIA_Numeric_Max,MIXMAX,0),
					Child, Label1((ULONG)"CD-ROM:"), Child, slcdmix=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,MIXMIN,MUIA_Numeric_Max,MIXMAX,0),
				End,
				Child, HGroup,
					Child, cmmtact= ImageObject,
              		ImageButtonFrame,\
            		MUIA_InputMode        , MUIV_InputMode_Toggle,
            		MUIA_Image_Spec       , MUII_CheckMark,
                  MUIA_Image_FreeVert   , TRUE,
            		MUIA_Selected         , TRUE,
            		MUIA_Background       , MUII_ButtonBack,
            		MUIA_ShowSelState     , FALSE,
            		MUIA_ControlChar      , 'm',
         		End, 
               Child, Label1((ULONG)"_Mute active input"), Child, HSpace(0),
				End,
			End;
		outputchild=VSpace(0);
	}

	
/* Create application */		
	app = ApplicationObject,
		MUIA_Application_Title      , PROJECT,
		MUIA_Application_Version    , "$VER: " VERS " (" DATE ")",
		MUIA_Application_Copyright  , "� 1997 by Petsoff Limited Partnership",
		MUIA_Application_Author     , "Teemu Suikki",
		MUIA_Application_Description, "Preferences for the Delfina DSP board",
		MUIA_Application_Base       , "DELFPREFS",
		MUIA_Application_HelpFile	, guidename,

		MUIA_Application_Window, win1 = WindowObject,
			MUIA_Window_Title, "Delfina Preferences",
			MUIA_Window_ID   , MAKE_ID('D','F','P','1'),
			MUIA_Window_Menustrip, strip = MUI_MakeObject(MUIO_MenustripNM,MenuData1,0),
			MUIA_HelpNode, "Contents", 
			WindowContents, VGroup,
				Child, HGroup, 
					Child, VGroup, GroupFrameT("Input settings"), 
						Child, HGroup, 
							MUIA_HelpNode, "InputSel",
							Child, cyinput=CycleObject, MUIA_Cycle_Entries, InputSel, End, 
						End,
						Child, VSpace(0),
						Child, ColGroup(2), GroupFrameT("Gain (dB)"),
							MUIA_HelpNode, "Gain",
							Child, Label1((ULONG)"Left:"),   Child, slinl=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,INMAX,0),
							Child, Label1((ULONG)"Right:"),  Child, slinr=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,INMAX,0),
							Child, Label1((ULONG)"Master:"), Child, slinm=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,INMAX,0),
							Child, Label1((ULONG)"Balance:"),Child, slinb=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,-INMAX,MUIA_Numeric_Max,INMAX,0),
						End,	
						Child, VSpace(0),
						Child, HGroup,
							MUIA_HelpNode, "Filter",
							Child, cmfilt=
                     	ImageObject,
                     		ImageButtonFrame,
                     		MUIA_InputMode        , MUIV_InputMode_Toggle,
                     		MUIA_Image_Spec       , MUII_CheckMark,
                     		MUIA_Image_FreeVert   , TRUE,
                     		MUIA_Selected         , TRUE,
                     		MUIA_Background       , MUII_ButtonBack,
                     		MUIA_ShowSelState     , FALSE,
                     		MUIA_ControlChar      , 'f',
                        End,
                        Child, Label1((ULONG)"Highpass _filter"), Child, HSpace(0),
						End,
					End,
					Child, VGroup,
						Child, VGroup, GroupFrameT("Output settings"),
							Child, outputchild,
							Child, VSpace(0),
							Child, ColGroup(2), GroupFrameT("Volume (dB)"),
								MUIA_HelpNode, "Volume",
								Child, Label1((ULONG)"Left:"),     Child, sloutl=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,-OUTMAX,MUIA_Numeric_Max,0,0),
								Child, Label1((ULONG)"Right:"),    Child, sloutr=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,-OUTMAX,MUIA_Numeric_Max,0,0),
								Child, Label1((ULONG)"Master:"),   Child, sloutm=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,-OUTMAX,MUIA_Numeric_Max,0,0),
								Child, Label1((ULONG)"Balance:"),  Child, sloutb=NewObject(DecibelClass->mcc_Class,0,MUIA_Numeric_Min,-OUTMAX,MUIA_Numeric_Max,OUTMAX,0),
								Child, Label1((ULONG)"Mix Boost:"),Child, slmix =NewObject(DecimalClass->mcc_Class,0,MUIA_Numeric_Min,0,MUIA_Numeric_Max,60,0),
							End,
							Child, VSpace(0),
							Child, HGroup,
								MUIA_HelpNode, "HighLevel",
								Child, cmlevel=
                        	ImageObject,
                        		ImageButtonFrame,
                        		MUIA_InputMode        , MUIV_InputMode_Toggle,
                        		MUIA_Image_Spec       , MUII_CheckMark,
                        		MUIA_Image_FreeVert   , TRUE,
                        		MUIA_Selected         , TRUE,
                        		MUIA_Background       , MUII_ButtonBack,
                        		MUIA_ShowSelState     , FALSE,
                        		MUIA_ControlChar      , 'h',
                           End,
                           Child, Label1((ULONG)"_High level output"), Child, HSpace(0),
							End,
						End,
					End,
					Child, VGroup,
						Child, mixchild,
						Child, VSpace(0),
						Child, ColGroup(2), GroupFrameT("Clipping indicators"),
							MUIA_HelpNode, "ClipInd",
							Child, Label1((ULONG)"Input"),  Child, clin= TextObject, TextFrame, MUIA_Background, MUII_BACKGROUND, MUIA_Text_Contents, "    ", End,
							Child, Label1((ULONG)"Output"), Child,clout= TextObject, TextFrame, MUIA_Background, MUII_BACKGROUND, MUIA_Text_Contents, "    ", End,
						End,
					End,
				End,
				Child, VSpace(2),
				Child, HGroup, MUIA_Group_SameSize, TRUE,
					MUIA_HelpNode,"Gadgets",
					Child, btsave	= SimpleButton((ULONG)"_Save"  ),
					Child, HSpace(0),
					Child, btuse	= SimpleButton((ULONG)"_Use"),
					Child, HSpace(0),
					Child, btcancel	= SimpleButton((ULONG)"_Cancel"  ),
				End,
			End,
		End,
	End;

	if (!app )
		fail(app,"Failed to create Application.");

/* notifications */
	DoMethod((Object *)DoMethod(strip,MUIM_FindUData,MEN_MUI      ),MUIM_Notify,MUIA_Menuitem_Trigger,MUIV_EveryTime,MUIV_Notify_Application,2,MUIM_Application_OpenConfigWindow,0);
	DoMethod((Object *)DoMethod(strip,MUIM_FindUData,MEN_HELP      ),MUIM_Notify,MUIA_Menuitem_Trigger,MUIV_EveryTime,MUIV_Notify_Application,5,MUIM_Application_ShowHelp,win1,guidename,"Main","0");
	DoMethod(app		,MUIM_Notify,MUIA_Application_MenuHelp,MUIV_EveryTime,MUIV_Notify_Application,5,MUIM_Application_ShowHelp,win1,guidename,"Menus","0");
	DoMethod(win1     	,MUIM_Notify,MUIA_Window_CloseRequest,TRUE,app,2,MUIM_Application_ReturnID,MUIV_Application_ReturnID_Quit);
	DoMethod(slinl	  	,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,slinl,6,MUIM_CallHook,&LRHook, slinl,slinr,slinm,slinb);
	DoMethod(slinr	  	,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,slinr,6,MUIM_CallHook,&LRHook, slinl,slinr,slinm,slinb);
	DoMethod(slinm	  	,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,slinm,6,MUIM_CallHook,&MBHook, slinl,slinr,slinm,slinb);
	DoMethod(slinb	  	,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,slinb,6,MUIM_CallHook,&MBHook, slinl,slinr,slinm,slinb);
	DoMethod(sloutl	  	,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,sloutl,6,MUIM_CallHook,&LRHook, sloutl,sloutr,sloutm,sloutb);
	DoMethod(sloutr	  	,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,sloutr,6,MUIM_CallHook,&LRHook, sloutl,sloutr,sloutm,sloutb);
	DoMethod(sloutm	  	,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,sloutm,6,MUIM_CallHook,&MBHook, sloutl,sloutr,sloutm,sloutb);
	DoMethod(sloutb	  	,MUIM_Notify,MUIA_Numeric_Value,MUIV_EveryTime,sloutb,6,MUIM_CallHook,&MBHook, sloutl,sloutr,sloutm,sloutb);
	DoMethod(btsave   	,MUIM_Notify,MUIA_Pressed,FALSE					,app,2,MUIM_Application_ReturnID,ID_SAVE  );
	DoMethod(btuse    	,MUIM_Notify,MUIA_Pressed,FALSE					,app,2,MUIM_Application_ReturnID,ID_USE   );
	DoMethod(btcancel 	,MUIM_Notify,MUIA_Pressed,FALSE					,app,2,MUIM_Application_ReturnID,ID_CANCEL);
	DoMethod(cyinput	,MUIM_Notify,MUIA_Cycle_Active  ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_INPUT);
	DoMethod(cyoutput	,MUIM_Notify,MUIA_Cycle_Active  ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_OUTPUT);
	DoMethod(cmfilt		,MUIM_Notify,MUIA_Selected	    ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_FILTER);
	DoMethod(cmlevel	,MUIM_Notify,MUIA_Selected	    ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_HIGHLEVEL);
	DoMethod(slinl		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_INVOL);
	DoMethod(slinr		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_INVOL);
	DoMethod(slinb		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_INVOL);
	DoMethod(slinm		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_INVOL);
	DoMethod(sloutl		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_OUTVOL);
	DoMethod(sloutr		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_OUTVOL);
	DoMethod(sloutb		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_OUTVOL);
	DoMethod(sloutm		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_OUTVOL);
	DoMethod(slmix		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_OUTVOL);

/* Window cycle chain etc */
	if (hwinfo & HWF_Classic) {
		DoMethod(slmon		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_PASS);
		DoMethod(slpass		,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_PASS);
		DoMethod(win1,MUIM_Window_SetCycleChain, btsave,btuse,btcancel,cyinput,cmfilt,slinl,slinr,
			slinm,slinb,cyoutput,sloutl,sloutr,sloutm,sloutb,slmix,cmlevel,slpass,slmon,NULL);
	} else { 
		DoMethod(sllnmix	,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_MIX);
		DoMethod(slmcmix	,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_MIX);
		DoMethod(slcdmix	,MUIM_Notify,MUIA_Numeric_Value ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_MIX);
		DoMethod(cmmtact	,MUIM_Notify,MUIA_Selected	    ,MUIV_EveryTime	,app,2,MUIM_Application_ReturnID,ID_MUTE);
		DoMethod(win1,MUIM_Window_SetCycleChain, btsave,btuse,btcancel,cyinput,cmfilt,slinl,slinr,
			slinm,slinb,sloutl,sloutr,sloutm,sloutb,slmix,cmlevel,sllnmix,slmcmix,slcdmix,cmmtact,NULL);
	}
		
	update();
	
/* create notify port */
	notifyport=Delf_StartNotify(DA_InputGainL,DA_InputGainR,DA_OutputVolL,DA_OutputVolR,
		DA_MonitorVol,DA_PassthruVol,DA_MixVol,DA_LineOut,DA_HeadphoneOut,DA_InputSel, 
		DA_LineMixVol,DA_MicMixVol,DA_CDMixVol,DA_MuteActive,
		DA_HighLevel,DA_MicIsLine,DA_HighPass,DA_InputClip,DA_OutputClip,0);
	if (!notifyport) goto clean;
	
/*
** Input loop...
*/

	set(win1,MUIA_Window_Open,TRUE);

	while (running)
	{
		while (msg=(struct DelfMsg*)GetMsg(notifyport)) {
			tagptr=msg->taglist;
			msg->result=NULL;
			while (tag=NextTagItem(&tagptr)) {
				switch (tag->ti_Tag) {
					case DA_InputGainL:
						nnset(slinl, MUIA_Numeric_Value,l=tag->ti_Data/INVAL);
						get(slinr, MUIA_Numeric_Value, &r);
						nnset(slinm,MUIA_Numeric_Value,max(l,r));
						nnset(slinb,MUIA_Numeric_Value,r-l);						
						break;
						
					case DA_InputGainR:
						get(slinl, MUIA_Numeric_Value, &l);
						nnset(slinr, MUIA_Numeric_Value,r=tag->ti_Data/INVAL);
						nnset(slinm,MUIA_Numeric_Value,max(l,r));
						nnset(slinb,MUIA_Numeric_Value,r-l);						
						break;
						
					case DA_OutputVolL:
						nnset(sloutl,MUIA_Numeric_Value,l=tag->ti_Data/OUTVAL-63);
						get(sloutr, MUIA_Numeric_Value, &r);
						nnset(sloutm,MUIA_Numeric_Value,max(l,r));
						nnset(sloutb,MUIA_Numeric_Value,r-l);						
						break;
					
					case DA_OutputVolR:
						get(sloutl, MUIA_Numeric_Value, &l);
						nnset(sloutr,MUIA_Numeric_Value,r=tag->ti_Data/OUTVAL-63);
						nnset(sloutm,MUIA_Numeric_Value,max(l,r));
						nnset(sloutb,MUIA_Numeric_Value,r-l);						
						break;
						
					case DA_LineMixVol:
						if (sllnmix) nnset(sllnmix,MUIA_Numeric_Value,(tag->ti_Data>>11)-24);
						break;
						
					case DA_MicMixVol:
						if (slmcmix) nnset(slmcmix,MUIA_Numeric_Value,(tag->ti_Data>>11)-24);
						break;
						
					case DA_CDMixVol:
						if (slcdmix) nnset(slcdmix,MUIA_Numeric_Value,(tag->ti_Data>>11)-24);
						break;
						
					case DA_MonitorVol:
						if (slmon) nnset(slmon, MUIA_Numeric_Value,tag->ti_Data/INVAL-15);
						break;
						
					case DA_PassthruVol:
						if (slpass) nnset(slpass,MUIA_Numeric_Value,tag->ti_Data/INVAL-15);
						break;
						
					case DA_MixVol:
						nnset(slmix, MUIA_Numeric_Value,60-tag->ti_Data/1093);
						break;
						
					case DA_LineOut:
					case DA_HeadphoneOut:
						if (Delf_GetAttr(DA_LineOut,0))
							if (Delf_GetAttr(DA_HeadphoneOut,0)) l=CYOUT_PHONESLINE; else l=CYOUT_LINE;
						else
							if (Delf_GetAttr(DA_HeadphoneOut,0)) l=CYOUT_PHONES; else l=CYOUT_NONE;
						if (cyoutput) nnset(cyoutput,MUIA_Cycle_Active,l);
						break;
						
					case DA_InputSel:
					case DA_MicIsLine:
						l=Delf_GetAttr(DA_InputSel,0);
						if (l==2) l=3;
						if (l==1 && !Delf_GetAttr(DA_MicIsLine,0)) l=2;
						if ((hwinfo & HWF_Classic) && l==1) l=2;
						else if ((hwinfo & HWF_Classic) && l==2) l=1;
						nnset(cyinput,MUIA_Cycle_Active,l);
						break;
					
					case DA_HighLevel:
						nnset(cmlevel,MUIA_Selected,tag->ti_Data);
						break;
						
					case DA_HighPass:
						nnset(cmfilt,MUIA_Selected,tag->ti_Data);
						break;
						
					case DA_MuteActive:
						if (cmmtact) nnset(cmmtact,MUIA_Selected,tag->ti_Data);
						break;
						
					case DA_InputClip:
						if (tag->ti_Data) nnset(clin,MUIA_Text_Contents,"Clip");
						else 			  nnset(clin,MUIA_Text_Contents," ");
						break;
						
					case DA_OutputClip:
						if (tag->ti_Data) nnset(clout,MUIA_Text_Contents,"Clip");
						else 			  nnset(clout,MUIA_Text_Contents," ");
						break;
				}
			}
			ReplyMsg(&msg->msg);
		}
		
		id = DoMethod(app,MUIM_Application_Input,&signals);
		switch (id)
		{
			case MEN_ABOUT:
				MUI_Request(app,win1,0,NULL,"OK",VERS" ("DATE")\n\n� 1997 by Petsoff Limited Partnership");
				break;
				
			case MEN_DEFAULT:
				Delf_SetAttrs(DA_Defaults,1,0);
				update();
				break;
				
			case MEN_LASTSAVED:
				Delf_SetAttrs(DA_Load,1,0);
				update();
				break;
				
			case MEN_QUIT:
			case MUIV_Application_ReturnID_Quit:
			case ID_CANCEL:
				running=FALSE;
			case MEN_RESTORE:
				Delf_SetAttrs(	DA_InputGainL,oldinl,
								DA_InputGainR,oldinr,
								DA_OutputVolL,oldoutl,
								DA_OutputVolR,oldoutr,
								DA_MonitorVol,oldmon,
								DA_PassthruVol,oldpass,
								DA_MixVol,oldmix,
								DA_LineOut,oldloe,
								DA_HeadphoneOut,oldhoe,
								DA_InputSel,oldinput,
								DA_HighLevel,oldlevel,
								DA_MicIsLine,oldmline,
								DA_HighPass,oldhpass, 0);
				if (running) update();
				break;
				
			case ID_SAVE:
				Delf_SetAttrs(	DA_Save, 1,0);
			case ID_USE:
				running=FALSE;
				break;

			case ID_INPUT:
				get(cyinput,MUIA_Cycle_Active,&l);
				switch(l) {
					case 0:
						Delf_SetAttrs( DA_InputSel, 0, 0);
						break;
					case 1:
						Delf_SetAttrs( DA_InputSel, 1, DA_MicIsLine, (hwinfo & HWF_Classic) ? 0:1, 0);
						break;
					case 2:
						Delf_SetAttrs( DA_InputSel, 1, DA_MicIsLine, (hwinfo & HWF_Classic) ? 1:0, 0);
						break;
					case 3:
						Delf_SetAttrs( DA_InputSel, 2, 0);
						break;
				}
				break;
			
			case ID_OUTPUT:
				get(cyoutput,MUIA_Cycle_Active,&l);
				switch(l) {
					case CYOUT_LINE:
						Delf_SetAttrs( DA_LineOut, TRUE, DA_HeadphoneOut, FALSE, 0);
						break;
					case CYOUT_PHONES:
						Delf_SetAttrs( DA_LineOut, FALSE, DA_HeadphoneOut, TRUE, 0);
						break;
					case CYOUT_PHONESLINE:
						Delf_SetAttrs( DA_LineOut, TRUE, DA_HeadphoneOut, TRUE, 0);
						break;
					case CYOUT_NONE:
						Delf_SetAttrs( DA_LineOut, FALSE, DA_HeadphoneOut, FALSE, 0);
				}
				break;
		
			case ID_FILTER:
				get(cmfilt,MUIA_Selected,&l);
				Delf_SetAttrs(DA_HighPass,l,0);
				break;
				
			case ID_HIGHLEVEL:
				get(cmlevel,MUIA_Selected,&l);
				Delf_SetAttrs(DA_HighLevel,l,0);
				break;

			case ID_INVOL:
				get(slinl,MUIA_Numeric_Value,&l);
				get(slinr,MUIA_Numeric_Value,&r);
				Delf_SetAttrs(DA_InputGainL,l*INVAL,DA_InputGainR,r*INVAL,0);
				break;
				
			case ID_OUTVOL:
				get(sloutl,MUIA_Numeric_Value,&l);
				get(sloutr,MUIA_Numeric_Value,&r);
				Delf_SetAttrs(DA_OutputVolL,(l+63)*OUTVAL,DA_OutputVolR,(r+63)*OUTVAL,0);
				get(slmix,MUIA_Numeric_Value,&l);
				Delf_SetAttrs(DA_MixVol,(60-l)*1093,0);
				break;
				
			case ID_PASS:
				get(slpass,MUIA_Numeric_Value,&l);
				get(slmon, MUIA_Numeric_Value,&r);
				Delf_SetAttrs(DA_PassthruVol,(l+15)*INVAL,DA_MonitorVol,(r+15)*INVAL,0);
				break;
				
			case ID_MIX:
				get(sllnmix, MUIA_Numeric_Value, &l);
				get(slmcmix, MUIA_Numeric_Value, &r);
				get(slcdmix, MUIA_Numeric_Value, &x);
				Delf_SetAttrs(DA_LineMixVol, (l+24)<<11, DA_MicMixVol, (r+24)<<11, DA_CDMixVol, (x+24)<<11, 0);
				break;
				
			case ID_MUTE:
				get(cmmtact,MUIA_Selected,&l);
				Delf_SetAttrs(DA_MuteActive,l,0);
				break;
				
		}
		if (running && signals) Wait(signals | (1<<notifyport->mp_SigBit));
	}
	Delf_EndNotify(notifyport);
	set(win1,MUIA_Window_Open,FALSE);

/*
** Shut down...
*/
clean:
	MUI_DisposeObject(app);
	CleanupClasses();
	fail(NULL,NULL);
}


/* update all gadget values */
void update()
{
	int x;
	
	set(slinl, MUIA_Numeric_Value,Delf_GetAttr(DA_InputGainL,0)/INVAL);
	set(slinr, MUIA_Numeric_Value,Delf_GetAttr(DA_InputGainR,0)/INVAL);
	set(sloutl,MUIA_Numeric_Value,Delf_GetAttr(DA_OutputVolL,0)/OUTVAL-63);
	set(sloutr,MUIA_Numeric_Value,Delf_GetAttr(DA_OutputVolR,0)/OUTVAL-63);
	set(slmix, MUIA_Numeric_Value,60-Delf_GetAttr(DA_MixVol,0)/1093);
	if (slpass) set(slpass,MUIA_Numeric_Value,Delf_GetAttr(DA_PassthruVol,0)/INVAL-15);
	if (slmon)	set(slmon, MUIA_Numeric_Value,Delf_GetAttr(DA_MonitorVol,0)/INVAL-15);
	
	if (sllnmix) set(sllnmix,MUIA_Numeric_Value,(Delf_GetAttr(DA_LineMixVol,0)>>11)-24);
	if (slmcmix) set(slmcmix,MUIA_Numeric_Value,(Delf_GetAttr(DA_MicMixVol,0)>>11)-24);
	if (slcdmix) set(slcdmix,MUIA_Numeric_Value,(Delf_GetAttr(DA_CDMixVol,0)>>11)-24);
	
	x=Delf_GetAttr(DA_InputSel,0);
	if (x==2) x=3;
	if (x==1 && !Delf_GetAttr(DA_MicIsLine,0)) x=2;
	if (cyoutput && x==1) x=2;
	else if (cyoutput && x==2) x=1;
	set(cyinput,MUIA_Cycle_Active,x);
	
	if (cyoutput) {
		if (Delf_GetAttr(DA_LineOut,0))
			if (Delf_GetAttr(DA_HeadphoneOut,0)) x=CYOUT_PHONESLINE; else x=CYOUT_LINE;
		else
			if (Delf_GetAttr(DA_HeadphoneOut,0)) x=CYOUT_PHONES; else x=CYOUT_NONE;
		set(cyoutput,MUIA_Cycle_Active,x);
	}
	
	set(cmfilt,MUIA_Selected,Delf_GetAttr(DA_HighPass,0));
	set(cmlevel,MUIA_Selected,Delf_GetAttr(DA_HighLevel,0));
	if (cmmtact) set(cmmtact,MUIA_Selected,Delf_GetAttr(DA_MuteActive,0));
}